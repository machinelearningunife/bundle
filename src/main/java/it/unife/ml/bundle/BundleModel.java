/**
 * This file is part of BUNDLE.
 *
 * BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 * BUNDLE can be used both as module and as standalone.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * BUNDLE and all its parts are distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import it.unife.ml.bundle.task.AllHierarchyTask;
import it.unife.ml.bundle.task.AllUnsatTask;
import it.unife.ml.bundle.task.InconsistencyTask;
import it.unife.ml.bundle.task.InstanceTask;
import it.unife.ml.bundle.task.PropertyValueTask;
import it.unife.ml.bundle.task.SubClassTask;
import it.unife.ml.bundle.task.Task;
import it.unife.ml.bundle.task.UnsatClassTask;
import it.unife.ml.bundle.utilities.Constants;
import it.unife.ml.bundle.utilities.BundleQueryResult;
import it.unife.ml.math.ApproxDouble;
import it.unife.ml.bundle.exception.IllegalValueException;
import it.unife.ml.bundle.factpp.FactppInitializer;
import it.unife.ml.bundle.utilities.Constants.ReasonerName;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;

import java.util.HashSet;
import java.util.Observable;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.manchester.cs.factplusplus.owlapiv3.FaCTPlusPlusReasonerFactory;
import uk.ac.manchester.cs.jfact.JFactFactory;

/**
 *
 * @author giuseppe
 */
public class BundleModel extends Observable {

    private Bundle bundle;

    private ProbabilisticReasonerResult queryResult;

    private OWLDataFactory df = OWLManager.getOWLDataFactory();

    protected String errorMessage;

    private static final Logger logger = LoggerFactory.getLogger(BundleModel.class);
    
    /**
     * If true means that the reasoner should obtain the explanations for the
     * inconsistency of the ontology.
     */
    protected boolean checkConsistency = false;

    public BundleModel() {
    }

    /**
     * Execute task and dispose BUNDLE if dispose is true.
     *
     * @param task
     * @param disposeAfterReasoning
     */
    public void executeTask(AllHierarchyTask task, boolean disposeAfterReasoning) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Searches for unsatisfiable classes (all-unsat) and dispose BUNDLE after
     * execution if {@code disposeAfterReasoning} is true. Executes an
     * unsatisfiable class query task for every class of the ontology
     *
     * @param task
     * @param disposeAfterReasoning
     */
    public void executeTask(AllUnsatTask task, boolean disposeAfterReasoning) {
        OWLReasoner reasoner = getReasonerFactory(task.getExternalReasonerName())
                .createReasoner(task.getRootOntology());
        for (OWLClass cls : reasoner.getEquivalentClasses(df.getOWLNothing())) {
            if (cls.isOWLNothing()) {
                continue;
            }

            UnsatClassTask unsatClassTask = new UnsatClassTask(cls, task);
            executeTask(unsatClassTask, disposeAfterReasoning);
        }
    }

    /**
     * Execute task and dispose BUNDLE if dispose is true.
     *
     * @param task
     * @param disposeAfterReasoning
     */
    public void executeTask(InconsistencyTask task, boolean disposeAfterReasoning) {

//        if (bundle.externalReasoner instanceof OWLExplanationExplanationReasoner) {
//            ((OWLExplanationExplanationReasoner) bundle.externalReasoner).setCheckConsistency(true);
//        }
        checkConsistency = true;
        SubClassTask subClassTask = new SubClassTask(df.getOWLThing(), df.getOWLNothing(), task);
        executeTask(subClassTask, disposeAfterReasoning);
        checkConsistency = false;
    }

    /**
     * Executes an "instance of" query (instance) task and dispose BUNDLE after
     * execution if {@code disposeAfterReasoning} is true.
     *
     * @param task
     * @param disposeAfterReasoning
     */
    public void executeTask(InstanceTask task, boolean disposeAfterReasoning) {
        OWLAxiom query = df.getOWLClassAssertionAxiom(task.getOwlClass(), task.getOwlIndividual());
        if (task.getOwlClass().isOWLThing()) {
            init(task);
            queryResult = new BundleQueryResult(query, ApproxDouble.one());
            Set<Set<OWLAxiom>> explanations = new HashSet<>();
            Set<OWLAxiom> explanation = new HashSet<>();
            explanation.add(query);
            explanations.add(explanation);
            ((BundleQueryResult)queryResult).setQueryExplanations(explanations);
//            queryResult.setQueryProbability(ApproxDouble.one());
        } else {
            try {
                init(task);
                if (bundle.isUseProbabilisticReasoner()) {
                    queryResult = bundle.computeQuery(query);
                } else {
                    queryResult = (BundleQueryResult) bundle.computeExplainQuery(query);
                }
            } catch (OWLException | ObjectNotInitializedException ex) {
                errorMessage = ex.getMessage();
                logger.error(errorMessage);
                setChanged();
                notifyObservers(BundleModelEvent.ERROR);
            }
        }
        setChanged();
        notifyObservers(BundleModelEvent.QUERY_COMPUTED);
        if (disposeAfterReasoning) {
            bundle.dispose();
        }
    }

    /**
     * Executes a property value query (property-value) task and dispose BUNDLE
     * after execution if {@code disposeAfterReasoning} is true.
     *
     * @param task
     * @param disposeAfterReasoning if true dispose BUNDLE.
     */
    public void executeTask(PropertyValueTask task, boolean disposeAfterReasoning) {
        OWLAxiom query;
        OWLIndividual subj = task.getSubject();
        OWLObject obj = task.getObject();
        if (task.getProperty().isOWLObjectProperty()) {
            query = df.getOWLObjectPropertyAssertionAxiom((OWLObjectProperty) task.getProperty(), subj, (OWLNamedIndividual) obj);
        } else {
            query = df.getOWLDataPropertyAssertionAxiom((OWLDataPropertyExpression) task.getProperty(), subj, (OWLLiteral) obj);
        }
        try {
            init(task);
            if (bundle.isUseProbabilisticReasoner()) {
                queryResult = bundle.computeQuery(query);
            } else {
                queryResult = (BundleQueryResult) bundle.computeExplainQuery(query);
            }
        } catch (OWLException | ObjectNotInitializedException ex) {
            errorMessage = ex.getMessage();
            logger.error(errorMessage);
            setChanged();
            notifyObservers(BundleModelEvent.ERROR);
        }

        setChanged();
        notifyObservers(BundleModelEvent.QUERY_COMPUTED);
        if (disposeAfterReasoning) {
            bundle.dispose();
        }
    }

    /**
     * Executes a "subclass of" query (subclass) task and dispose BUNDLE if
     * dispose is true.
     *
     * @param task
     * @param disposeAfterReasoning
     */
    public void executeTask(SubClassTask task, boolean disposeAfterReasoning) {
        OWLClass subClass = task.getSubClass();
        OWLClass supClass = task.getSuperClass();

        OWLSubClassOfAxiom query = df.getOWLSubClassOfAxiom(subClass, supClass);
        if (subClass.equals(supClass)
                || subClass.isOWLNothing()
                || supClass.isOWLThing()) {
            queryResult = new BundleQueryResult(query, ApproxDouble.one());
//            queryResult.setQueryProbability(ApproxDouble.one());
        } else {
            try {
                init(task);
                if (bundle.isUseProbabilisticReasoner()) {
                    queryResult = bundle.computeQuery(query);
                } else {
                    queryResult = (BundleQueryResult) bundle.computeExplainQuery(query);
                }
            } catch (OWLException | ObjectNotInitializedException ex) {
                errorMessage = ex.getMessage();
                logger.error(errorMessage);
                setChanged();
                notifyObservers(BundleModelEvent.ERROR);
            }
        }

        setChanged();
        notifyObservers(BundleModelEvent.QUERY_COMPUTED);
        if (disposeAfterReasoning) {
            bundle.dispose();
        }

    }

    /**
     * Executes an unsatisfiable class query (unsat) task and dispose BUNDLE
     * after execution if {@code dispose} is true.
     *
     * @param task
     * @param disposeAfterReasoning if true dispose BUNDLE.
     */
    public void executeTask(UnsatClassTask task, boolean disposeAfterReasoning) {
        SubClassTask subClassTask = new SubClassTask(task.getUnsatClass(), df.getOWLNothing(), task);
        executeTask(subClassTask, disposeAfterReasoning);
    }

    private void init(Task task) throws ExceptionInInitializerError {
        // validate userInput
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Task>> violations = validator.validate(task);
        for (ConstraintViolation<Task> violation : violations) {
            errorMessage = violation.getMessage();
            logger.error(errorMessage);
            setChanged();
            notifyObservers(BundleModelEvent.ERROR);
            throw new ExceptionInInitializerError(errorMessage);
        }

        if (task.getExternalReasonerName() == null) {
            errorMessage = "No external reasoner was set!";
            logger.error(errorMessage);
            setChanged();
            notifyObservers(BundleModelEvent.ERROR);
            throw new ExceptionInInitializerError(errorMessage);
        }

        BundleConfigurationBuilder configBuilder = new BundleConfigurationBuilder(task.getRootOntology());
        BundleConfiguration config = configBuilder.bddFType(task.getBDDFactoryType())
                .hstMethod(task.getHSTMethod())
                .maxExplanations(task.getMaxExplanations())
                .probabilistic(!task.isNoProb())
                .reasoner(task.getExternalReasonerName())
                .timeout(task.getReasoningTimeout())
                .verbose(task.isVerbose())
                .checkConsistency(checkConsistency)
                .buildConfiguration();

//        bundle.setBddFType(task.getBDDFactoryType());
//        bundle.setHstMethod(task.getHSTMethod());
//        bundle.setMaxExplanations(task.getMaxExplanations());
//        bundle.setProbabilistic(!task.isNoProb());
//        bundle.setReasonerFactory(task.getReasonerFactory());
//        bundle.setReasoningTimeout(task.getReasoningTimeout());
//        bundle.setRootOntology(task.getRootOntology());
//        bundle.setVerbose(task.isVerbose());
//        bundle.setExplanationReasoner(task.getExplanationReasoner());
//
//        ProbabilisticReasoner probabilisticReasoner = task.getProbabilisticReasoner();
//
//        if (probabilisticReasoner != null) {
//
//            probabilisticReasoner.setRootOntology(task.getRootOntology());
//            probabilisticReasoner.init();
//            /*
//             TO DO ALFREDO PAMBIANCHI:
//             Probabilmente qui è il punto opportuno in cui settare il reasoner trill/trillp/tornado.
//             */
//        }
//
//        bundle.setProbabilisticReasoner(probabilisticReasoner);
        bundle = new Bundle(config);
        bundle.init();
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    public BundleQueryResult getQueryResult() {
        return (BundleQueryResult) queryResult;
    }

    public boolean isProbabilistic() {
        return bundle.isProbabilistic();
    }

    public long getTiming(Constants.Timings timings) {
        return bundle.getTimers().getTimer(timings.toString()).getTotal();
    }

    private OWLReasonerFactory getReasonerFactory(ReasonerName reasonerName) {

        switch (reasonerName) {
            case PELLET:
                return PelletReasonerFactory.getInstance();
            case HERMIT:
                return new org.semanticweb.HermiT.ReasonerFactory();
            case JFACT:
                return new JFactFactory();
            case FACTPP:
                // load native library
                FactppInitializer.init();
                return new FaCTPlusPlusReasonerFactory();
            case TRILL:
            case TRILLP:
            case TORNADO:
                return PelletReasonerFactory.getInstance();
            default:
                throw new IllegalValueException("Unsupported reasoner: " + reasonerName);
        }
    }

}
