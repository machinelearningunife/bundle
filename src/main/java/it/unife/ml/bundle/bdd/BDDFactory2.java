/**
 *  This file is part of BUNDLE.
 * 
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 * 
 *  BUNDLE can be used both as module and as standalone.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.unife.ml.bundle.bdd;

import it.unife.ml.bundle.utilities.LoadNativeLibrary;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import net.sf.javabdd.BDDFactory;
import static net.sf.javabdd.BDDFactory.getProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class BDDFactory2 extends BDDFactory {

    private static final Logger logger = LoggerFactory.getLogger(BDDFactory2.class);

    static {
        try {
            String currDir = getProperty("user.dir", ".") + getProperty("file.separator", "/");
            String pack = "/bdd-libraries/lib";
            URL f = BDDFactory2.class.getResource(pack);
            String libraryDir = f.getFile();

            String jarPath = libraryDir.replaceFirst("[.]jar[!].*", ".jar").replaceFirst("file:", "");
            JarFile jarFile = new JarFile(jarPath);
            logger.debug("BDD-Libraries Jar: {}", jarPath);
            Enumeration<JarEntry> entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                String entryName = "/" + entry.getName();
                String ext;
                if (System.getProperty("os.name").startsWith("Windows")) {
                    ext = ".dll";
                } else if (System.getProperty("os.name").startsWith("Linux")) {
                    ext = ".so";
                } else if (System.getProperty("os.name").startsWith("Mac OS X")) {
                    ext = ".jnilib";
                } else {
                    throw new RuntimeException("Unknown Operating System");
                }
                if (entryName.startsWith(pack)
                        && entryName.length() > (pack.length() + "/".length())
                        && entryName.substring(entryName.length() - ext.length()).equals(ext)) {

                    String libFile = LoadNativeLibrary.createTempFile(entryName, currDir).getName();
                }
            }

        } catch (RuntimeException ex) {
            logger.error("Unable to load the Native Library.\nJ will be used!");
            StackTraceElement[] stack = ex.getStackTrace();
            for (StackTraceElement se : stack) {
                logger.error(se.toString());
            }
        } catch (IOException ex) {
            logger.error("Unable to load the Native Library.\nJ will be used!");
            logger.error(ex.getMessage());
            StackTraceElement[] stack = ex.getStackTrace();
            for (StackTraceElement se : stack) {
                logger.error(se.toString());
            }
        }
    }

    /**
     * <p>
     * Initializes a BDD factory of the given type with the given initial node
     * table size and operation cache size. The type is a string that can be
     * "buddy", "cudd", "cal", "j", "java", "jdd", "test", "typed", or a name of
     * a class that has an init() method that returns a BDDFactory. If it fails,
     * it falls back to the "java" factory.</p>
     *
     * @param bddpackage BDD package string identifier
     * @param nodenum initial node table size
     * @param cachesize operation cache size
     * @return BDD factory object
     */
    public static BDDFactory init(String bddpackage, int nodenum, int cachesize) {
        return BDDFactory.init(bddpackage, nodenum, cachesize);
    }
}
