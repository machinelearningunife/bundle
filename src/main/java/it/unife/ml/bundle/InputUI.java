/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import it.unife.ml.bundle.BundleCLIImpl.CLIOption;
import static it.unife.ml.bundle.BundleCLIImpl.CLIOption.*;
import java.util.Properties;
import javax.validation.constraints.NotNull;

/**
 * An instance of this class is passed from the view to the controller. It
 * should contains all informations necessary to run an inference task.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class InputUI {

    protected Properties properties = new Properties();

    public void setTask(@NotNull String task) {
        properties.setProperty(CLIOption.TASK.getLongName(), task);
    }

    public void setIndividual(@NotNull String individual) {
        properties.setProperty("individual", individual);
    }

    public void setConcept(@NotNull String clazz) {
        properties.setProperty("class", clazz);
    }

    public void setSubject(@NotNull String subject) {
        properties.setProperty("subject", subject);
    }

    public void setProperty(@NotNull String property) {
        properties.setProperty("property", property);
    }

    public void setObject(@NotNull String object) {
        properties.setProperty("object", object);
    }

    public void setSubConcept(@NotNull String subConcept) {
        properties.setProperty("subConcept", subConcept);
    }

    public void setSuperConcept(@NotNull String superConcept) {
        properties.setProperty("superConcept", superConcept);
    }

    public void setHSTMethod(@NotNull String hstm) {
        properties.setProperty(CLIOption.HSTMETHOD.getLongName(), hstm);
    }

    public void setMaxExplanations(String maxExplanations) {
        properties.setProperty(CLIOption.MAX_EXPLANATION.getLongName(), maxExplanations);
    }

    public void setReasoningTimeout(@NotNull String reasoningTimeout) {
        properties.setProperty(CLIOption.TIMEOUT.getLongName(), reasoningTimeout);
    }

    public void setNoProb(@NotNull String noProb) {
        properties.setProperty(CLIOption.NOPROB.getLongName(), noProb);
    }

    public void setBDDFactory(@NotNull String bddfact) {
        properties.setProperty(CLIOption.BDD_FACTORY.getLongName(), bddfact);
    }

    public void setVerbose(@NotNull String verbose) {
        properties.setProperty(CLIOption.VERBOSE.getLongName(), verbose);
    }

    public void setIgnoreImports(@NotNull String ignoreImports) {
        properties.setProperty(CLIOption.IGNORE_IMPORTS.getLongName(), ignoreImports);
    }

    public void setTimeout(@NotNull String timeout) {
        properties.setProperty(TIMEOUT.getLongName(), timeout);
    }

    public void setRootOntology(@NotNull String rootOntology) {
        properties.setProperty("ontology", rootOntology);
    }

    public void setReasoner(@NotNull String reasoner) {
        properties.setProperty(REASONER.getLongName(), reasoner);
    }

    @NotNull
    public String getTask() {
        return properties.getProperty(CLIOption.TASK.getLongName());
    }

    public String getIndividual() {
        return properties.getProperty("individual");
    }

    public String getConcept() {
        return properties.getProperty("class");
    }

    public String getSubject() {
        return properties.getProperty("subject");
    }

    public String getProperty() {
        return properties.getProperty("property");
    }

    public String getObject() {
        return properties.getProperty("object");
    }

    public String getSubConcept() {
        return properties.getProperty("subConcept");
    }

    public String getSuperConcept() {
        return properties.getProperty("superConcept");
    }

    @NotNull
    public String getHSTMethod() {
        return properties.getProperty(CLIOption.HSTMETHOD.getLongName());
    }

    @NotNull
    public String getMaxExplanations() {
        return properties.getProperty(CLIOption.MAX_EXPLANATION.getLongName());
    }

    @NotNull
    public String getReasoningTimeout() {
        return properties.getProperty(CLIOption.TIMEOUT.getLongName());
    }

    @NotNull
    public String getNoProb() {
        return properties.getProperty(CLIOption.NOPROB.getLongName());
    }

    @NotNull
    public String getBDDFactory() {
        return properties.getProperty(CLIOption.BDD_FACTORY.getLongName());
    }

    @NotNull
    public String getVerbose() {
        return properties.getProperty(CLIOption.VERBOSE.getLongName());
    }

    @NotNull
    public String getIgnoreImports() {
        return properties.getProperty(CLIOption.IGNORE_IMPORTS.getLongName());
    }

    @NotNull
    public String getTimeout() {
        return properties.getProperty(TIMEOUT.getLongName());
    }

    @NotNull
    public String getRootOntology() {
        return properties.getProperty("ontology");
    }

    @NotNull
    public String getReasoner() {
        return properties.getProperty(REASONER.getLongName());
    }
}
