/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import static it.unife.ml.bundle.BundleCLIImpl.CLIOption.*;
import it.unife.ml.bundle.utilities.BundleUtilities;
import it.unife.ml.bundle.utilities.Constants.Timings;
import it.unife.ml.bundle.utilities.LogUtilities;
import it.unife.ml.bundle.utilities.BundleQueryResult;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import static it.unife.ml.probowlapi.utilities.GeneralUtils.safe;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Observable;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.validation.constraints.NotNull;
import org.apache.logging.log4j.Level;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class BundleCLIImpl extends AbstractBundleCLI {

    private final BundleModel bundleModel;

    public enum CLIOption {

        TASK("task", null, "inconsistent"),
        HELP("help", "h", null),
        VERBOSE("verbose", "v", "false"),
        IGNORE_IMPORTS("ignoreImports", null, "false"),
        UNSAT("unsat", null, null),
        ALL_UNSAT("allUnsat", null, null),
        INCONSISTENT("inconsistent", null, null),
        HIERARCHY("hierarchy", null, null),
        SUBCLASS("subclass", null, null),
        INSTANCE("instance", null, null),
        PROPERTY_VALUE("propertyValue", null, null),
        MAX_EXPLANATION("maxExplanations", "max", "" + Integer.MAX_VALUE),
        REASONER("reasoner", "r", "pellet"),
        HSTMETHOD("method", "m", "owlexp"),
        TIMEOUT("time", "t", "0s"),
        NOPROB("noProb", null, "false"),
        BDD_FACTORY("bddfact", "bf", "buddy");

        private final String longName;

        private final String shortName;

        private final String defaultValue;

        private CLIOption(@NotNull final String longName, final String shortName, final String defaultValue) {
            this.longName = longName;
            this.shortName = shortName;
            this.defaultValue = defaultValue;
        }

        /**
         * @return the longName
         */
        public final String getLongName() {
            return longName;
        }

        /**
         * @return the shortName
         */
        public String getShortName() {
            if (shortName != null && !shortName.isEmpty()) {
                return shortName;
            } else {
                return longName;
            }
        }

        public String toString() {
            return longName;
        }

        /**
         * @return the defaultValue
         */
        public String getDefaultValue() {
            return defaultValue;
        }

    }

    private final CommandLineParser parser = new DefaultParser();

    private CommandLine cmd;

    private InputUI userInput = new InputUI();

    private static final Logger logger = LoggerFactory.getLogger(BundleCLIImpl.class);

    public BundleCLIImpl(BundleModel bundleModel) {
        defineOptions();
        this.bundleModel = bundleModel;
    }

    @Override
    public void run(String[] args) {

        LogUtilities.addConsoleAppender(logger, Level.INFO);

        if (args.length == 0) {
            logger.error("Type 'bundle help' for usage.");
            return;
//            System.exit(1);
        }

        if (cmd == null) {
            try {
                cmd = parser.parse(getOptions(), args);
            } catch (ParseException ex) {
                logger.error("Parsing error. ", ex);
            }
        }

        if (cmd.hasOption("help") || cmd.getArgs()[0].equals("help")) {
            showHelp();
        } else {
            if (cmd.getArgs().length < 1) {
                System.out.println("Missing argument: FILE URI");
                showHelp(1); // Prints the help file and closes the program with an exit code of 1.
            }
            // create ViewProperties object with the parsed args
            OptionGroup taskOption = options.getOptionGroup(Option.builder(CLIOption.ALL_UNSAT.longName).build());
            String selectedTask = CLIOption.TASK.defaultValue;
            if (taskOption.getSelected() != null) {
                selectedTask = options.getOption(taskOption.getSelected()).getLongOpt();
            }
            userInput.setTask(selectedTask);
            if (selectedTask.equals(UNSAT.longName)) {
                // consider only the OWL Class
                String concept = cmd.getOptionValue(selectedTask);
                userInput.setConcept(concept);

            } else if (selectedTask.equals(SUBCLASS.longName)) {
                // Consider the OWL subClass and the OWL superClass
                String subConcept = cmd.getOptionValues(selectedTask)[0];
                String superConcept = cmd.getOptionValues(selectedTask)[1];
                userInput.setSubConcept(subConcept);
                userInput.setSuperConcept(superConcept);

            } else if (selectedTask.equals(INSTANCE.longName)) {
                // Consider the individual and its OWL Class
                String individual = cmd.getOptionValues(selectedTask)[0];
                String concept = cmd.getOptionValues(selectedTask)[1];
                userInput.setIndividual(individual);
                userInput.setConcept(concept);
            } else if (selectedTask.equals(PROPERTY_VALUE.longName)) {
                // Consider subject, property and object
                String subject = cmd.getOptionValues(selectedTask)[0];
                String property = cmd.getOptionValues(selectedTask)[1];
                String object = cmd.getOptionValues(selectedTask)[2];
                userInput.setSubject(subject);
                userInput.setProperty(property);
                userInput.setObject(object);
            }

            // verbose option value
            String verbose = cmd.hasOption(VERBOSE.longName)
                    ? "true" : VERBOSE.defaultValue;
            userInput.setVerbose(verbose);
            // ignore-imports option value
            String ignoreImports = cmd.hasOption(IGNORE_IMPORTS.longName)
                    ? "true" : IGNORE_IMPORTS.defaultValue;
            userInput.setIgnoreImports(ignoreImports);
            // method option value
            String method = cmd.getOptionValue(HSTMETHOD.longName, HSTMETHOD.defaultValue);
            userInput.setHSTMethod(method);
            // max explanation option value
            String maxExpl = cmd.getOptionValue(MAX_EXPLANATION.longName, MAX_EXPLANATION.defaultValue);
            userInput.setMaxExplanations(maxExpl);
            // timeout option value
            String timeout = cmd.getOptionValue(TIMEOUT.longName, TIMEOUT.defaultValue);
            userInput.setTimeout(timeout);
            // noProb option value
            String noProb = cmd.hasOption(CLIOption.NOPROB.longName)
                    ? "true" : CLIOption.NOPROB.defaultValue;
            userInput.setNoProb(noProb);
            // reasoner option
            String reasoner = cmd.getOptionValue(REASONER.toString(), REASONER.defaultValue);
            userInput.setReasoner(reasoner);
            // BDD Factory option
            String bddFactory = cmd.getOptionValue(BDD_FACTORY.toString(), BDD_FACTORY.defaultValue);
            userInput.setBDDFactory(bddFactory);
            // get root ontology
            String rootOntology = cmd.getArgs()[0];
            userInput.setRootOntology(rootOntology);

            // notify the controller
            setChanged();
            notifyObservers();
        }

    }

    @Override
    public void showHelp() {
        showHelp(0);
    }

    public void showHelp(int status) {
        StringWriter out = new StringWriter();
        PrintWriter pw = new PrintWriter(out);

        HelpFormatter formatter = new HelpFormatter();
        String header = "\nBundle: Explains one or more inferences in a given "
                + "ontology including ontology inconsistency\n";
        String footer = "\n\nFor any issues please contact Riccardo Zese <riccardo.zese@unife.it>";
        formatter.printHelp(pw,
                //                formatter.getWidth(),
                120,
                "\nbundle [OPTIONS] <FILE URI>\n",
                header, getOptions(),
                formatter.getLeftPadding(),
                formatter.getDescPadding(),
                footer,
                false);
        pw.flush();
        logger.info(out.toString());
//        System.exit(status);
    }

    public AbstractBundleUI getUI(String[] args) {

        try {
            // parse the command line arguments
            cmd = parser.parse(getOptions(), args, true);
            if (cmd.hasOption("gui")) {
                return new BundleGUI();
            } else {
                return this;
            }
        } catch (ParseException ex) {
            logger.error(ex.getMessage());
            return null;
        }
    }

    private void defineOptions() {

        logger.debug("START - Defining possible options");
        Option help = new Option("h", "help", false, "print this message");
        getOptions().addOption(help);

        Option gui = new Option("gui", "run GUI");
        getOptions().addOption(gui);

        // mutually exclusive tasks - BEGIN
        OptionGroup taskOption = new OptionGroup();
        Option option = Option.builder(UNSAT.getShortName())
                .longOpt(UNSAT.getLongName())
                .desc("Explain why the given class C is unsatisfiable")
                //                .required(false)
                .argName("C")
                .hasArg()
                .build();
        taskOption.addOption(option);

        option = Option.builder(ALL_UNSAT.getShortName())
                .longOpt(ALL_UNSAT.getLongName())
                .desc("Explain all unsatisfiable classes")
                //                .required(false)
                .hasArg(false)
                .build();
        taskOption.addOption(option);

        option = Option.builder(INCONSISTENT.getShortName())
                .longOpt(INCONSISTENT.getLongName())
                .desc("Explain why the ontology is inconsistent")
                //                .required(false)
                .hasArg(false)
                .build();
        taskOption.addOption(option);

        option = Option.builder(HIERARCHY.getShortName())
                .longOpt(HIERARCHY.getLongName())
                .desc("Print all explanations for the class hierarchy")
                //                .required(false)
                .hasArg(false)
                .build();
        taskOption.addOption(option);

        option = Option.builder(SUBCLASS.getShortName())
                .longOpt(SUBCLASS.getLongName())
                .desc("Explain why C is a subclass of D")
                .numberOfArgs(2)
                .valueSeparator(',')
                .argName("C,D")
                //                .required(false)
                .build();
        taskOption.addOption(option);

        option = Option.builder(INSTANCE.getShortName())
                .longOpt(INSTANCE.getLongName())
                .desc("Explain why i is an instance of C")
                .numberOfArgs(2)
                .valueSeparator(',')
                .argName("i,C")
                //                .required(false)
                .build();
        taskOption.addOption(option);

        option = Option.builder(PROPERTY_VALUE.getShortName())
                .longOpt(PROPERTY_VALUE.getLongName())
                .desc("Explain why s has value o for property p")
                .numberOfArgs(3)
                .valueSeparator(',')
                .argName("s,p,o")
                //                .required(false)
                .build();
        taskOption.setRequired(false);
        taskOption.addOption(option);
        getOptions().addOptionGroup(taskOption);
        // mutually exclusive tasks - END

        option = Option.builder(REASONER.getShortName())
                .longOpt(REASONER.getLongName())
                .desc("Inner reasoner to be used. Bundle currently supports: "
                        + "Pellet, Hermit, JFact, FaCT++, and the TRILL framework. The valid arguments "
                        + "for this option are: "
                        + "\"pellet\", \"hermit\", \"jfact\", \"fact++\", \"trill\", \"trillp\" or \"tornado\". "
                        + "NOTE: to use the reasoners trill, trillp and tornado you must first install "
                        + "SWI-Prolog (https://www.swi-prolog.org/) and the "
                        + "TRILL framework (http://ml.unife.it/trill-framework/) on your system. "
                        + "The reasoners trillp and tornado only return the probability of the query. "
                        + "Default value: pellet")
                .hasArg()
                .argName("pellet | hermit | jfact | fact++ | trill | trillp | tornado")
                .required(false)
                .build();
        options.addOption(option);

        option = Option.builder(HSTMETHOD.getShortName())
                .longOpt(HSTMETHOD.getLongName())
                .hasArg()
                .argName("glass | black | owlexp")
                .desc("Method that will be used to generate explanations. Default value: owlexp")
                .required(false)
                .build();
        getOptions().addOption(option);

        option = Option.builder(MAX_EXPLANATION.getShortName())
                .longOpt(MAX_EXPLANATION.getLongName())
                .type(Integer.class)
                .desc("Maximum number of generated explanations for each inference. "
                        + "The provided number must be a positive integer. "
                        + "Default value: " + Integer.MAX_VALUE)
                .hasArg()
                .required(false)
                .build();
        getOptions().addOption(option);

        option = Option.builder(TIMEOUT.getShortName())
                .longOpt(TIMEOUT.getLongName())
                .desc("Maximum time allowed for the inference, 0 for unlimited time. Format: [Xh][Ym][Zs]. Default value 0s")
                .required(false)
                .hasArg()
                .build();
        getOptions().addOption(option);

        option = Option.builder(NOPROB.getShortName())
                .longOpt(NOPROB.getLongName())
                .desc("Disable the computation of the probability")
                .required(false)
                .hasArg(false)
                .build();
        getOptions().addOption(option);

        option = Option.builder(BDD_FACTORY.getShortName())
                .longOpt(BDD_FACTORY.getLongName())
                .desc("Set the BDD Factory, possible values can be "
                        + "\"buddy\", \"cudd\", \"j\", \"java\", \"jdd\" or a name of a"
                        + " class that has an init() method "
                        + "that returns a BDDFactory. Note: \"cal\" is not supported, "
                        + "use \"buddy\" for better performances. If the loading fails "
                        + "the \"java\" factory will be used. Default value: buddy.")
                .hasArg()
                .argName("buddy | cudd | j | java | jdd")
                .required(false)
                .build();
        getOptions().addOption(option);

        option = Option.builder(VERBOSE.getShortName())
                .longOpt(VERBOSE.getLongName())
                .hasArg(false)
                .desc("Print detailed exceptions and messages about the progress.")
                .required(false)
                .build();
        getOptions().addOption(option);
        // ignore-imports option
        option = Option.builder(IGNORE_IMPORTS.getShortName())
                .longOpt(IGNORE_IMPORTS.getLongName())
                .hasArg(false)
                .desc("Ignore imported ontologies.")
                .required(false)
                .build();
        options.addOption(option);

        logger.debug("END - Defining possible options");
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof BundleModel) {
            BundleModelEvent event = (BundleModelEvent) arg;
            switch (event) {
                case QUERY_COMPUTED:
                    ProbabilisticReasonerResult result = bundleModel.getQueryResult();
                    printResult(result);
                    break;
                case LOADED_ONTOLOGY:
                    throw new UnsupportedOperationException();
//                    break;
                case ERROR:
                    throw new UnsupportedOperationException();
//                    break;
                default:
                    throw new UnsupportedOperationException();
            }
        }
    }

    @Override
    public void errorLoadingOntology(Exception e) {
        logger.error("Can't Load Ontology. ", e);
    }

    /**
     * @return the options
     */
    public Options getOptions() {
        return options;
    }

    public InputUI getUserInput() {
        return userInput;
    }

    @Override
    public void showError(String message) {
        logger.error(message);
        throw new RuntimeException(message);
    }

    /**
     * Prints useful statistics if in verbose mode.
     */
    private void printStatistics() {
        // TO DO

//        if (!verbose) {
//            return;
//        }
//
//        Timer timer = timers.getTimer("explain");
//        if (timer != null) {
//            // TODO change with logger????
//            logger.info("");
//            logger.info(("========== Statistics ==========");
//            //verbose("Subclass relations   : " + timer.getCount(), log);
//            logger.info(("Multiple explanations: " + multipleExpCount, log);
//            logger.info(("Single explanation     ", log);
//            logger.info((" with multiple axioms: " + multiAxiomExpCount, log);
//            logger.info(("Error explaining     : " + errorExpCount, log);
//            logger.info(("Average time         : " + timer.getAverage() + "ms", log);
//
//        }
    }

    /**
     * Prints final results.
     *
     * @param result the results to print
     */
    private void printResult(ProbabilisticReasonerResult result) {
        logger.info("");
        logger.info("============ Result ============");
        if (result instanceof BundleQueryResult) {
            String reasoner = cmd.getOptionValue(REASONER.toString(), REASONER.defaultValue);
            if (!(reasoner.equals("trillp")||reasoner.equals("tornado"))) {
                logger.info("N. of Explanations: " + ((BundleQueryResult) result).getNumberOfExplanations());
            }
        }
        if (bundleModel.isProbabilistic()) {
            if (result instanceof BundleQueryResult) {
                if (userInput.getVerbose().equals("true")) {
                    logger.info("Probabilistic axioms used:");
                    safe(((BundleQueryResult) result).getUsedProbabilisticAxioms()).entrySet().forEach((entry) -> {
                        logger.info("\t" + BundleUtilities.getManchesterSyntaxString(entry.getKey()) + " - prob: "
                                + entry.getValue());
                    });
                }
                logger.info("");
            }
            logger.info("Probability of the query: " + result.getQueryProbability().getValue());
        }
        logger.info("");

        logger.info("Execution time (ms): " + bundleModel.getTiming(Timings.MAIN));
//        logger.info("Execution time (ms): " + timers.mainTimer.getTotal());

        logger.info("");
        logger.info("================================");
        logger.info("");

    }

}
