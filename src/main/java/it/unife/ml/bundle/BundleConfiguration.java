/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import it.unife.ml.probowlapi.core.Reasoner;
import net.sf.javabdd.BDDFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class BundleConfiguration {

    // maximum number of digits after the dot during the calculations
    private int numericalAccuracy;

    /**
     * Method used to retrieve an explanation during the Hitting Set Tree
     * algorithm. Default: OWLEXPLANATION
     */
//    protected HSTMethod hstMethod = OWLEXPLANATION;
    private Reasoner externalReasoner;

    /**
     * This value must be true if we want to compute the probability.
     */
    private boolean probabilistic;

    /**
     * The type of BDD Factory. Possible values: BUDDY, CUDD, CAL, J, U, JDD.
     */
    private BDDFactoryType bddFType;

    private int maxExplanations;

    /**
     * Reasoning time in milliseconds.
     */
    private long reasoningTimeout;

    private OWLOntology rootOntology;

    /**
     * If set to true all the explanations (and other messages) are logged. The
     * destinations of the logs are configured through the property file of the
     * logging service. Default value is false.
     */
    private boolean verbose;

    /**
     * If true means that the reasoner should obtain the explanations for the
     * inconsistency of the ontology.
     */
    private boolean checkConsistency;

    public BundleConfiguration(int numericalAccuracy,
            Reasoner externalReasoner,
            boolean probabilistic,
            BDDFactoryType bddFType,
            int maxExplanations,
            long reasoningTimeout,
            OWLOntology rootOntology,
            boolean checkConsistency,
            boolean verbose) {
        this.numericalAccuracy = numericalAccuracy;
        this.externalReasoner = externalReasoner;
        this.probabilistic = probabilistic;
        this.bddFType = bddFType;
        this.maxExplanations = maxExplanations;
        this.reasoningTimeout = reasoningTimeout;
        this.rootOntology = rootOntology;
        this.checkConsistency = checkConsistency;
        this.verbose = verbose;

    }

    /**
     * @return the numericalAccuracy
     */
    public int getNumericalAccuracy() {
        return numericalAccuracy;
    }

    /**
     * @return the externalReasoner
     */
    public Reasoner getExternalReasoner() {
        return externalReasoner;
    }

    /**
     * @return the probabilistic
     */
    public boolean isProbabilistic() {
        return probabilistic;
    }

    /**
     * @return the bddFType
     */
    public BDDFactoryType getBddFType() {
        return bddFType;
    }

    /**
     * @return the maxExplanations
     */
    public int getMaxExplanations() {
        return maxExplanations;
    }

    /**
     * @return the reasoningTimeout
     */
    public long getReasoningTimeout() {
        return reasoningTimeout;
    }

    /**
     * @return the rootOntology
     */
    public OWLOntology getRootOntology() {
        return rootOntology;
    }

    /**
     * @return the verbose
     */
    public boolean isVerbose() {
        return verbose;
    }

    /**
     * @return the checkConsistency
     */
    public boolean isCheckConsistency() {
        return checkConsistency;
    }

}
