/**
 * This file is part of BUNDLE.
 *
 * BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 * BUNDLE can be used both as module and as standalone.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * BUNDLE and all its parts are distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import it.unife.ml.bundle.trill.TRILLPReasoner;
import it.unife.ml.bundle.trill.TORNADOReasoner;
import it.unife.ml.bundle.task.UnsatClassTask;
import it.unife.ml.bundle.task.SubClassTask;
import it.unife.ml.bundle.exception.TaskCreationException;
import static it.unife.ml.bundle.BundleCLIImpl.CLIOption.*;
import it.unife.ml.bundle.exception.IllegalValueException;
import it.unife.ml.bundle.factpp.FactppInitializer;
import it.unife.ml.bundle.task.AllHierarchyTask;
import it.unife.ml.bundle.task.AllUnsatTask;
import it.unife.ml.bundle.task.InconsistencyTask;
import it.unife.ml.bundle.task.InstanceTask;
import it.unife.ml.bundle.task.PropertyValueTask;
import it.unife.ml.bundle.task.Task;
import it.unife.ml.bundle.trill.TRILLReasoner;
import it.unife.ml.bundle.utilities.BundleUtilities;
import it.unife.ml.bundle.utilities.Constants.HSTMethod;
import static it.unife.ml.bundle.utilities.Constants.ReasonerName.*;
import java.io.IOException;
import java.util.Observable;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.RemoveImport;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.util.mansyntax.ManchesterOWLSyntaxParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.manchester.cs.factplusplus.owlapiv3.FaCTPlusPlusReasonerFactory;
import uk.ac.manchester.cs.jfact.JFactFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class BundleControllerImpl extends AbstractBundleController {

    OWLOntologyManager manager;

    private static final Logger logger = LoggerFactory.getLogger(BundleControllerImpl.class);

    public BundleControllerImpl(AbstractBundleUI ui, BundleModel bundleModel) {
        super(ui, bundleModel);
        manager = OWLManager.createOWLOntologyManager();
    }

    @Override
    public void update(Observable o, Object o1) {
        if (o == ui) {
            // argument parsed
            if (ui instanceof AbstractBundleUI) {
                InputUI userInput = ui.getUserInput();
                // validate userInput
                ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
                Validator validator = factory.getValidator();
                Set<ConstraintViolation<InputUI>> violations = validator.validate(userInput);
                for (ConstraintViolation<InputUI> violation : violations) {
                    ui.showError(violation.getPropertyPath() + " " + violation.getMessage());
                }

                // create task
                try {
                    Task task = createTask(userInput);
                    task.execute(bundleModel);
                } catch (TaskCreationException e) {
                    ui.showError(e.getMessage());
                }
            }
        } else if (o == bundleModel) {
            // nothing
        }
    }

    private Task createTask(InputUI userInput) throws TaskCreationException {
        String taskId = userInput.getTask();
        OWLDataFactory df = manager.getOWLDataFactory();
        Task task;

        Imports importClosure;
        if (Boolean.parseBoolean(userInput.getIgnoreImports())) {
            importClosure = Imports.EXCLUDED;
        } else {
            importClosure = Imports.INCLUDED;
        }

        OWLOntology rootOntology;
        try {
            rootOntology = manager.loadOntology(IRI.create(userInput.getRootOntology()));
            if (importClosure == Imports.EXCLUDED) {
//                for (OWLOntology importedOnt : rootOntology.getImports()) {
//                    manager.removeOntology(importedOnt);
//                }
                for (OWLImportsDeclaration importDecl : rootOntology.getImportsDeclarations()) {
                    manager.applyChange(new RemoveImport(rootOntology, importDecl));
                }
                if (rootOntology.getImports().size() > 0) {
                    throw new TaskCreationException("Unable to remove imported ontologies");
                }
            }

        } catch (OWLOntologyCreationException ex) {
            throw new TaskCreationException(ex);
        }

        // Default: inconsistent task
        task = new InconsistencyTask();

        if (taskId.equals(ALL_UNSAT.toString())) {
            task = new AllUnsatTask();
        } else if (taskId.equals(HIERARCHY.toString())) {
            task = new AllHierarchyTask();
        } else if (taskId.equals(INSTANCE.toString())) {
            OWLClass owlClass = df.getOWLClass(IRI.create(userInput.getConcept()));
            if (!rootOntology.containsClassInSignature(owlClass.getIRI(), importClosure)) {
                throw new TaskCreationException("Not an existing class: " + owlClass.getIRI());
            }
            OWLNamedIndividual owlInd = df.getOWLNamedIndividual(IRI.create(userInput.getIndividual()));
            if (!rootOntology.containsIndividualInSignature(owlInd.getIRI(), importClosure)) {
                throw new TaskCreationException("Not an existing individual: " + owlInd.getIRI());
            }
            task = new InstanceTask(owlClass, owlInd);
        } else if (taskId.equals(PROPERTY_VALUE.toString())) {
            OWLNamedIndividual subjectInd = df.getOWLNamedIndividual(IRI.create(userInput.getSubject()));
            if (!rootOntology.containsIndividualInSignature(subjectInd.getIRI(), importClosure)) {
                throw new TaskCreationException("Not an existing individual: " + subjectInd.getIRI());
            }
            // create property and check if it exists in ontology
            OWLProperty owlp;
            IRI owlpIRI = IRI.create(userInput.getProperty());
            if (rootOntology.containsObjectPropertyInSignature(owlpIRI, importClosure)) {
                owlp = df.getOWLObjectProperty(owlpIRI);
            } else if (rootOntology.containsDataPropertyInSignature(owlpIRI, importClosure)) {
                owlp = df.getOWLDataProperty(owlpIRI);
            } else {
                throw new TaskCreationException("Not an existing property: " + owlpIRI);
            }

            OWLObject object;
            if (owlp.isOWLObjectProperty()) {
                IRI objectIRI = IRI.create(userInput.getObject());
                object = df.getOWLNamedIndividual(objectIRI);
                if (!rootOntology.containsIndividualInSignature(objectIRI, importClosure)) {
                    throw new TaskCreationException("Not an existing individual: " + objectIRI);
                }
            } else {
                ManchesterOWLSyntaxParser parser = OWLManager.createManchesterParser();
                parser.setStringToParse(userInput.getObject());
                object = parser.parseLiteral(null);
            }

            task = new PropertyValueTask(subjectInd, owlp, object);
        } else if (taskId.equals(SUBCLASS.toString())) {
            OWLClass subClass = df.getOWLClass(IRI.create(userInput.getSubConcept()));
            if (!rootOntology.containsClassInSignature(subClass.getIRI(), importClosure)) {
                throw new TaskCreationException("Not an existing class: " + subClass.getIRI());
            }

            OWLClass superClass = df.getOWLClass(IRI.create(userInput.getSuperConcept()));
            if (!rootOntology.containsClassInSignature(superClass.getIRI(), importClosure)) {
                throw new TaskCreationException("Not an existing class: " + superClass.getIRI());
            }

            task = new SubClassTask(subClass, superClass);
        } else if (taskId.equals(UNSAT.toString())) {
            OWLClass owlClass = df.getOWLClass(IRI.create(userInput.getConcept()));
            if (!rootOntology.containsClassInSignature(owlClass.getIRI(), importClosure)) {
                throw new TaskCreationException("Not an existing class: " + owlClass.getIRI());
            }
            task = new UnsatClassTask(owlClass);
        }

        try {
            task.setRootOntology(rootOntology);
            task.setImportClosure(importClosure);
            task.setMaxExplanations(Integer.parseUnsignedInt(userInput.getMaxExplanations()));
            task.setNoProb(Boolean.parseBoolean(userInput.getNoProb()));
            task.setReasoningTimeout(BundleUtilities.convertTimeValue(userInput.getTimeout()));
            task.setVerbose(Boolean.parseBoolean(userInput.getVerbose()));

            setBDDFactoryType(task, userInput.getBDDFactory());
            setHSTMethod(task, userInput.getHSTMethod());
//            setReasonerFactory(task, userInput.getReasoner());
            setExternalReasoner(task, userInput.getReasoner());
        } catch (Exception ex) {
            throw new TaskCreationException(ex);
        }

        return task;
    }

    private void setBDDFactoryType(Task task, String bddFTypeStr) {

        switch (bddFTypeStr.toLowerCase()) {
            case "buddy":
                task.setBDDFactoryType(BDDFactoryType.BUDDY);
                break;
            case "cudd":
                task.setBDDFactoryType(BDDFactoryType.CUDD);
                break;
            case "cal":
                task.setBDDFactoryType(BDDFactoryType.CAL);
                break;
            case "j":
            case "java":
                task.setBDDFactoryType(BDDFactoryType.J);
                break;
            case "jdd":
                task.setBDDFactoryType(BDDFactoryType.JDD);
                break;
            case "u":
                task.setBDDFactoryType(BDDFactoryType.U);
                break;
            default:
                throw new IllegalValueException("Unknown BDD Factory: " + bddFTypeStr);
        }

    }

    private void setHSTMethod(Task task, String hstMethod) {

        switch (hstMethod.toLowerCase()) {
            case "glass":
                task.setHSTMethod(HSTMethod.GLASSBOX);
                break;
            case "black":
                task.setHSTMethod(HSTMethod.BLACKBOX);
                break;
            case "owlexp":
                task.setHSTMethod(HSTMethod.OWLEXPLANATION);
                break;
            default:
                throw new IllegalValueException("Unknown Hitting Set Tree method: " + hstMethod);
        }

    }

//    private void setReasonerFactory(Task task, String reasoner) throws IOException {
//
//        switch (reasoner) {
//            case "pellet":
//                task.setReasonerFactory(PelletReasonerFactory.getInstance());
//                break;
//            case "hermit":
//                task.setReasonerFactory(new org.semanticweb.HermiT.ReasonerFactory());
//                break;
//            case "jfact":
//                task.setReasonerFactory(new JFactFactory());
//                break;
//            case "fact++":
//                // load native library
//                FactppInitializer.init();
//                task.setReasonerFactory(new FaCTPlusPlusReasonerFactory());
//                break;
//            case "trill":
//                task.setHSTMethod(HSTMethod.NONE);
//                TRILLReasoner trill = new TRILLReasoner();
//                trill.setProbabilistic(!task.isNoProb());
//                trill.setVerbose(task.isVerbose());
//                task.setProbabilisticReasoner(trill);
//                break;
//            case "trillp":
//                if (task.isNoProb()) {
//                    throw new IllegalValueException("Unsupported reasoner for non-probabilistic query: " + reasoner +
//                            "\nPlease, remove -noProb flag or consider using TRILL instead (-r trill)");
//                }
//                task.setHSTMethod(HSTMethod.NONE);
//                TRILLPReasoner trillp = new TRILLPReasoner();
//                trillp.setVerbose(task.isVerbose());
//                task.setProbabilisticReasoner(trillp);
//                break;
//            case "tornado":
//                if (task.isNoProb()) {
//                    throw new IllegalValueException("Unsupported reasoner for non-probabilistic query: " + reasoner +
//                            "\nPlease, remove -noProb flag or consider using TRILL instead (-r trill)");
//                }
//                task.setHSTMethod(HSTMethod.NONE);
//                TORNADOReasoner tornado = new TORNADOReasoner();
//                tornado.setVerbose(task.isVerbose());
//                task.setProbabilisticReasoner(tornado);
//                break;
//            default:
//                throw new IllegalValueException("Unsupported reasoner: " + reasoner);
//        }
//    }
    private void setExternalReasoner(Task task, String reasoner) throws IOException {

        switch (reasoner) {
            case "pellet":
                task.setExternalReasonerName(PELLET);
                break;
            case "hermit":
                task.setExternalReasonerName(HERMIT);
                break;
            case "jfact":
                task.setExternalReasonerName(JFACT);
                break;
            case "fact++":
                task.setExternalReasonerName(FACTPP);
                break;
            case "trill":
                task.setHSTMethod(HSTMethod.NONE);
                task.setExternalReasonerName(TRILL);
                break;
            case "trillp":
                if (task.isNoProb()) {
                    throw new IllegalValueException("Unsupported reasoner for non-probabilistic query: " + reasoner
                            + "\nPlease, remove -noProb flag or consider using TRILL instead (-r trill)");
                }
                task.setHSTMethod(HSTMethod.NONE);
                task.setExternalReasonerName(TRILLP);
                break;
            case "tornado":
                if (task.isNoProb()) {
                    throw new IllegalValueException("Unsupported reasoner for non-probabilistic query: " + reasoner
                            + "\nPlease, remove -noProb flag or consider using TRILL instead (-r trill)");
                }
                task.setHSTMethod(HSTMethod.NONE);
                task.setExternalReasonerName(TORNADO);
                break;
            default:
                throw new IllegalValueException("Unsupported reasoner: " + reasoner);
        }
    }

}
