/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.factpp;

import it.unife.ml.bundle.BundleControllerImpl;
import it.unife.ml.bundle.utilities.LoadNativeLibrary;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class FactppInitializer {

    private static final Logger logger = LoggerFactory.getLogger(FactppInitializer.class);
    
    protected static File libFile;

    static {
        String libraryPath = getFactPPLibraryPath();
        logger.debug("Trying loading library {}", libraryPath);

//        catch (java.lang.UnsatisfiedLinkError x) {
//            // Cannot find library, try loading it from the current directory...
//            String libname = System.mapLibraryName("FaCTPlusPlusJNI");
//            String currentdir = System.getProperty("user.dir", ".");
//            String sep = System.getProperty("file.separator", "/");
//            String filename = currentdir+sep+libname;
//            System.load(filename);
//            
//        }
//        String libname = System.mapLibraryName("FaCTPlusPlusJNI");
//        String currentdir = System.getProperty("user.dir", ".");
//        String sep = System.getProperty("file.separator", "/");
//        String filename = currentdir + sep + libname;
//        System.load(filename);
//        logger.debug("Loaded library {}", filename);



        try {
            libFile = LoadNativeLibrary.createTempFile(libraryPath);
            System.load(libFile.getAbsolutePath());
            logger.debug("Loaded library {}", libFile.getAbsolutePath());
        } catch (IOException ex) {
            logger.error("Cannot initialize Fact++", ex);
        }

    }

    public static String getFactPPLibraryPath() {
        // get architecture
        String libraryPath = "/lib/native/";

        if (System.getProperty("os.arch").endsWith("64")) { // 64 bit
            libraryPath += "64bit/";
        } else { // 32 bit
            libraryPath += "32bit/";
        }

        if (SystemUtils.IS_OS_LINUX) {
            libraryPath += "libFaCTPlusPlusJNI.so";
        } else if (SystemUtils.IS_OS_MAC_OSX) {
            libraryPath += "libFaCTPlusPlusJNI.jnilib";
        } else if (SystemUtils.IS_OS_WINDOWS) {
            libraryPath += "FaCTPlusPlusJNI.dll";
        }

        return libraryPath;
    }

    public static void init() {
    }

    /**
     * @return the libFile
     */
    public static File getLibFile() {
        return libFile;
    }

}
