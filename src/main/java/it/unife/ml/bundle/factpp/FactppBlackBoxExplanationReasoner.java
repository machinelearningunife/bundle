/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.factpp;

import it.unife.ml.bundle.explreasoner.BlackBoxExplanationReasoner;
import uk.ac.manchester.cs.factplusplus.owlapiv3.FaCTPlusPlusReasonerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class FactppBlackBoxExplanationReasoner extends BlackBoxExplanationReasoner {

    public FactppBlackBoxExplanationReasoner() {
        super(new FaCTPlusPlusReasonerFactory());
    }

    @Override
    public void init() throws ExceptionInInitializerError {
        FactppInitializer.init();
        super.init();
    }

    @Override
    public void dispose() {
        // There are problems with the dispose of FactPlusPlus reasoner
    }

}
