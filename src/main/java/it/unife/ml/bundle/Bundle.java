/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import com.google.common.collect.HashBiMap;
import it.unife.ml.bundle.bdd.BDDFactory2;
import it.unife.ml.bundle.utilities.BundleUtilities;
import it.unife.ml.bundle.utilities.BundleQueryResult;
import it.unife.ml.math.ApproxDouble;
import it.unife.ml.bundle.exception.IllegalValueException;
import it.unife.ml.bundle.explreasoner.AbstractExplanationReasoner;
import it.unife.ml.bundle.explreasoner.OWLExplanationExplanationReasoner;
import it.unife.ml.bundle.explreasoner.Verbozer;
import it.unife.ml.probowlapi.core.ProbabilisticExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import static it.unife.ml.probowlapi.utilities.GeneralUtils.safe;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import net.sf.javabdd.JFactory;
import org.mindswap.pellet.utils.Timer;
import org.mindswap.pellet.utils.Timers;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.unife.ml.bundle.utilities.Constants.Timings;
import it.unife.ml.bundle.utilities.LogUtilities;
import it.unife.ml.probowlapi.core.ExplanationReasoner;
import it.unife.ml.probowlapi.core.ExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ProbabilisticExplanationReasoner;
import it.unife.ml.probowlapi.core.ProbabilisticReasoner;
import it.unife.ml.probowlapi.core.Reasoner;
import org.apache.logging.log4j.Level;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class Bundle extends AbstractExplanationReasoner implements ProbabilisticExplanationReasoner {

    private static final Logger logger = LoggerFactory.getLogger(Bundle.class);

    protected Timers timers;

    // maximum number of digits after the dot during the calculations
    protected int accuracy = 5;

    /**
     * Method used to retrieve an explanation during the Hitting Set Tree
     * algorithm. Default: OWLEXPLANATION
     */
//    protected HSTMethod hstMethod = OWLEXPLANATION;
    protected Reasoner externalReasoner;

    /**
     * This value must be true if we want to compute the probability.
     */
    private boolean probabilistic = true;

    /**
     * The type of BDD Factory. Possible values: BUDDY, CUDD, CAL, J, U, JDD.
     */
    private BDDFactoryType bddFType = BDDFactoryType.BUDDY;

    private BDDFactory bddF;
    /**
     * When building the BDD we associate each axiom with and integer value
     * assigned by BDDFactory,varNum();
     */
    private HashBiMap<OWLAxiom, Integer> usedProbabilisticAxioms;

//    protected OWLReasoner reasoner;
    protected Map<OWLAxiom, ApproxDouble> pMap;
    
    private BundleConfiguration config;

    public Bundle(BundleConfiguration config) {
        super();
        this.config = config;
        this.accuracy = config.getNumericalAccuracy();
        this.bddFType = config.getBddFType();
        this.externalReasoner = config.getExternalReasoner();
        this.maxExplanations = config.getMaxExplanations();
        this.probabilistic = config.isProbabilistic();
        this.reasoningTimeout = config.getReasoningTimeout();
        this.rootOntology = config.getRootOntology();
        this.verbose = config.isVerbose();
    }

    /**
     *
     * @param query
     * @return an object of type it.unife.ml.bundle.utilities.BundleQueryResult containing the results of the query
     * @throws org.semanticweb.owlapi.model.OWLException
     * @throws ObjectNotInitializedException
     */
    @Override
    public BundleQueryResult computeQuery(OWLAxiom query)
            throws OWLException, ObjectNotInitializedException {
        if (!initialized) {
            String msg = "BUNDLE is not correctly initialized.";
            logger.error(msg);
            throw new ObjectNotInitializedException(msg);
        } else {
            BundleQueryResult result;
            if (isUseProbabilisticExplanationReasoner()) {
                ProbabilisticExplanationReasonerResult reasonerResult = 
                        ((ProbabilisticExplanationReasoner) externalReasoner).computeExplainQuery(query);
                if (isProbabilistic()) {
                    result = new BundleQueryResult(reasonerResult, ((ProbabilisticExplanationReasoner) externalReasoner).getpMap());
                    result.collectUsedProbabilisticAxioms(pMap);
                } else {
                    result = new BundleQueryResult(reasonerResult);
                }
                timers.mainTimer.stop();
                result.setTimers(timers);
            } else if (isUseProbabilisticReasoner()) {
//                timers.mainTimer.start();
                ProbabilisticReasonerResult reasonerResult = ((ProbabilisticReasoner) externalReasoner).computeQuery(query);
                result = new BundleQueryResult(reasonerResult);
                timers.mainTimer.stop();
                result.setTimers(timers);
            } else {
                ExplanationReasonerResult explanations = explainAxiom(query);
                if (isProbabilistic()) {
                    result = computeProbability(explanations);
                } else {
                    result = new BundleQueryResult(explanations);
                }

            }
            return result;
        }
    }

    @Override
    public void dispose() {
//        if (reasonerFactory instanceof FaCTPlusPlusReasonerFactory) {
//            // There are problems with the dispose of FactPlusPlus reasoner
//            // reasoner.dispose()
//        } else {
//            if (isUseExplainationReasoner()) {
//                explanationReasoner.dispose();
//            } else if (isUseProbabilisticReasoner()) {
//                probabilisticReasoner.dispose();
//            } else {
//                reasoner.dispose();
//            }
//        }
        externalReasoner.dispose();
        if (pMap != null) {
            pMap.clear();
        }
        pMap = null;
        if (usedProbabilisticAxioms != null) {
            usedProbabilisticAxioms.clear();
        }
        usedProbabilisticAxioms = null;
        timers = null;

        if (getBddF() != null) {
            getBddF().done();
        }
    }

    @Override
    public void init() throws ExceptionInInitializerError {

        if (getTimers() == null) {
            timers = new Timers();
        }

        if (rootOntology == null) {
            String msg = "rootOntology is null";
            logger.error(msg);
            throw new ExceptionInInitializerError(msg);
        }

        // check if it's Pellet Reasoner. For Pellet it should be preferable to
        // use the class GlassBoxExplanation (it is faster than BlackBox)
//        if (getHstMethod() == GLASSBOX && !(reasonerFactory instanceof PelletReasonerFactory)) {
//            String msg = "GlassBox HST method can be used only with Pellet";
//            logger.error(msg);
//            throw new IncompatibleHSTMethod(msg);
//        }
//        if (getHstMethod() == GLASSBOX) {
//
//            BundleGlassBoxExplanation.setup();
////            GlassBoxExplanation.setup();
//        }
        if (externalReasoner == null) {
            throw new ExceptionInInitializerError("No reasoner was set!");
        }

        // start the timer
        Timer timerInit = getTimers().startTimer(Timings.INIT.toString());
        // set the accuracy of the decimal number
        if (!ApproxDouble.isAccuracySet()) {
            ApproxDouble.setAccuracy(accuracy);
        }
        if (probabilistic) {
            // building the pMap if not given from outside
            if (safe(pMap).isEmpty()) {
                logger.debug("Loading probabilities...");
                buildPMap();
                if (pMap.isEmpty()) {
                    logger.warn("Probabilistic axioms not loaded in initialization: "
                            + "probability will be 0.0 if the query is not entailed, 1.0 if there are explanations.");
                    //setProbabilistic(false);
                }
            }
        }
        // stop timer
        timerInit.stop();

        this.externalReasoner.setRootOntology(rootOntology);
        this.externalReasoner.setReasoningTimeout(reasoningTimeout);
        if (externalReasoner instanceof ExplanationReasoner) {
            ExplanationReasoner reasoner = (ExplanationReasoner) externalReasoner;
            reasoner.setMaxExplanations(maxExplanations);
        }
        if (externalReasoner instanceof ProbabilisticReasoner) {
            ProbabilisticReasoner reasoner = (ProbabilisticReasoner) externalReasoner;
            reasoner.setpMap(pMap);
        }
        if (externalReasoner instanceof Verbozer) {
            ((Verbozer) externalReasoner).setVerbose(verbose);
        }
        if (externalReasoner instanceof OWLExplanationExplanationReasoner) {
            ((OWLExplanationExplanationReasoner) externalReasoner).setCheckConsistency(config.isCheckConsistency());
        }
        this.externalReasoner.init();





        initialized = true;
    }

    /**
     * Creates a pMap from the probabilistic ontology.
     *
     * @throws NumberFormatException
     */
    private void buildPMap() throws NumberFormatException {

        if (!this.initialized) {
            if (pMap != null) {
                return;
            }

            //Timer PMapTimer = timers.startTimer("pMap");
            pMap = BundleUtilities.createPMap(rootOntology, verbose);
//            PMapTimer.stop();
        }

    }

    @Override
    public ExplanationReasonerResult explainAxiom(OWLAxiom axiom)
            throws OWLException, ObjectNotInitializedException {
        Timer timer = getTimers().startTimer(Timings.EXPLANATION.toString());

        if (verbose) {
            LogUtilities.addConsoleAppender(logger, Level.DEBUG);
        }

        ExplanationReasonerResult result;
        if (isUseExplainationReasoner()) {
            result = ((ExplanationReasoner) externalReasoner).explainAxiom(axiom);

        } else {
            String msg = "The external reasoner of BUNDLE is not a reasoner that"
                    + " retrieves explanations";
            logger.error(msg);
            throw new IllegalValueException(msg);
        }

        timer.stop();
        getTimers().mainTimer.stop();
        return result;
    }

    @Override
    public ProbabilisticExplanationReasonerResult computeExplainQuery(OWLAxiom query)
            throws OWLException, ObjectNotInitializedException {
        return (ProbabilisticExplanationReasonerResult) computeQuery(query);
    }

//    /**
//     * Instantiates and returns the explanation generator.
//     *
//     * @return the explanation generator
//     */
//    private TransactionAwareSingleExpGen getSingleExplanationGenerator() {
////        switch (getHstMethod()) {
////            case GLASSBOX:
////                return new GlassBoxExplanation((PelletReasoner) reasoner);
////            case BLACKBOX:
////                return new BlackBoxExplanation(reasoner.getRootOntology(), reasonerFactory, reasoner);
////        }
//        if (getHstMethod() == GLASSBOX) {
//            return new BundleGlassBoxExplanation((PelletReasoner) reasoner);
//        } else {
//            return new BlackBoxExplanation(reasoner.getRootOntology(), reasonerFactory, reasoner);
//        }
//    }
    /**
     * Computes the probability of a set of explanations contained in a
     * QueryResult object. After the computation of the probability, the given
     * QueryResult object will update
     *
     * @param explanationResult a QueryResult object containing the set of
     * explanations of which the method will compute the probability
     * @return a BundleQueryResult containing explanations, probability and
     * general information
     */
    private BundleQueryResult computeProbability(ExplanationReasonerResult explanationResult) {
        getTimers().mainTimer.start();
//        if (probabilistic && pMap != null) {

        ApproxDouble pq = new ApproxDouble(0.0);

        BundleQueryResult result = new BundleQueryResult(explanationResult.getQuery());
        Set<Set<OWLAxiom>> explanations = explanationResult.getQueryExplanations();
        result.setQueryExplanations(explanations,pMap);

        if (pMap.isEmpty()) {
            if (explanationResult.getQueryExplanations().size() > 0) {
                pq = new ApproxDouble(1.0);
            }
        } else {

            Timer timerBDD = getTimers().startTimer(Timings.BDD_COMPUTATION.toString());

            // VarAxAnn composed by two (three) arrays:
            //      prob    (probability of axioms)
            //      axi     (OWLAxiom)
            //     (axp     (translations of axioms into strings))
            initBddF();

            BDD bdd = buildBDD(explanations);

            pq = probabilityOfBDD(bdd);

            timerBDD.stop();

            if ((pq.compareTo(ApproxDouble.one()) < 0)
                    && (//BlockedIndividualSet.isApproximate() ||
                    getMaxExplanations() == explanations.size())) {
                logger.warn("WARNING! The value of the probability may be a lower bound.");
            }

            result.setBDD(bdd);
            //        result.setQueryProbability(pq);
//            Map<OWLAxiom, ApproxDouble> usedProbAxioms = new HashMap<>();
//            for (OWLAxiom usedProbAx : safe(usedAxioms).keySet()) {
//                usedProbAxioms.put(usedProbAx, pMap.get(usedProbAx));
//            }
//            result.setUsedAxioms(usedProbAxioms);
            result.setUsedProbabilisticAxioms(usedProbabilisticAxioms,pMap);
        }

        getTimers().mainTimer.stop();
        result.setTimers(this.getTimers());
        result.setQueryProbability(pq);

        return result;
    }

    /**
     * Build the BDD corresponding to the given set of explanations.
     *
     * @param explanations the set of explanations for which the method
     * calculates the corresponding BDD
     * @return the resulting BDD
     */
    private BDD buildBDD(Set<Set<OWLAxiom>> explanations) {
        usedProbabilisticAxioms = HashBiMap.create();
        return BundleUtilities.buildBDD(explanations, getBddF(), pMap, usedProbabilisticAxioms);
    }

    /**
     * Computes the probability of the given BDD. This is a recursive method
     * that takes as input a BDD (that is the current node) and a map between
     * all the already computed nodes and the corresponding probabilities in
     * order to avoid to recompute the probability of already visited nodes
     *
     * @param node the current BDD
     * @return
     */
    private ApproxDouble probabilityOfBDD(BDD node) {
        return BundleUtilities.probabilityOfBDD(node, new HashMap<>(), pMap, usedProbabilisticAxioms);
    }

    /**
     * Initializes the BDD factory.
     */
    private void initBddF() {
        if (this.getBddF() == null) {
            this.setBddF(BDDFactory2.init(bddFType.toString().toLowerCase(),
                    100, 10000));
            if (getBddF() instanceof JFactory) {
                bddFType = BDDFactoryType.J;
            }
        }
    }

    public BDDFactoryType getBddFType() {
        return bddFType;
    }

    public void setBddFType(BDDFactoryType bddFType) {
        if (!this.initialized) {
            this.bddFType = bddFType;
        }
    }

    public void setBddFType(String bddFTypeStr) {
        if (!this.initialized) {
            switch (bddFTypeStr.toLowerCase()) {
                case "buddy":
                    bddFType = BDDFactoryType.BUDDY;
                    break;
                case "cudd":
                    bddFType = BDDFactoryType.CUDD;
                    break;
                case "cal":
                    bddFType = BDDFactoryType.CAL;
                    break;
                case "j":
                case "java":
                    bddFType = BDDFactoryType.J;
                    break;
                case "jdd":
                    bddFType = BDDFactoryType.JDD;
                    break;
                case "u":
                    bddFType = BDDFactoryType.U;
                    break;
            }
        }
    }

    /**
     * @param probabilistic the probabilistic to set
     */
    public void setProbabilistic(boolean probabilistic) {
        this.probabilistic = probabilistic;
    }

    /**
     * @return the probabilistic
     */
    public boolean isProbabilistic() {
        return probabilistic;
    }

//    /**
//     * @return the hstMethod
//     */
//    public HSTMethod getHstMethod() {
//        return hstMethod;
//    }
//
//    /**
//     * @param hstMethod the hstMethod to set
//     */
//    public void setHstMethod(HSTMethod hstMethod) {
//        this.hstMethod = hstMethod;
//    }
    /**
     * @return the timers
     */
    public Timers getTimers() {
        return timers;
    }

    public boolean isUseExplainationReasoner() {
        return externalReasoner instanceof ExplanationReasoner;
    }

    public boolean isUseProbabilisticReasoner() {
        return externalReasoner instanceof ProbabilisticReasoner;
    }

    public boolean isUseProbabilisticExplanationReasoner() {
        return (externalReasoner instanceof ProbabilisticExplanationReasoner);
    }

//
//    /**
//     * @return the explanationReasoner
//     */
//    public ExplanationReasoner getExplanationReasoner() {
//        return explanationReasoner;
//    }
//
//    /**
//     * @param explanationReasoner the explanationReasoner to set
//     */
//    public void setExplanationReasoner(ExplanationReasoner explanationReasoner) {
//        this.explanationReasoner = explanationReasoner;
//    }
//    /**
//     * @return the probabilisticReasoner
//     */
//    public ProbabilisticReasoner getProbabilisticReasoner() {
//        return probabilisticReasoner;
//    }
//    /**
//     * @param probabilisticReasoner the probabilisticReasoner to set
//     */
//    public void setProbabilisticReasoner(ProbabilisticReasoner probabilisticReasoner) {
//        this.probabilisticReasoner = probabilisticReasoner;
//    }
    @Override
    public Map<OWLAxiom, ApproxDouble> getpMap() {
        return pMap;
    }

    @Override
    public void setpMap(Map<OWLAxiom, ApproxDouble> pMap) {
        this.pMap = pMap;
    }

    /**
     * @return the bddF
     */
    public BDDFactory getBddF() {
        return bddF;
    }

    /**
     * @param bddF the bddF to set
     */
    public void setBddF(BDDFactory bddF) {
        this.bddF = bddF;
    }
}
