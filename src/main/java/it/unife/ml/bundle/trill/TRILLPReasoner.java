/**
 * This file is part of BUNDLE.
 *
 * BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 * BUNDLE can be used both as module and as standalone.
 *
 * BUNDLE and all its parts are distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.trill;

import org.jpl7.Atom;
import org.jpl7.Query;
import org.jpl7.Term;

/**
 *
 * @author Alfredo
 */
public class TRILLPReasoner extends AbstractTRILLFrameworkReasoner {
    @Override
    public void initTrill() {
        Query q = new Query("init_trill",
                new Term[]{new Atom("trillp")}
        );
        q.hasSolution();
        q.close();
    }

}
