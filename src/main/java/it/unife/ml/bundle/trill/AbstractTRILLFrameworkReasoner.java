/**
 * This file is part of BUNDLE.
 *
 * BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 * BUNDLE can be used both as module and as standalone.
 *
 * BUNDLE and all its parts are distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.trill;

import it.unife.ml.bundle.utilities.LogUtilities;
import it.unife.ml.bundle.utilities.TRILLClassExpressionConverter;
import it.unife.ml.bundle.utilities.TRILLClassExpressionConverterImpl;
import it.unife.ml.math.ApproxDouble;
import it.unife.ml.bundle.explreasoner.Verbozer;
import it.unife.ml.probowlapi.core.ProbabilisticReasoner;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResultImpl;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.Integer;
import java.util.*;

import it.unife.ml.probowlapi.renderer.LogManchesterSyntaxExplanationRenderer;
import org.apache.logging.log4j.Level;
import org.jpl7.*;
import org.semanticweb.owlapi.formats.RDFXMLDocumentFormat;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class AbstractTRILLFrameworkReasoner implements ProbabilisticReasoner, Verbozer {

    private static final Logger logger = LoggerFactory.getLogger(AbstractTRILLFrameworkReasoner.class.getName());

    private boolean initialized = false;
    private OWLOntology rootOntology = null;

    /**
     * This value must be true if we want to compute the probability.
     */
    private boolean probabilistic = true;

    /**
     * If set to true all the explanations (and other messages) are written to
     * std output. Default value is false.
     */
    protected boolean verbose = false;

    /**
     * Reasoning time in milliseconds.
     */
    protected long reasoningTimeout = 0;

    protected Map<OWLAxiom, ApproxDouble> pMap;

    public AbstractTRILLFrameworkReasoner() {
//        System.load("/usr/lib/swi-prolog/lib/x86_64-linux/libswipl.so");
//        System.load();
        Query q1
                = new Query(
                        "use_module",
                        new Term[]{new Compound("library", new Term[]{new Atom("trill")})}
                );
        q1.hasSolution();

        q1 = new Query(
                "use_module",
                new Term[]{new Compound("library", new Term[]{new Atom("trill_2_funct")})}
        );
        q1.hasSolution();

    }

    @Override
    public void setRootOntology(OWLOntology rootOntology) {
        this.rootOntology = rootOntology;
    }

    @Override
    public OWLOntology getRootOntology() {
        return rootOntology;
    }

    @Override
    public void setReasoningTimeout(long reasoningTimeout) {
        this.reasoningTimeout = reasoningTimeout;
    }

    @Override
    public void dispose() {
        // do nothing
    }

    public void initTrill() {
        Query q = new Query("init_trill",
                new Term[]{new Atom("trill")}
        );
        q.hasSolution();
        q.close();
    }

    @Override
    public void init() throws ExceptionInInitializerError {

        OWLOntologyManager manager = rootOntology.getOWLOntologyManager();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        RDFXMLDocumentFormat rdfxmlFormat = new RDFXMLDocumentFormat();
        String ontologyRDFXML = null;
        try {
            manager.saveOntology(rootOntology, rdfxmlFormat, baos);
        } catch (OWLOntologyStorageException ex) {
            logger.error(ex.getMessage());
            throw new ExceptionInInitializerError(ex);
        }
        try {
            ontologyRDFXML = baos.toString("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ExceptionInInitializerError(ex);
        }
        initTrill();
        Query q = new Query("load_owl_kb_from_string",
                new Term[]{new Atom(ontologyRDFXML)}
        );
        q.hasSolution();
        q.close();

        initialized = true;

    }

    @Override
    public boolean isInitialized() {
        return initialized;
    }

    /**
     * @param probabilistic the probabilistic to set
     */
    public void setProbabilistic(boolean probabilistic) {
        this.probabilistic = probabilistic;
    }

    /**
     * @return the showAll
     */
    @Override
    public boolean isVerbose() {
        return verbose;
    }

    /**
     * @param verbose the showAll to set
     */
    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    /**
     * @return the probabilistic
     */
    public boolean isProbabilistic() {
        return probabilistic;
    }

    @Override
    public ProbabilisticReasonerResult computeQuery(OWLAxiom query) throws OWLException, ObjectNotInitializedException {

        double prob = 0;

        Map<String, Term> solution = executeTRILLQuery(query);

        String sol = "" + solution.get("Prob");
        prob = Double.parseDouble(sol);
        if (verbose && prob == 0.0) {
            LogUtilities.addConsoleAppender(getLogger(), Level.DEBUG);
            LogManchesterSyntaxExplanationRenderer rend = new LogManchesterSyntaxExplanationRenderer();
            try {
                rend.startRendering(getLogger());
                rend.render(Collections.EMPTY_SET);
                rend.endRendering();
            } catch (IOException | OWLException var3) {
                throw new RuntimeException("Error rendering explanation: " + var3);
            }
        }


        ApproxDouble queryProbability = new ApproxDouble(prob);
        return new ProbabilisticReasonerResultImpl(query, queryProbability);
    }

    /**
     * This method execute the query specified by (@link query) and compute its
     * probability iff (@link probabilistic) is true.
     *
     * @param query the query to compute
     * @param maxExplanations the maximum number of explanations to find
     * @return the result of the query
     */
    protected Map<String, Term> executeTRILLQuery(OWLAxiom query, int maxExplanations) {

        Query q = setQuery(query, maxExplanations);

        Map<String, Term> solution = q.oneSolution();
        q.close();
        return solution;
    }

    /**
     * This method execute the query specified by (@link query)
     *
     * @param query the query to compute
     * @return the result of the query
     */
    protected Map<String, Term> executeTRILLQuery(OWLAxiom query) {
        return executeTRILLQuery(query, Integer.MAX_VALUE);
    }

    protected Query setQuery(OWLAxiom query, int maxExplanations) {
        Query q = null;
        String queryType = null;

        String query_options_string = null;
        if (maxExplanations != -1 && maxExplanations != Integer.MAX_VALUE) {
            query_options_string = "max_expl(" + maxExplanations + ")";
        } else {
            query_options_string = "max_expl(all)";
        }
        if (isProbabilistic()) {
            query_options_string += ",compute_prob(query,Prob)";
        }
        if (getReasoningTimeout() > 0) {
            query_options_string += ",time_limit(" + (getReasoningTimeout()/1000) + ")";
        }
        Term query_options = Term.textToTerm("[" + query_options_string + "]");
        Term explTerm = new Variable("ExplsT");
        List<Term> argsList = new ArrayList<>();
        Term[] args = {};
        TRILLClassExpressionConverter converter = new TRILLClassExpressionConverterImpl();

        if (query.getAxiomType() == AxiomType.CLASS_ASSERTION) {

            OWLClassAssertionAxiom axiom = (OWLClassAssertionAxiom) query;
            OWLClassExpression owlClass = axiom.getClassExpression();
            OWLIndividual owlIndividual = axiom.getIndividual();

//            converter.getTRILLClassExpression(owlClass);
//            owlClass.accept(converter);
            queryType = "instanceOf";

            //argsList.add(new Atom(owlClass.asOWLClass().getIRI().toString()));
            argsList.add(new Atom(converter.getTRILLClassExpression(owlClass)));
            argsList.add(new Atom(owlIndividual.asOWLNamedIndividual().getIRI().toString()));

        } else if (query.getAxiomType() == AxiomType.OBJECT_PROPERTY_ASSERTION) {

            OWLObjectPropertyAssertionAxiom axiom = (OWLObjectPropertyAssertionAxiom) query;
            OWLObjectPropertyExpression owlProperty = axiom.getProperty();
            OWLIndividual owlIndividual1 = axiom.getSubject();
            OWLIndividual owlIndividual2 = axiom.getObject();

            queryType = "property_value";

            argsList.add(new Atom(owlProperty.asOWLObjectProperty().getIRI().toString()));
            argsList.add(new Atom(owlIndividual1.asOWLNamedIndividual().getIRI().toString()));
            argsList.add(new Atom(owlIndividual2.asOWLNamedIndividual().getIRI().toString()));

        } else if (query.getAxiomType() == AxiomType.DATA_PROPERTY_ASSERTION) {
            throw new UnsupportedOperationException("Not supported yet.");
        } else if (query.getAxiomType() == AxiomType.SUBCLASS_OF) {

            OWLSubClassOfAxiom axiom = (OWLSubClassOfAxiom) query;
            OWLClassExpression owlSuperClass = axiom.getSuperClass();
            OWLClassExpression owlSubClass = axiom.getSubClass();
            if (owlSuperClass.isOWLNothing()) {
                if (owlSubClass.isOWLThing()) {
                    queryType = "inconsistent_theory";
                } else {
                    queryType = "unsat";

                    //argsList.add(new Atom(owlSubClass.asOWLClass().getIRI().toString()));
                    argsList.add(new Atom(converter.getTRILLClassExpression(owlSubClass)));
                }
            } else {
                queryType = "sub_class";

//                argsList.add(new Atom(owlSubClass.asOWLClass().getIRI().toString()));
//                argsList.add(new Atom(owlSuperClass.asOWLClass().getIRI().toString()));
                argsList.add(new Atom(converter.getTRILLClassExpression(owlSubClass)));
                argsList.add(new Atom(converter.getTRILLClassExpression(owlSuperClass)));
            }
        }

        argsList.add(explTerm);
        argsList.add(query_options);
        args = argsList.toArray(args);
        q = new Query(queryType, args);

        if (q == null) {
            throw new UnsupportedOperationException("The axiom " + query + "is not supported yet.");
        }

        return q;
    }

    @Override
    public void setpMap(Map<OWLAxiom, ApproxDouble> pMap) {
        this.pMap = pMap;
    }

    @Override
    public Map<OWLAxiom, ApproxDouble> getpMap() {
        return pMap;
    }

    public static Logger getLogger() {
        return logger;
    }

    @Override
    public long getReasoningTimeout() {
        return reasoningTimeout;
    }
}
