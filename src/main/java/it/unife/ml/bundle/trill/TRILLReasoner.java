/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.trill;

import it.unife.ml.math.ApproxDouble;
import it.unife.ml.bundle.utilities.LogUtilities;
import it.unife.ml.probowlapi.core.*;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import it.unife.ml.probowlapi.renderer.LogManchesterSyntaxExplanationRenderer;
import org.apache.logging.log4j.Level;
import org.jpl7.Query;
import org.jpl7.Term;
import org.jpl7.Util;
import org.jpl7.Variable;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.functional.parser.OWLFunctionalSyntaxOWLParserFactory;
import org.semanticweb.owlapi.io.OWLParser;
import org.semanticweb.owlapi.io.StringDocumentSource;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyLoaderConfiguration;
import org.semanticweb.owlapi.model.OWLOntologyManager;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese <riccardo.zese@unife.it>
 */
public class TRILLReasoner extends AbstractTRILLFrameworkReasoner implements ProbabilisticExplanationReasoner {

    protected int maxExplanations = Integer.MAX_VALUE;

    @Override
    public ProbabilisticExplanationReasonerResult computeExplainQuery(OWLAxiom query) throws OWLException, ObjectNotInitializedException {
        return (ProbabilisticExplanationReasonerResult) computeQuery(query);
    }

    @Override
    public ProbabilisticReasonerResult computeQuery(OWLAxiom query) throws OWLException, ObjectNotInitializedException {

        double prob = 0;

        Map<String, Term> solution = executeTRILLQuery(query,getMaxExplanations());

        Term expl = solution.get("ExplsT");

        Term explTermFunct = new Variable("ExplsF");
        Query qConv = new Query("convert_explanations", new Term[]{expl, explTermFunct});
        java.util.Map<String, Term> solutionConv;
        if (qConv == null) {
            throw new UnsupportedOperationException("The action " + query + "is not supported in this version of TRILL.\n"
                    + "Please, upgrade your version of TRILL Framework by executing the command in a SWI-Prolog console:\n"
                    + "?- pack_upgrade(trill).");
        }

        solutionConv = qConv.oneSolution();
        qConv.close();
        Set<Set<OWLAxiom>> explanations = null;
        try {
            explanations = convertExplanations(solutionConv.get("ExplsF"));
        } catch (OWLOntologyCreationException | IOException ex) {
            throw new OWLException("Cannot convert explanations: " + ex);
        }

        if (verbose) {
            LogUtilities.addConsoleAppender(getLogger(), Level.DEBUG);
            LogManchesterSyntaxExplanationRenderer rend = new LogManchesterSyntaxExplanationRenderer();
            try {
                rend.startRendering(getLogger());
                rend.render(explanations);
                rend.endRendering();
            } catch (IOException | OWLException var3) {
                throw new RuntimeException("Error rendering explanation: " + var3);
            }
        }

        ApproxDouble queryProbability = null;
        if (isProbabilistic()) {
            String sol = "" + solution.get("Prob");
            prob = Double.parseDouble(sol);

            queryProbability = new ApproxDouble(prob);
        }

        return new ProbabilisticExplanationReasonerResultImpl(query, queryProbability, explanations);

    }

    @Override
    public ExplanationReasonerResult explainAxiom(OWLAxiom query) throws OWLException, ObjectNotInitializedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMaxExplanations(int maxExplanations) {
        this.maxExplanations = maxExplanations;
    }

    @Override
    public int getMaxExplanations() {
        return maxExplanations;
    }

    private Set<Set<OWLAxiom>> convertExplanations(Term TRILLExplanations) throws OWLOntologyCreationException, IOException {

        Set<Set<OWLAxiom>> explanations = new HashSet<>();

        Term[] solutionConvArray = Term.listToTermArray(TRILLExplanations);
        for (Term expl : solutionConvArray) {
            Set<OWLAxiom> explanation = new HashSet<>();
            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
            OWLOntology ontology = manager.createOntology();
            String[] explConvArray = Term.atomListToStringArray(expl);
            String tempOntologyString = "Ontology ( ";
            for (String axiomStr : explConvArray) {
                tempOntologyString += axiomStr;
            }
            tempOntologyString += ")";
            OWLParser owlParser = new OWLFunctionalSyntaxOWLParserFactory().createParser();
            owlParser.parse(new StringDocumentSource(tempOntologyString), ontology, new OWLOntologyLoaderConfiguration());
            explanations.add(ontology.getAxioms());
        }
        return explanations;

    }

}
