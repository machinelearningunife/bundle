/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author giuseppe
 */
public abstract class AbstractBundleController implements Observer {

    protected AbstractBundleUI ui;

    protected BundleModel bundleModel;

    public AbstractBundleController(AbstractBundleUI ui, BundleModel bundleModel) {
        this.ui = ui;
        this.bundleModel = bundleModel;
        ui.addObserver(this);
        bundleModel.addObserver(this);
        bundleModel.addObserver(ui);
    }
    

}
