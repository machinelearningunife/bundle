/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import it.unife.ml.bundle.exception.IllegalValueException;
import it.unife.ml.bundle.exception.IncompatibleHSTMethod;
import it.unife.ml.bundle.explreasoner.BlackBoxExplanationReasoner;
import it.unife.ml.bundle.explreasoner.OWLExplanationExplanationReasoner;
import it.unife.ml.bundle.factpp.FactppBlackBoxExplanationReasoner;
import it.unife.ml.bundle.factpp.FactppOWLExplanationReasoner;
import it.unife.ml.bundle.pellet.PelletBlackBoxExplanationReasoner;
import it.unife.ml.bundle.pellet.PelletGlassBoxExplanationReasoner;
import it.unife.ml.bundle.pellet.PelletOWLExplanationReasoner;
import it.unife.ml.bundle.trill.TORNADOReasoner;
import it.unife.ml.bundle.trill.TRILLPReasoner;
import it.unife.ml.bundle.trill.TRILLReasoner;
import it.unife.ml.bundle.utilities.Constants.HSTMethod;
import static it.unife.ml.bundle.utilities.Constants.HSTMethod.GLASSBOX;
import static it.unife.ml.bundle.utilities.Constants.HSTMethod.NONE;
import static it.unife.ml.bundle.utilities.Constants.HSTMethod.OWLEXPLANATION;
import it.unife.ml.bundle.utilities.Constants.ReasonerName;
import static it.unife.ml.bundle.utilities.Constants.ReasonerName.*;
import it.unife.ml.probowlapi.core.Reasoner;
import org.semanticweb.owlapi.model.OWLOntology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.manchester.cs.jfact.JFactFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class BundleConfigurationBuilder {

    private static final Logger logger = LoggerFactory.getLogger(BundleConfigurationBuilder.class);

    // maximum number of digits after the dot during the calculations
    protected int numericalAccuracy = 5;

    /**
     * Method used to retrieve an explanation during the Hitting Set Tree
     * algorithm. Default: OWLEXPLANATION
     */
    private HSTMethod hstMethod = OWLEXPLANATION;

    protected Reasoner externalReasoner;

    protected ReasonerName reasonerName = ReasonerName.PELLET;

    /**
     * This value must be true if we want to compute the probability.
     */
    private boolean probabilistic = true;

    /**
     * The type of BDD Factory. Possible values: BUDDY, CUDD, CAL, J, U, JDD.
     */
    private BDDFactoryType bddFType = BDDFactoryType.BUDDY;

    protected int maxExplanations = 0;

    /**
     * Reasoning time in milliseconds.
     */
    protected long reasoningTimeout = 0;

    protected OWLOntology rootOntology;

    /**
     * If set to true all the explanations (and other messages) are logged. The
     * destinations of the logs are configured through the property file of the
     * logging service. Default value is false.
     */
    protected boolean verbose = false;

    /**
     * If true means that the reasoner should obtain the explanations for the
     * inconsistency of the ontology.
     */
    protected boolean checkConsistency = false;

    public BundleConfigurationBuilder(OWLOntology rootOntology) {
        this.rootOntology = rootOntology;
    }

//    public BundleConfigurationBuilder(BundleConfiguration config) {
//        this.bddFType = config.getBddFType();
//        this.externalReasoner = config.getExternalReasoner();
//        this.hstMethod = 
//    }

    public BundleConfiguration buildConfiguration() {

        buildExternalReasoner();

        return new BundleConfiguration(numericalAccuracy,
                externalReasoner,
                probabilistic,
                bddFType,
                maxExplanations,
                reasoningTimeout,
                rootOntology,
                checkConsistency,
                verbose);
    }

    public BundleConfigurationBuilder numericalAccuracy(int numericalAccuracy) {
        this.numericalAccuracy = numericalAccuracy;
        return this;
    }

    public BundleConfigurationBuilder hstMethod(HSTMethod hstMethod) {
        this.hstMethod = hstMethod;
        return this;
    }

    private void buildExternalReasoner() {

        switch (getHstMethod()) {
            case NONE:
                switch (reasonerName) {
                    case TRILL:
                        this.externalReasoner = new TRILLReasoner();
                        break;
                    case TRILLP:
                        this.externalReasoner = new TRILLPReasoner();
                        break;
                    case TORNADO:
                        this.externalReasoner = new TORNADOReasoner();
                        break;
                    default:
                        String msg = "An HST method must be provided for " + reasonerName;
                        logger.error(msg);
                        throw new IncompatibleHSTMethod(msg);
                }
                break;
            case GLASSBOX:
                switch (reasonerName) {
                    // check if it's Pellet Reasoner. For Pellet it should be preferable to
                    // use the class GlassBoxExplanation (it is faster than BlackBox)
                    case PELLET:
                        this.externalReasoner = new PelletGlassBoxExplanationReasoner();
                        break;
                    default:
                        String msg = "GlassBox HST method can be used only with Pellet";
                        logger.error(msg);
                        throw new IncompatibleHSTMethod(msg);
                }
                break;
            case BLACKBOX:
                switch (reasonerName) {
                    case PELLET:
                        this.externalReasoner = new PelletBlackBoxExplanationReasoner();
                        break;
                    case FACTPP:
                        this.externalReasoner = new FactppBlackBoxExplanationReasoner();
                        break;
                    case HERMIT:
                        this.externalReasoner = new BlackBoxExplanationReasoner(new org.semanticweb.HermiT.ReasonerFactory());
                        break;
                    case JFACT:
                        this.externalReasoner = new BlackBoxExplanationReasoner(new JFactFactory());
                        break;
                    case TRILL:
                    case TRILLP:
                    case TORNADO:
                        String msg = "HST method cannot be used with TRILL, TRILL^P or TORNADO";
                        logger.error(msg);
                        throw new IncompatibleHSTMethod(msg);
                    default:
                        throw new IllegalValueException("Unsupported reasoner: " + reasonerName);
                }
                break;
            case OWLEXPLANATION:
                switch (reasonerName) {
                    case PELLET:
                        this.externalReasoner = new PelletOWLExplanationReasoner();
                        break;
                    case FACTPP:
                        this.externalReasoner = new FactppOWLExplanationReasoner();
                        break;
                    case HERMIT:
                        this.externalReasoner = new OWLExplanationExplanationReasoner(new org.semanticweb.HermiT.ReasonerFactory());
                        break;
                    case JFACT:
                        this.externalReasoner = new OWLExplanationExplanationReasoner(new JFactFactory());
                        break;
                    case TRILL:
                    case TRILLP:
                    case TORNADO:
                        String msg = "HST method cannot be used with TRILL, TRILL^P or TORNADO";
                        logger.error(msg);
                        throw new IncompatibleHSTMethod(msg);
                    default:
                        throw new IllegalValueException("Unsupported reasoner: " + reasonerName);
                }
                break;

        }

    }

    public BundleConfigurationBuilder maxExplanations(int maxExplanations) {
        this.maxExplanations = maxExplanations;
        return this;
    }

    public BundleConfigurationBuilder timeout(long reasoningTimeout) {
        this.reasoningTimeout = reasoningTimeout;
        return this;
    }

    public BundleConfigurationBuilder verbose(boolean verbose) {
        this.verbose = verbose;
        return this;
    }

    public BundleConfigurationBuilder probabilistic(boolean probabilistic) {
        this.probabilistic = probabilistic;
        return this;
    }

    public BundleConfigurationBuilder bddFType(BDDFactoryType bDDFactoryType) {
        this.bddFType = bDDFactoryType;
        return this;
    }

    public BundleConfigurationBuilder reasoner(ReasonerName reasonerName) {
        this.reasonerName = reasonerName;
        return this;
    }
    
    public BundleConfigurationBuilder checkConsistency(boolean checkConsistency) {
        this.checkConsistency = checkConsistency;
        return this;
    }

    /**
     * @return the hstMethod
     */
    public HSTMethod getHstMethod() {
        return hstMethod;
    }
    
    

}
