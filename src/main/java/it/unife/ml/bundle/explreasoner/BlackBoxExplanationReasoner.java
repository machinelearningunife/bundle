/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.explreasoner;

import com.clarkparsia.owlapi.explanation.BlackBoxExplanation;
import com.clarkparsia.owlapi.explanation.MultipleExplanationGenerator;
import com.clarkparsia.owlapi.explanation.SatisfiabilityConverter;
import com.clarkparsia.owlapi.explanation.TransactionAwareSingleExpGen;
import it.unife.ml.bundle.utilities.LogUtilities;
import it.unife.ml.probowlapi.core.ExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ExplanationReasonerResultImpl;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import it.unife.ml.probowlapi.explanation.BundleHSTExplanationGenerator;
import it.unife.ml.probowlapi.monitor.BundleRendererExplanationProgressMonitor;
import it.unife.ml.probowlapi.monitor.LogRendererTimeExplanationProgressMonitor;
import it.unife.ml.probowlapi.monitor.SilentRendererTimeExplanationProgressMonitor;
import java.util.HashSet;
import java.util.Set;
import org.apache.logging.log4j.Level;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class BlackBoxExplanationReasoner extends AbstractExplanationReasoner {

    private static final Logger logger = LoggerFactory.getLogger(BlackBoxExplanationReasoner.class);

    protected OWLReasonerFactory reasonerFactory;

    protected OWLReasoner reasoner;

    public BlackBoxExplanationReasoner(OWLReasonerFactory reasonerFactory) {
        this.reasonerFactory = reasonerFactory;
    }

    // create converter to convert an axiom to its negation (inference based
    // refutation). 
    /**
     * Used to convert an axiom to its negation.
     */
    protected SatisfiabilityConverter converter = new SatisfiabilityConverter(OWLManager.getOWLDataFactory());

    @Override
    public ExplanationReasonerResult explainAxiom(OWLAxiom axiom) throws OWLException, ObjectNotInitializedException {

        Set<Set<OWLAxiom>> explanations = new HashSet<>();

        // create an instance of the class that computes all the explanations.
        // HSTExplanationGenerator is a class that implements the Hitting Set Tree
        // algorithm.
        TransactionAwareSingleExpGen explanationGen = new BlackBoxExplanation(rootOntology, reasonerFactory, reasoner);
        MultipleExplanationGenerator expGen = new BundleHSTExplanationGenerator(explanationGen);

        BundleRendererExplanationProgressMonitor rendererMonitor;
        if (verbose) {
            LogUtilities.addConsoleAppender(logger, Level.DEBUG);
            rendererMonitor = new LogRendererTimeExplanationProgressMonitor(axiom, logger);
        } else {
            rendererMonitor = new SilentRendererTimeExplanationProgressMonitor();
        }
        expGen.setProgressMonitor(rendererMonitor);

        OWLClassExpression unsatClass = converter.convert(axiom);

        rendererMonitor.setParamAndStart(getReasoningTimeout());

        // obtain all the explanations
        explanations = expGen.getExplanations(unsatClass, getMaxExplanations());

        // dispose explanation generator
        expGen.dispose();

        rendererMonitor.stopMonitoring();

        if (explanations.isEmpty()) {
            rendererMonitor.foundNoExplanations();
        }
        return new ExplanationReasonerResultImpl(axiom, explanations);

    }

    @Override
    public void dispose() {
        reasoner.dispose();
    }

    @Override
    public void init() throws ExceptionInInitializerError {
        reasoner = reasonerFactory.createNonBufferingReasoner(rootOntology);
//        converter = new SatisfiabilityConverter(this.rootOntology.getOWLOntologyManager().getOWLDataFactory());
    }

}
