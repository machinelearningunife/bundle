/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.explreasoner;

import it.unife.ml.probowlapi.core.ExplanationReasoner;
import org.semanticweb.owlapi.model.OWLOntology;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class AbstractExplanationReasoner implements ExplanationReasoner, Verbozer {

    protected int maxExplanations;

    /**
     * Reasoning time in milliseconds.
     */
    protected long reasoningTimeout;

    protected OWLOntology rootOntology;

    protected boolean initialized = false;

    /**
     * If set to true all the explanations (and other messages) are logged. The
     * destinations of the logs are configured through the property file of the
     * logging service. Default value is false.
     */
    protected boolean verbose = false;

    @Override
    public void setMaxExplanations(int maxExplanations) {
        this.maxExplanations = maxExplanations;
    }

    @Override
    public int getMaxExplanations() {
        return maxExplanations;
    }

    @Override
    public void setRootOntology(OWLOntology rootOntology) {
        this.rootOntology = rootOntology;
    }

    @Override
    public OWLOntology getRootOntology() {
        return rootOntology;
    }

    /**
     * Returns the reasoning timeout in milliseconds
     *
     * @return the reasoningTimeout
     */
    @Override
    public long getReasoningTimeout() {
        return reasoningTimeout;
    }

    @Override
    public void setReasoningTimeout(long reasoningTimeout) {
        this.reasoningTimeout = reasoningTimeout;
    }

    @Override
    public boolean isInitialized() {
        return this.initialized;
    }
    
        /**
     * @return the showAll
     */
    @Override
    public boolean isVerbose() {
        return verbose;
    }

    /**
     * @param verbose the showAll to set
     */
    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

}
