/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.explreasoner;

import it.unife.ml.bundle.utilities.LogUtilities;
import it.unife.ml.probowlapi.core.ExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ExplanationReasonerResultImpl;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import it.unife.ml.probowlapi.monitor.BundleRendererExplanationProgressMonitor2;
import it.unife.ml.probowlapi.monitor.LogRendererTimeExplanationProgressMonitor2;
import it.unife.ml.probowlapi.monitor.NullRendererTimeExplanationProgressMonitor;
import static it.unife.ml.probowlapi.utilities.GeneralUtils.safe;
import java.util.HashSet;
import java.util.Set;
import org.apache.logging.log4j.Level;
import org.semanticweb.owl.explanation.api.Explanation;
import org.semanticweb.owl.explanation.api.ExplanationGenerator;
import org.semanticweb.owl.explanation.api.ExplanationGeneratorFactory;
import org.semanticweb.owl.explanation.api.ExplanationGeneratorInterruptedException;
import org.semanticweb.owl.explanation.api.ExplanationManager;
import org.semanticweb.owl.explanation.impl.blackbox.checker.InconsistentOntologyExplanationGeneratorFactory;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.TimeOutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class OWLExplanationExplanationReasoner extends AbstractExplanationReasoner {

    private static final Logger logger = LoggerFactory.getLogger(OWLExplanationExplanationReasoner.class);

    protected OWLReasonerFactory reasonerFactory;

    public OWLExplanationExplanationReasoner(OWLReasonerFactory reasonerFactory) {
        this.reasonerFactory = reasonerFactory;
    }

    /**
     * If true means that the reasoner should obtain the explanations for the
     * inconsistency of the ontology.
     */
    protected boolean checkConsistency = false;

    @Override
    public ExplanationReasonerResult explainAxiom(OWLAxiom axiom) throws OWLException, ObjectNotInitializedException {

        Set<Set<OWLAxiom>> explanations = new HashSet<>();

        BundleRendererExplanationProgressMonitor2<OWLAxiom> rendererMonitor;
        if (verbose) {
            LogUtilities.addConsoleAppender(logger, Level.DEBUG);
            rendererMonitor = new LogRendererTimeExplanationProgressMonitor2(logger);
        } else {
            rendererMonitor = new NullRendererTimeExplanationProgressMonitor<>();
        }
        // Create the explanation generator factory which uses reasoners provided by the specified
        // reasoner factory
        ExplanationGeneratorFactory<OWLAxiom> expGen;
        if (isCheckConsistency()) {
            expGen = new InconsistentOntologyExplanationGeneratorFactory(reasonerFactory, getReasoningTimeout());
        } else {
            expGen = ExplanationManager.createExplanationGeneratorFactory(reasonerFactory);
        }
        // start the timeout
        rendererMonitor.setParamAndStart(getReasoningTimeout());
        // Now create the actual explanation generator for our ontology
        ExplanationGenerator<OWLAxiom> gen = expGen.createExplanationGenerator(rootOntology, rendererMonitor);

        // Get our explanations.
        Set<Explanation<OWLAxiom>> expl = null;
        try {
            if (maxExplanations > 0) {
                expl = gen.getExplanations(axiom, maxExplanations);
            } else {
                expl = gen.getExplanations(axiom);
            }
        } catch (TimeOutException | ExplanationGeneratorInterruptedException e) {
            logger.warn("Reached timeout while reasoning");
            expl = rendererMonitor.getFoundExplanations();
        }

        for (Explanation<OWLAxiom> ex : safe(expl)) {
            explanations.add(ex.getAxioms());
        }

        rendererMonitor.stopMonitoring();

        if (explanations.isEmpty()) {
            rendererMonitor.foundNoExplanations(axiom);
        }

        return new ExplanationReasonerResultImpl(axiom, explanations);
    }

    @Override
    public void dispose() {
    }

    @Override
    public void init() throws ExceptionInInitializerError {
    }

    /**
     * @return the checkConsistency
     */
    public boolean isCheckConsistency() {
        return checkConsistency;
    }

    /**
     * @param checkConsistency the checkConsistency to set
     */
    public void setCheckConsistency(boolean checkConsistency) {
        this.checkConsistency = checkConsistency;
    }

}
