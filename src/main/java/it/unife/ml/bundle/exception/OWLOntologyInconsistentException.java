/**
 *  This file is part of BUNDLE.
 * 
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 * 
 *  BUNDLE can be used both as module and as standalone.
 * 
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, 
 *  but some components can be used as stand-alone.
 * 
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.unife.ml.bundle.exception;

import java.io.Serializable;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLRuntimeException;

/**
 * This exception is thrown when the addition of the axiom 
 * will cause the ontology to be inconsistent.
 * 
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese <riccardo.zese@unife.it>
 */
public class OWLOntologyInconsistentException extends OWLRuntimeException implements Serializable {

    public OWLOntologyInconsistentException(String message) {
        super(message);
    }
}
