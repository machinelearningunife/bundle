/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.task;

import it.unife.ml.bundle.BDDFactoryType;
import it.unife.ml.bundle.BundleModel;
import it.unife.ml.bundle.utilities.Constants.HSTMethod;
import it.unife.ml.bundle.utilities.Constants.ReasonerName;
import it.unife.ml.probowlapi.core.Reasoner;
import javax.validation.constraints.NotNull;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class Task {

//    @NotNull
    /**
     * reasoner factory for non-probabilistic reasoners that does not directly 
     * return the set of justifications.
     */
//    protected OWLReasonerFactory reasonerFactory;
    
    /**
     * External reasoner used for computing the probability and/or the set of
     * justifications.
     */
    @NotNull
    private ReasonerName externalReasonerName;
    
//    protected ExplanationReasoner explanationReasoner;
    
//    protected ProbabilisticReasoner probabilisticReasoner;

    @NotNull
    protected HSTMethod hstm;

    @NotNull
    protected Integer maxExplanations;

    @NotNull
    protected Long reasoningTimeout;

    @NotNull
    protected Boolean noProb;

    @NotNull
    protected BDDFactoryType bddft;

    @NotNull
    protected OWLOntology rootOntology;

    @NotNull
    protected Imports importClosure;

    @NotNull
    protected Boolean verbose;

    public Task() {
    }

    public Task(Task task) {
        this.bddft = task.bddft;
        this.hstm = task.hstm;
        this.importClosure = task.importClosure;
        this.maxExplanations = task.maxExplanations;
        this.noProb = task.noProb;
        this.externalReasonerName = task.externalReasonerName;
        this.reasoningTimeout = task.reasoningTimeout;
        this.rootOntology = task.rootOntology;
        this.verbose = task.verbose;
        this.externalReasonerName = task.externalReasonerName;
//        this.explanationReasoner = task.explanationReasoner;
//        this.probabilisticReasoner = task.probabilisticReasoner;
    }

    /**
     * @return the reasonerFactory
     */
//    public OWLReasonerFactory getReasonerFactory() {
//        return reasonerFactory;
//    }
//
//    /**
//     * @param reasonerFactory the reasonerFactory to set
//     */
//    public void setReasonerFactory(OWLReasonerFactory reasonerFactory) {
//        this.reasonerFactory = reasonerFactory;
//    }

    /**
     * @return the hstm
     */
    public HSTMethod getHSTMethod() {
        return hstm;
    }

    /**
     * @param hstm the hstm to set
     */
    public void setHSTMethod(HSTMethod hstm) {
        this.hstm = hstm;
    }

    /**
     * @return the maxExplanations
     */
    public Integer getMaxExplanations() {
        return maxExplanations;
    }

    /**
     * @return the importClosure
     */
    public Imports getImportClosure() {
        return importClosure;
    }

    /**
     * @param maxExplanations the maxExplanations to set
     */
    public void setMaxExplanations(Integer maxExplanations) {
        this.maxExplanations = maxExplanations;
    }

    /**
     * @return the reasoningTimeout
     */
    public Long getReasoningTimeout() {
        return reasoningTimeout;
    }

    /**
     * @param reasoningTimeout the reasoningTimeout to set
     */
    public void setReasoningTimeout(Long reasoningTimeout) {
        this.reasoningTimeout = reasoningTimeout;
    }

    /**
     * @return the noProb
     */
    public boolean isNoProb() {
        return noProb;
    }

    /**
     * @param noProb the noProb to set
     */
    public void setNoProb(boolean noProb) {
        this.noProb = noProb;
    }

    /**
     * @return the bddft
     */
    public BDDFactoryType getBDDFactoryType() {
        return bddft;
    }

    /**
     * @param bddft the bddft to set
     */
    public void setBDDFactoryType(BDDFactoryType bddft) {
        this.bddft = bddft;
    }

    /**
     * @return the rootOntology
     */
    public OWLOntology getRootOntology() {
        return rootOntology;
    }

    /**
     * @param rootOntology the rootOntology to set
     */
    public void setRootOntology(OWLOntology rootOntology) {
        this.rootOntology = rootOntology;
    }

    public void setImportClosure(Imports importClosure) {
        this.importClosure = importClosure;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public boolean isVerbose() {
        return this.verbose;
    }

    public void execute(BundleModel bundleModel) {
        execute(bundleModel, true);
    }

    public abstract void execute(BundleModel bundleModel, boolean disposeAfterReasoning);

//    /**
//     * @return the explanationReasoner
//     */
//    public ExplanationReasoner getExplanationReasoner() {
//        return explanationReasoner;
//    }
//
//    /**
//     * @return the probabilisticReasoner
//     */
//    public ProbabilisticReasoner getProbabilisticReasoner() {
//        return probabilisticReasoner;
//    }

//    /**
//     * @param probabilisticReasoner the probabilisticReasoner to set
//     */
//    public void setProbabilisticReasoner(ProbabilisticReasoner probabilisticReasoner) {
//        this.probabilisticReasoner = probabilisticReasoner;
//    }

    /**
     * @return the externalReasonerName
     */
    public ReasonerName getExternalReasonerName() {
        return externalReasonerName;
    }

    /**
     * @param externalReasonerName the externalReasonerName to set
     */
    public void setExternalReasonerName(ReasonerName externalReasonerName) {
        this.externalReasonerName = externalReasonerName;
    }

}
