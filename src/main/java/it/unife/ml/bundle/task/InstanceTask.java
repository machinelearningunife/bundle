/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.task;

import it.unife.ml.bundle.BundleModel;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLNamedIndividual;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class InstanceTask extends Task {
    
    protected final OWLClass owlClass;
    
    protected final OWLNamedIndividual owlIndividual;
    
    public InstanceTask(OWLClass owlClass, OWLNamedIndividual owlIndividual) {
        this.owlClass = owlClass;
        this.owlIndividual = owlIndividual;
    }

    /**
     * @return the owlClass
     */
    public OWLClass getOwlClass() {
        return owlClass;
    }

    /**
     * @return the owlIndividual
     */
    public OWLNamedIndividual getOwlIndividual() {
        return owlIndividual;
    }

    @Override
    public void execute(BundleModel bundleModel, boolean disposeAfterReasoning) {
        bundleModel.executeTask(this, disposeAfterReasoning);
    }
    
}
