/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.task;

import it.unife.ml.bundle.BundleModel;
import org.semanticweb.owlapi.model.OWLClass;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SubClassTask extends Task {
    
    protected OWLClass subClass;
    
    protected OWLClass superClass;

    public SubClassTask(OWLClass subClass, OWLClass superClass) {
        this.subClass = subClass;
        this.superClass = superClass;
    }
    
    public SubClassTask(OWLClass subClass, OWLClass superClass, Task task) {
        super(task);
        this.subClass = subClass;
        this.superClass = superClass;
    }

    /**
     * @return the subClass
     */
    public OWLClass getSubClass() {
        return subClass;
    }

    /**
     * @return the superClass
     */
    public OWLClass getSuperClass() {
        return superClass;
    }

    @Override
    public void execute(BundleModel bundleModel, boolean disposeAfterReasoning) {
        bundleModel.executeTask(this, disposeAfterReasoning);
    }
    
}
