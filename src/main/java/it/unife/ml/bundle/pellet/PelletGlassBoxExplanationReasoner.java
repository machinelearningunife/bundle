/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.pellet;

import com.clarkparsia.owlapi.explanation.MultipleExplanationGenerator;
import com.clarkparsia.owlapi.explanation.SatisfiabilityConverter;
import com.clarkparsia.owlapi.explanation.TransactionAwareSingleExpGen;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import it.unife.ml.bundle.explreasoner.AbstractExplanationReasoner;
import it.unife.ml.bundle.explreasoner.BlackBoxExplanationReasoner;
import it.unife.ml.probowlapi.core.ExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ExplanationReasonerResultImpl;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import it.unife.ml.probowlapi.explanation.BundleGlassBoxExplanation;
import it.unife.ml.probowlapi.explanation.BundleHSTExplanationGenerator;
import it.unife.ml.probowlapi.monitor.BundleRendererExplanationProgressMonitor;
import it.unife.ml.probowlapi.monitor.LogRendererTimeExplanationProgressMonitor;
import it.unife.ml.probowlapi.monitor.SilentRendererTimeExplanationProgressMonitor;
import java.util.HashSet;
import java.util.Set;
import org.mindswap.pellet.PelletOptions;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class PelletGlassBoxExplanationReasoner extends AbstractExplanationReasoner {

    private static final Logger logger = LoggerFactory.getLogger(BlackBoxExplanationReasoner.class);

    protected OWLReasonerFactory reasonerFactory = PelletReasonerFactory.getInstance();

    protected OWLReasoner reasoner;

    /**
     * Used to convert an axiom to its negation.
     */
    protected SatisfiabilityConverter converter = new SatisfiabilityConverter(OWLManager.getOWLDataFactory());

    @Override
    public ExplanationReasonerResult explainAxiom(OWLAxiom axiom) throws OWLException, ObjectNotInitializedException {

        Set<Set<OWLAxiom>> explanations = new HashSet<>();

        // create an instance of the class that computes all the explanations.
        // HSTExplanationGenerator is a class that implements the Hitting Set Tree
        // algorithm.
        TransactionAwareSingleExpGen explanationGen = new BundleGlassBoxExplanation((PelletReasoner) reasoner);
        MultipleExplanationGenerator expGen = new BundleHSTExplanationGenerator(explanationGen);

        BundleRendererExplanationProgressMonitor rendererMonitor;
        if (verbose) {
            rendererMonitor = new LogRendererTimeExplanationProgressMonitor(axiom, logger);
        } else {
            rendererMonitor = new SilentRendererTimeExplanationProgressMonitor();
        }
        expGen.setProgressMonitor(rendererMonitor);

        OWLClassExpression unsatClass = converter.convert(axiom);

        rendererMonitor.setParamAndStart(getReasoningTimeout());

        // obtain all the explanations
        explanations = expGen.getExplanations(unsatClass, getMaxExplanations());

        // dispose explanation generator
        expGen.dispose();

        rendererMonitor.stopMonitoring();

        if (explanations.isEmpty()) {
            rendererMonitor.foundNoExplanations();
        }
        return new ExplanationReasonerResultImpl(axiom, explanations);

    }

    @Override
    public void dispose() {
        PelletOptions.USE_TRACING = false;
        reasoner.dispose();
    }

    @Override
    public void init() throws ExceptionInInitializerError {
        PelletOptions.USE_TRACING = true;

        BundleGlassBoxExplanation.setup();
//            GlassBoxExplanation.setup();

        reasoner = reasonerFactory.createNonBufferingReasoner(rootOntology);
//        converter = new SatisfiabilityConverter(this.rootOntology.getOWLOntologyManager().getOWLDataFactory());
    }

}
