/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.pellet;

import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import it.unife.ml.bundle.explreasoner.BlackBoxExplanationReasoner;
import org.mindswap.pellet.PelletOptions;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class PelletBlackBoxExplanationReasoner extends BlackBoxExplanationReasoner {

    public PelletBlackBoxExplanationReasoner() {
        super(PelletReasonerFactory.getInstance());
    }

    @Override
    public void init() {
        PelletOptions.USE_TRACING = true;
        super.init();
    }

    @Override
    public void dispose() {
        PelletOptions.USE_TRACING = false;
        reasoner.dispose();
    }

}
