/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.utilities;

import com.clarkparsia.owlapi.explanation.io.manchester.TextBlockWriter;
import com.google.common.collect.BiMap;
import it.unife.ml.bundle.exception.IllegalValueException;
//import org.semanticweb.owlapi.manchestersyntax.renderer.ManchesterOWLSyntaxObjectRenderer;
import java.io.StringWriter;
//import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDFactory;
import org.semanticweb.owlapi.model.AddImport;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;
import uk.ac.manchester.cs.owl.owlapi.OWLAnnotationPropertyImpl;
import it.unife.ml.math.ApproxDouble;
import it.unife.ml.probowlapi.constants.UniFeIRI;
import it.unife.ml.probowlapi.renderer.ManchesterOWLSyntaxObjectRendererExt;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.TreeSet;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class containing some useful methods. To reorganize
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota
 * <giuseppe.cota@unife.it>
 */
public class BundleUtilities {

    // public static final OWLAnnotationProperty PROBABILISTIC_ANNOTATION_PROPERTY = new OWLAnnotationPropertyImpl(IRI.create(UniFeIRI.DISPONTE + "#probability"));
    private static final Logger logger = LoggerFactory.getLogger(BundleUtilities.class);

    /**
     * This method converts an OWL object to Manchester syntax.
     *
     * @param owlObject
     * @return a String representing the object owlObject
     */
    public static String getManchesterSyntaxString(OWLObject owlObject) {
        StringWriter stringWriter = new StringWriter();
        TextBlockWriter printWriter = new TextBlockWriter(stringWriter);
        ManchesterOWLSyntaxObjectRendererExt manch = new ManchesterOWLSyntaxObjectRendererExt(printWriter, new SimpleShortFormProvider());
        owlObject.accept(manch);
        return stringWriter.toString();
    }

    /**
     * It returns a copy of a given OWL ontology
     *
     * @param manager
     * @param ontology
     * @return a new instance that is a copy of the input ontology
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyOntology(OWLOntologyManager manager, OWLOntology ontology) throws OWLOntologyCreationException {
        OWLOntology ontologyCopy = manager.createOntology();
        manager.addAxioms(ontologyCopy, ontology.getAxioms());

        for (OWLImportsDeclaration d : ontology.getImportsDeclarations()) {
            manager.applyChange(new AddImport(ontologyCopy, d));
        }

        return ontologyCopy;
    }

    /**
     * It returns a copy of a given OWL ontology
     *
     * @param ontology
     * @return a new instance that is a copy of the input ontology
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyOntology(OWLOntology ontology) throws OWLOntologyCreationException {
        return copyOntology(ontology.getOWLOntologyManager(), ontology);
    }

    /**
     * It returns an ontology containing only the certain axioms of a given
     * ontology.
     *
     * @param manager
     * @param ontology
     * @return a new instance that is a copy of the input ontology to which the probabilistic axioms have been removed
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyCertainOntology(OWLOntologyManager manager, OWLOntology ontology) throws OWLOntologyCreationException {
        OWLOntology ontologyCopy = BundleUtilities.copyOntology(manager, ontology);
        // probabilistic axioms to remove
        Set<OWLAxiom> probAxioms = BundleUtilities.getProbabilisticAxioms(ontologyCopy);
        manager.removeAxioms(ontologyCopy, probAxioms);
        return ontologyCopy;
    }

    /**
     * Returns a copy of a given ontology containing only the probabilistic
     * axioms
     *
     * @param manager
     * @param ontology
     * @return The copy of ontology with only the probabilistic axioms
     * @throws OWLOntologyCreationException
     */
    public static OWLOntology copyProbabilisticOntology(OWLOntologyManager manager,
            OWLOntology ontology) throws OWLOntologyCreationException {
        Set<OWLAxiom> probAxioms = BundleUtilities.getProbabilisticAxioms(ontology);
        OWLOntology ontologyCopy = manager.createOntology();
        manager.addAxioms(ontologyCopy, probAxioms);
        return ontologyCopy;
    }

    /**
     * It returns the set of the probabilistic axioms.
     *
     * @param ontology
     * @return the set of probabilistic axioms contained in the input ontology
     */
    public static SortedSet<OWLAxiom> getProbabilisticAxioms(OWLOntology ontology) {
        SortedSet<OWLAxiom> probAxioms = new TreeSet<>();
        for (OWLAxiom axiom : ontology.getLogicalAxioms(Imports.INCLUDED)) {
            if (BundleUtilities.getAxiomProbabilisticAnnotations(axiom).size() > 0) {
                probAxioms.add(axiom);
            }
        }
        return probAxioms;
    }

    /**
     * It returns the set of the probabilistic annotations associated to the
     * axioms.
     *
     * @param axiom
     * @return the set of probabilistic annotations of the axiom. Returns an
     * empty set if no probabilistic annotation is present.
     */
    public static Set<OWLAnnotation> getAxiomProbabilisticAnnotations(OWLAxiom axiom) {
        Set<OWLAnnotation> probAnnotations = new TreeSet<>();
        probAnnotations = axiom.getAnnotations(new OWLAnnotationPropertyImpl(IRI.create(UniFeIRI.DISPONTE + "#probability")));
        probAnnotations.addAll(axiom.getAnnotations(new OWLAnnotationPropertyImpl(IRI.create(UniFeIRI.DISPONTE_HTTP + "#probability")))); // Retro-compatibility
        probAnnotations.addAll(axiom.getAnnotations(new OWLAnnotationPropertyImpl(IRI.create(UniFeIRI.DISPONTE_OLD + "#probability")))); // Retro-compatibility
        probAnnotations.addAll(axiom.getAnnotations(new OWLAnnotationPropertyImpl(IRI.create(UniFeIRI.DISPONTE_ML_HTTP + "#probability")))); // Retro-compatibility
        probAnnotations.addAll(axiom.getAnnotations(new OWLAnnotationPropertyImpl(IRI.create(UniFeIRI.DISPONTE_ML + "#probability")))); // Retro-compatibility
        return probAnnotations;
    }

    public static OWLAxiom getAxiomWithoutProbabilisticAnnotations(OWLAxiom axiom) {
        Set<OWLAnnotation> annotations = axiom.getAnnotations();
        Set<OWLAnnotation> probAnnotations = getAxiomProbabilisticAnnotations(axiom);
        annotations.removeAll(probAnnotations);
        return axiom.getAxiomWithoutAnnotations().getAnnotatedAxiom(annotations);
    }

    /**
     * It converts the input time string into the corresponding integer value in
     * milliseconds.
     *
     * @param stringTime The input string in the format -h-m-s, where '-' are
     * the placeholder for the integer values of hours (h), minutes (m), seconds
     * (s). It is not mandatory that the string contains all the four values,
     * but the order must be followed.
     * @return the long value in milliseconds of the time specified in
     * stringTime.
     */
    public static long convertTimeValue(String stringTime) {

        int h = 0, m = 0, s = 0;

        if (stringTime != null) {
            if (stringTime.matches("([0-9]+h){0,1}([0-9]+m){0,1}([0-9]+s){0,1}")) {
                String[] div = stringTime.split("\\d+");
                String[] val = stringTime.split("\\D+");

                try {

                    for (int i = 0; i < val.length; i++) {
                        if (div[i + 1].equals("h")) {
                            h = Integer.parseInt(val[i]);
                        } else if (div[i + 1].equals("m")) {
                            m = Integer.parseInt(val[i]);
                        } else if (div[i + 1].equals("s")) {
                            s = Integer.parseInt(val[i]);
                        }
                    }
                    int time = ((((h * 60) + m) * 60) + s) * 1000;
                    if (time > 0) {
                        return time;
                    } else {
                        return Long.MAX_VALUE;
                    }

                } catch (Exception e) {
                    logger.warn("Incorrect format for maximum execution time. Time setted to 0 (infinite)!");
                    return Long.MAX_VALUE;
                }
            } else {
                logger.warn("Incorrect format for maximum execution time. Time setted to 0 (infinite)!");
                return Long.MAX_VALUE;
            }
        } else {
            return Long.MAX_VALUE;
        }

    }

    /**
     * Takes an ontology and returns a Map containing all the probabilistic
     * axioms with their probabilities.
     *
     * @return a map containing as key an axiom without annotations and as value
     * the probability of that axiom
     */
    public static Map<OWLAxiom, ApproxDouble> createPMap(OWLOntology ontology) {
        return createPMap(ontology, false, false, false, null);
    }

    /**
     * Takes an ontology and returns a Map containing all the probabilistic
     * axioms with their probabilities.
     *
     * @param showAll
     * @return a map containing as key an axiom without annotations and as value
     * the probability of that axiom
     */
    public static Map<OWLAxiom, ApproxDouble> createPMap(OWLOntology ontology,
            boolean showAll) {
        return createPMap(ontology, showAll, false, false, null);
    }

    /**
     * Takes an ontology and returns a Map containing all the probabilistic
     * axioms with their probabilities.
     *
     * @param randomize if true a random probability is assigned to the
     * probabilistic axioms
     * @param probabilizeAll if true all the logical axioms in the ontology
     * becomes probabilistic. If probabilizeAll is true and randomize is false,
     * then the probabilistic axioms keep their probability unchanged, wheras
     * the logical non-probabilistic axioms become probabilistic and their
     * probability is a random value.
     * @param randomSeed random seed for generating probability values. Used if
     * randomized is true.
     * @return a map containing as key an axiom without annotations and as value
     * the probability of that axiom
     */
    public static Map<OWLAxiom, ApproxDouble> createPMap(OWLOntology ontology,
            boolean randomize, boolean probabilizeAll,
            Integer randomSeed) {
        return createPMap(ontology, false, randomize, probabilizeAll, randomSeed);
    }

    /**
     * Takes an ontology and returns a Map containing all the probabilistic
     * axioms with their probabilities.
     *
     * @param showAll
     * @param randomize give a random probability to the probabilistic axioms
     * @param probabilizeAll if true all the axioms in the ontology becomes
     * probabilistic
     * @param randomSeed
     * @return a map containing as key an axiom without annotations and as value
     * the probability of that axiom
     */
    public static Map<OWLAxiom, ApproxDouble> createPMap(OWLOntology ontology,
            boolean showAll, boolean randomize, boolean probabilizeAll,
            Integer randomSeed) {

        logger.debug("Preparing Probability Map...");
        //System.out.println("Preparing ApproxDouble Map...");

        Map<OWLAxiom, ApproxDouble> pMap = new HashMap<>();

        Random probGenerator = new Random();

        if (randomize) {
            if (randomSeed != null) {
                probGenerator.setSeed(randomSeed);
                logger.debug("Random Seed set to: " + randomSeed);
            }
        }

        Set<? extends OWLAxiom> axioms;
        if (probabilizeAll) {
            axioms = ontology.getLogicalAxioms();
        } else {
            axioms = getProbabilisticAxioms(ontology);
        }

        for (OWLAxiom axiom : axioms) {
            List<ApproxDouble> probList = new ArrayList<>();

            Set<OWLAnnotation> probabilisticAnnotations = BundleUtilities.getAxiomProbabilisticAnnotations(axiom);

            if (probabilisticAnnotations.size() > 0) {
                for (OWLAnnotation annotation : BundleUtilities.getAxiomProbabilisticAnnotations(axiom)) {

                    // method to add the pair axiom/probability to the Map
                    if (annotation.getValue() != null) {

                        ApproxDouble annProbability;
                        if (randomize) {
                            annProbability = new ApproxDouble(probGenerator.nextDouble());
                            probList.add(annProbability);
                        } else {
//                            String annotationStr = annotation.getValue().toString().trim();
                            String annotationStr = annotation.getValue().asLiteral().get().getLiteral().trim();
                            annotationStr = annotationStr.replaceAll("\"", "");
                            if (annotationStr.contains("^^")) {
                                annotationStr = annotationStr.substring(0, annotationStr.indexOf("^^"));
                            }
                            annProbability = new ApproxDouble(Double.parseDouble(annotationStr));
                            probList.add(annProbability);

                        }
                        if (showAll) {
                            String axiomName = getManchesterSyntaxString(axiom);
//                            System.out.print(axiomName);
                            String str = " => " + annProbability;
//                            System.out.println(str);
                            logger.debug("{}", axiomName + str);
                        }
                    }
                }

            } else {
                probList.add(new ApproxDouble(probGenerator.nextDouble()));
            }

            if (probList.size() > 0) {
                OWLAxiom pMapAxiom = axiom.getAxiomWithoutAnnotations();
                ApproxDouble varProbTemp = new ApproxDouble(ApproxDouble.zero());
//                if (pMap.containsKey(pMapAxiom)) {
//                    varProbTemp = pMap.get(pMapAxiom);
//                }
                for (ApproxDouble ithProb : probList) { // combine multiple probabilities
                    ApproxDouble mul = new ApproxDouble(varProbTemp)._multiply(ithProb);
                    varProbTemp._add(ithProb)._subtract(mul);
                }
                pMap.put(pMapAxiom, varProbTemp);
            }

        }

        if (probabilizeAll) {
//            System.out.println("Created " + axioms.size() + " probabilistic axiom");
            logger.info("Created " + axioms.size() + " probabilistic axiom");
        }

        if (pMap.size() > 0) {
//            System.out.println("ApproxDouble Map computed. Size: " + pMap.size());
            logger.info("Probability Map computed. Size: " + pMap.size());
        } else {
//            System.out.println("ApproxDouble Map computed. Size: " + pMap.size());
            logger.warn("Probability Map is empty");
        }
        return pMap;

    }

    /**
     * Build the BDD corresponding to the given set of explanations.
     *
     * @param explanations the set of explanations for which the method
     * calculates the corresponding BDD
     * @param bddF The BDD factory used to create the BDD
     * @param pMap The Probabilistic Map containing the pairs axiom-probability
     * @param usedAxioms
     * @return the resulting BDD
     */
    @Deprecated
    public static BDD buildBDD(Set<Set<OWLAxiom>> explanations,
            BDDFactory bddF,
            Map<OWLAxiom, ApproxDouble> pMap,
            List<OWLAxiom> usedAxioms) {
        BDD bdd = bddF.zero();

        //for all explanation (minA) in explanations (CS)
        for (Iterator<Set<OWLAxiom>> it = explanations.iterator(); it.hasNext();) {
            BDD bdde = bddF.one();
            Set<OWLAxiom> s = it.next();
            //for all axiom in explanation
            for (Iterator<OWLAxiom> it1 = s.iterator(); it1.hasNext();) {
                boolean stop = false;
                BDD bdda;
                OWLAxiom s1 = it1.next();
//                String s1name = BundleUtilities.getManchesterSyntaxString(s1);
                //System.out.println(stringWriter.toString());
                // TO DO: TO CHECK AND FIX (Che bisogna fare?)
//                if (s1name.startsWith("Transitive")) {
//                    String[] splits1name = s1name.split(" ", 2);
//                    s1name = splits1name[1] + " type " + splits1name[0] + "Property";
//                }

                if (!pMap.containsKey(s1.getAxiomWithoutAnnotations())) {
                    bdda = bddF.one();
                } else {
                    // Creazione e inizializzazione di Res
                    ApproxDouble probAx = pMap.get(s1);

                    bdda = bddF.zero();

                    if (stop) {
                        bdde.andWith(bdda);
                        //printCalculateBDD(s, s1, key, axiomVar);
                        break;
                    }

                    int i;
                    boolean found = false;
                    for (i = 0; i < usedAxioms.size(); i++) {
                        if (usedAxioms.get(i).equalsIgnoreAnnotations(s1)) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        usedAxioms.add(s1.getAxiomWithoutAnnotations());
                        if (i >= bddF.varNum()) {
                            bddF.setVarNum(bddF.varNum() + 1);
                        }
                    }
                    BDD b = bddF.ithVar(i);
                    bdda.orWith(b);

                }

                bdde.andWith(bdda);
            }

            bdd.orWith(bdde);
        }
        return bdd;
    }

    /**
     * Build the BDD corresponding to the given set of explanations. It tackes
     * as input a set of explanations, a BDD Factory, a probabilistic map
     * containing the pairs (axiom, probability) and an empty map that will be
     * populated by this method and it will contain the pairs (axiom, id-axiom),
     * where id-axiom is an integer that identifies the axiom in the BDD that
     * will be returned. This method return a BDD that represents a boolean
     * formula where each variable is an axiom of the explanations. For more
     * information read the main paper about BUNDLE.
     *
     * Riguzzi, Fabrizio, et al."BUNDLE: A reasoner for probabilistic
     * ontologies." International Conference on Web Reasoning and Rule Systems.
     * Springer, Berlin, Heidelberg, 2013.
     *
     * @param explanations the set of explanations for which the method
     * calculates the corresponding BDD
     * @param bddF The BDD factory used to create the BDD
     * @param pMap The Probabilistic Map containing the pairs (axiom,
     * probability)
     * @param usedAxioms An empty map. When the method terminates the map will
     * contain the pairs (axiom, id-axiom)
     * @return the resulting BDD
     */
    public static BDD buildBDD(Set<Set<OWLAxiom>> explanations,
            BDDFactory bddF,
            Map<OWLAxiom, ApproxDouble> pMap,
            BiMap<OWLAxiom, Integer> usedAxioms) {
        BDD bdd = bddF.zero();

        //for all explanation (minA) in explanations (CS)
        for (Set<OWLAxiom> explanation : explanations) {
            BDD bdde = bddF.one();
//            Set<OWLAxiom> s = it.next();
            //for all axiom in explanation
            for (OWLAxiom axiom : explanation) {
                boolean stop = false;
                BDD bdda;
                axiom = axiom.getAxiomWithoutAnnotations();
//                String s1name = BundleUtilities.getManchesterSyntaxString(s1);
                //System.out.println(stringWriter.toString());
                // TO DO: TO CHECK AND FIX (Che bisogna fare?)
//                if (s1name.startsWith("Transitive")) {
//                    String[] splits1name = s1name.split(" ", 2);
//                    s1name = splits1name[1] + " type " + splits1name[0] + "Property";
//                }

                if (!pMap.containsKey(axiom)) {
                    bdda = bddF.one();
                } else {
                    bdda = bddF.zero();

                    if (stop) {
                        bdde.andWith(bdda);
                        //printCalculateBDD(s, s1, key, axiomVar);
                        break;
                    }

                    Integer i = usedAxioms.get(axiom);

                    if (i == null) {
                        i = usedAxioms.values().stream().max(Integer::compareTo).orElse(-1);
                        i++;
                        usedAxioms.put(axiom, i);
                        if (usedAxioms.size() > bddF.varNum()) {
//                            i = bddF.varNum();
                            bddF.setVarNum(bddF.varNum() + 1);
                        }
                    }

//                    if (i == null) {
//                        if (usedAxioms.size() < bddF.varNum()) {
//                            String msg = "Wrong axiom-id during building of the BDD";
//                            logger.error(msg);
//                            throw new IllegalValueException(msg);
//                        }
//                        i = bddF.varNum();
//                        bddF.setVarNum(bddF.varNum() + 1);
//                        usedAxioms.put(axiom, i);
//                    }
                    BDD b = bddF.ithVar(i);
                    bdda.orWith(b);

                }

                bdde.andWith(bdda);
            }

            bdd.orWith(bdde);
        }
        return bdd;
    }

    /**
     * Computes the probability of the given BDD. This is a recursive method
     * that takes as input a BDD (that is the current node) and a map between
     * all the already computed nodes and the corresponding probabilities in
     * order to avoid to recompute the probability of already visited nodes
     *
     * @param node the current BDD
     * @param nodes all the already computed probability with the corresponding
     * BDD
     * @param pMap
     * @param usedAxioms
     * @return the probability of the BDD
     */
    @Deprecated
    public static ApproxDouble probabilityOfBDD(BDD node,
            Map<BDD, ApproxDouble> nodes,
            Map<OWLAxiom, ApproxDouble> pMap,
            List<OWLAxiom> usedAxioms) {
        if (node.isOne()) {
            return ApproxDouble.one();
        } else if (node.isZero()) {
            return ApproxDouble.zero();
        } else {

            ApproxDouble value_p = getValue(node, nodes);
            if (value_p != null) {
                return value_p;
            } else {
//                ApproxDouble p = new ApproxDouble(prob[node.var()]);
                ApproxDouble p = new ApproxDouble(pMap.get(usedAxioms.get(node.var())));
                ApproxDouble notP = new ApproxDouble(ApproxDouble.one());
                notP._subtract(p);
                p._multiply(probabilityOfBDD(node.high(), nodes, pMap, usedAxioms));
                notP._multiply(probabilityOfBDD(node.low(), nodes, pMap, usedAxioms));
                p._add(notP);
                add_node(node, p, nodes);
                return p;
            }

        }
    }

    /**
     * Computes the probability of the given BDD. This is a recursive method
     * that takes as input a BDD (that is the current node) and a map between
     * all the already computed nodes and the corresponding probabilities in
     * order to avoid to recompute the probability of already visited nodes.
     *
     * @param node the current BDD
     * @param nodes all the already computed probability with the corresponding
     * BDD
     * @param pMap
     * @param usedAxioms
     * @return the probability of the BDD
     */
    public static ApproxDouble probabilityOfBDD(BDD node,
            Map<BDD, ApproxDouble> nodes,
            Map<OWLAxiom, ApproxDouble> pMap,
            BiMap<OWLAxiom, Integer> usedAxioms) {
        if (node.isOne()) {
            return ApproxDouble.one();
        } else if (node.isZero()) {
            return ApproxDouble.zero();
        } else {

            ApproxDouble value_p = getValue(node, nodes);
            if (value_p != null) {
                return value_p;
            } else {
//                ApproxDouble p = new ApproxDouble(prob[node.var()]);
                ApproxDouble p = new ApproxDouble(pMap.get(usedAxioms.inverse().get(node.var())));
                ApproxDouble notP = new ApproxDouble(ApproxDouble.one());
                notP._subtract(p);
                p._multiply(probabilityOfBDD(node.high(), nodes, pMap, usedAxioms));
                notP._multiply(probabilityOfBDD(node.low(), nodes, pMap, usedAxioms));
                p._add(notP);
                add_node(node, p, nodes);
                return p;
            }

        }
    }

    /**
     * Looks for the given BDD in the map between already visited BDDs an
     * probabilities
     *
     * @param node the current BDD
     * @param nodes all the already computed probability with the corresponding
     * BDD
     * @return null if not yet visited or the probability previously computed
     */
    private static ApproxDouble getValue(BDD node, Map<BDD, ApproxDouble> nodes) {
        if (nodes.containsKey(node)) {
            return nodes.get(node);
        } else {
            return null;
        }
    }

    /**
     * Adds to the map of the already visited node a new node.
     *
     * @param node the new node (BDD) to be added
     * @param pt the probability of node
     * @param nodes all the already computed probability with the corresponding
     * BDD
     */
    private static void add_node(BDD node, ApproxDouble pt, Map<BDD, ApproxDouble> nodes) {
        nodes.put(node, pt);
    }

    /**
     * Loads OWLOntology from String and returns it.
     *
     * @param ontoloyStr the ontology in string format (supposed charset UTF-8)
     * @return an instance of OWLOntology
     * @throws org.semanticweb.owlapi.model.OWLOntologyCreationException
     */
    public static OWLOntology loadOntologyFromString(String ontoloyStr) throws OWLOntologyCreationException {
        return loadOntologyFromString(ontoloyStr, StandardCharsets.UTF_8);
    }

    /**
     * Loads OWLOntology from String and returns it.
     *
     * @param ontoloyStr the ontology in string format
     * @param charset charset of the string
     * @return an instance of OWLOntology
     * @throws org.semanticweb.owlapi.model.OWLOntologyCreationException
     */
    public static OWLOntology loadOntologyFromString(String ontoloyStr, Charset charset) throws OWLOntologyCreationException {
        OWLOntologyManager man = OWLManager.createOWLOntologyManager();
        InputStream stream = new ByteArrayInputStream(ontoloyStr.getBytes(charset));
        return man.loadOntologyFromOntologyDocument(stream);
    }
}
