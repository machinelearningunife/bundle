/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.utilities;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Constants {

    public static final String CONSOLE_APPENDER = "APPENDER_OUT";

    /**
     * Enumeration of available reasoner implementations that can be used via
     * OWL API interface.
     *
     * @author Giuseppe Cota <giuseppe.cota@unife.it>
     */
    public enum ReasonerName {
        PELLET,
        HERMIT,
        JFACT,
        ELK,
        OWLLINK,
        FACTPP,
        STRUCTURAL,
        TRILL,
        TRILLP,
        TORNADO
    }

    /**
     * Enumeration of the available methods used to retrieve the explanations
     * during the Hitting Set Tree algorithm. If we are using Pellet it is
     * preferable to use the method GlassBox to obtain the explanations.
     * However, there are some issues with KBs that contain DisjointUnion
     * axioms.
     */
    public enum HSTMethod {
        NONE,
        GLASSBOX,
        BLACKBOX,
        OWLEXPLANATION
    }

    public enum Timings {
        MAIN("main"),
        INIT("init"),
        EXPLANATION("explain"),
        BDD_COMPUTATION("bddCalc"),
        LOAD_EXT_LIBRARIES("loadExtLib");

        private final String text;

        /**
         * @param text
         */
        Timings(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
     * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }
}
