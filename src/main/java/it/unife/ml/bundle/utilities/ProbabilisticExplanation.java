package it.unife.ml.bundle.utilities;

import it.unife.ml.math.ApproxDouble;
import org.semanticweb.owlapi.model.OWLAxiom;

import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Set;

/**
 * Class representing a probabilistic explanation containing the explanation and its probability,
 *
 * @author Riccardo Zese <riccardo.zese@unife.it>, Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ProbabilisticExplanation implements Comparable<ProbabilisticExplanation>{

    @NotNull
    private Set<OWLAxiom> explanation;
    @NotNull
    private ApproxDouble explanationProbability;

    public ProbabilisticExplanation(@NotNull Set<OWLAxiom> explanation) {
        this.explanation = explanation;
        this.explanationProbability = new ApproxDouble(1.0);
    }

    public ProbabilisticExplanation(@NotNull Set<OWLAxiom> explanation, ApproxDouble explanationProbability) {
        this.explanation = explanation;
        this.explanationProbability = explanationProbability;
    }

    public ProbabilisticExplanation(@NotNull Set<OWLAxiom> explanation, Map<OWLAxiom, ApproxDouble> pMap) {
        this.explanation = explanation;
        this.explanationProbability = new ApproxDouble(1.0);
        setExplanationProbability(pMap);
    }

    public Set<OWLAxiom> getExplanation() {
        return explanation;
    }

    public void setExplanation(Set<OWLAxiom> explanation) {
        this.explanation = explanation;
    }

    public ApproxDouble getExplanationProbability() {
        return explanationProbability;
    }

    public void setExplanationProbability(ApproxDouble explanationProbability) {
        this.explanationProbability = explanationProbability;
    }

    public void setExplanationProbability(Map<OWLAxiom, ApproxDouble> pMap) {

        for (OWLAxiom ax:explanation) {
            ApproxDouble axP = pMap.get(ax.getAxiomWithoutAnnotations());
            if (axP != null) explanationProbability._multiply(axP);
        }
    }

    /**
     * Compares two probabilistic explanation sorting them on the basis of their probability.
     * In case the probabilities are equal, the length of the explanations are considered (shorter first).
     *
     * @param probabilisticExplanation the probabilistic explanation to compare with
     * @return an integer value of the comparison
     */
    @Override
    public int compareTo(ProbabilisticExplanation probabilisticExplanation) {
        if (this == probabilisticExplanation) return 0;

        int probabilityComparison = this.explanationProbability.compareTo(probabilisticExplanation.getExplanationProbability());

        if (probabilityComparison != 0)
            return -probabilityComparison;
        else {
            int lengthComparison = this.getExplanation().size() - probabilisticExplanation.getExplanation().size();
            if (lengthComparison != 0)
                return lengthComparison;
            else
                return this.getExplanation().hashCode() - probabilisticExplanation.getExplanationProbability().hashCode();
        }
    }

    @Override
    public String toString() {
        String output = "Explanation probability: " + explanationProbability.toString();
        for (OWLAxiom ax: explanation) {
            output += "\n    " + BundleUtilities.getManchesterSyntaxString(ax);
        }
        return output;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProbabilisticExplanation)) return false;

        ProbabilisticExplanation that = (ProbabilisticExplanation) o;

        if (!getExplanation().equals(that.getExplanation())) return false;
        return getExplanationProbability() != null ? getExplanationProbability().equals(that.getExplanationProbability()) : that.getExplanationProbability() == null;
    }

    @Override
    public int hashCode() {
        int result = getExplanation().hashCode();
        result = 31 * result + (getExplanationProbability() != null ? getExplanationProbability().hashCode() : 0);
        return result;
    }

}
