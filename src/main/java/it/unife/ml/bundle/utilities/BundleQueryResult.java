/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.utilities;

//import java.math.BigDecimal;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.BiMap;
import java.util.*;

import it.unife.ml.probowlapi.core.ExplanationReasonerResult;
import org.semanticweb.owlapi.model.OWLAxiom;
import net.sf.javabdd.BDD;
import org.mindswap.pellet.utils.Timers;
import it.unife.ml.math.ApproxDouble;
import it.unife.ml.probowlapi.core.ProbabilisticExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;

import javax.validation.constraints.NotNull;

/**
 * Class containing the results of a query: explanations, probabilities,
 * usedAxioms, BDDs
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>, Riccardo Zese
 * <riccardo.zese@unife.it>
 */
public class BundleQueryResult implements ProbabilisticExplanationReasonerResult {

    @NotNull
    private OWLAxiom query;
    private ApproxDouble queryProbability;

    private BDD bdd;

    @NotNull
    private Set<ProbabilisticExplanation> queryExplanations;
//    @NotNull
//    private Map<OWLAxiom, ApproxDouble> usedAxioms;
    
    /**
     * When building the BDD we associate each axiom with and integer value
     * assigned by BDDFactory,varNum();
     */
    @Deprecated
    private HashBiMap<OWLAxiom, Integer> usedProbabilisticAxiomsBiMap;

    private HashMap<OWLAxiom,ApproxDouble> usedProbabilisticAxioms;
    
//    private List<ApproxDouble> probOfAxioms;

    private int numberOfExplanations;

    private Timers timers;

    public BundleQueryResult(OWLAxiom query, ApproxDouble queryProbability) {
        this.query = query;
        this.queryProbability = queryProbability;
        queryExplanations = new HashSet<>();
        numberOfExplanations = 0;
//        this.usedAxioms = new HashMap<>();
    }

    public BundleQueryResult(OWLAxiom query) {
        this.query = query;
        this.queryProbability = null;
        queryExplanations = new HashSet<>();
        numberOfExplanations = 0;
//        this.usedAxioms = new HashMap<>();
    }

    public BundleQueryResult(ExplanationReasonerResult result) {
        this.query = result.getQuery();
        this.queryProbability = null;
        setQueryExplanations(result.getQueryExplanations());
//        this.usedAxioms = new HashMap<>();
        this.timers = result.getTimers();
    }

    public BundleQueryResult(ProbabilisticExplanationReasonerResult result) {
        this.query = result.getQuery();
        this.queryProbability = result.getQueryProbability();
        setQueryExplanations(result.getQueryExplanations());
//        this.usedAxioms = new HashMap<>();
        this.timers = result.getTimers();
    }

    public BundleQueryResult(ProbabilisticExplanationReasonerResult result, Map<OWLAxiom,ApproxDouble> pMap) {
        this.query = result.getQuery();
        this.queryProbability = result.getQueryProbability();
        setQueryExplanations(result.getQueryExplanations(),pMap);
//        this.usedAxioms = new HashMap<>();
        this.timers = result.getTimers();
    }

    public BundleQueryResult(ProbabilisticReasonerResult result) {
        this.query = result.getQuery();
        this.queryProbability = result.getQueryProbability();
        queryExplanations = new HashSet<>();
//        this.usedAxioms = new HashMap<>();
        this.timers = result.getTimers();
    }

    /**
     * @return the explanations
     */
    @Override
    public Set<Set<OWLAxiom>> getQueryExplanations() {
        Set<Set<OWLAxiom>> explanations = new HashSet<>();
        for (ProbabilisticExplanation probExpl : queryExplanations) {
            explanations.add(probExpl.getExplanation());
        }
        return explanations;
    }

    /**
     * @return the explanations with their probability
     */
    public Set<ProbabilisticExplanation> getQueryProbabilisticExplanations() {
        return queryExplanations;
    }

    /**
     * @param queryExplanations the explanations to set
     */
    public void setQueryExplanations(Set<Set<OWLAxiom>> queryExplanations) {
        this.queryExplanations = new HashSet<>();
        for (Set<OWLAxiom> expl : queryExplanations) {
            this.queryExplanations.add(new ProbabilisticExplanation(expl));
        }
        numberOfExplanations = queryExplanations.size();
    }

    public void setQueryExplanations(Set<Set<OWLAxiom>> queryExplanations, Map<OWLAxiom,ApproxDouble> pMap) {
        this.queryExplanations = new HashSet<>();
        for (Set<OWLAxiom> expl : queryExplanations) {
            this.queryExplanations.add(new ProbabilisticExplanation(expl,pMap));
        }
        numberOfExplanations = queryExplanations.size();
    }

    public void setQueryProbabilisticExplanations(Set<ProbabilisticExplanation> queryExplanations) {
        this.queryExplanations = queryExplanations;
        numberOfExplanations = queryExplanations.size();
    }

//    /**
//     * @return the probOfAxioms
//     */
//    public List<ApproxDouble> getProbOfAxioms() {
//        return probOfAxioms;
//    }
//
//    /**
//     * @param probOfAxioms the probOfAxioms to set
//     */
//    public void setProbOfAxioms(List<ApproxDouble> probOfAxioms) {
//        this.probOfAxioms = probOfAxioms;
//    }
    /**
     * @return the queryProbability
     */
    @Override
    public ApproxDouble getQueryProbability() {
        return queryProbability;
    }

    /**
     * @param probability the queryProbability to set
     */
    public void setQueryProbability(ApproxDouble probability) {
        this.queryProbability = probability;
    }

    public void merge(BundleQueryResult qr) {
        queryExplanations.addAll(qr.getQueryProbabilisticExplanations());
    }

    /**
     * @return the number of explanations
     */
    public int getNumberOfExplanations() {
        return numberOfExplanations;
    }

    /**
     * @return the bdd
     */
    public BDD getBDD() {
        return bdd;
    }

    /**
     * @param bdd the bdd to set
     */
    public void setBDD(BDD bdd) {
        this.bdd = bdd;
    }

    @Override
    public String toString() {
        return "N. of Explanations: " + numberOfExplanations
                + " | Probability of the query: " + queryProbability;
    }

    /**
     * @return the timers
     */
    @Override
    public Timers getTimers() {
        return timers;
    }

    /**
     * @param timers the timers to set
     */
    @Override
    public void setTimers(Timers timers) {
        this.timers = timers;
    }

//    /**
//     * @return the usedAxioms
//     */
//    public Map<OWLAxiom, ApproxDouble> getUsedAxioms() {
//        return usedAxioms;
//    }
//
//    /**
//     * @param usedAxioms the usedAxioms to set
//     */
//    public void setUsedAxioms(Map<OWLAxiom, ApproxDouble> usedAxioms) {
//        this.usedAxioms = usedAxioms;
//        for (ProbabilisticExplanation probExpl : this.queryExplanations) {
//            probExpl.setExplanationProbability(usedAxioms);
//        }
//    }

    /**
     * @param query the query to set
     */
    public void setQuery(OWLAxiom query) {
        this.query = query;
    }

    @Override
    public OWLAxiom getQuery() {
        return query;
    }

    public Set<ProbabilisticExplanation> getSortedExplanations() {
        return new TreeSet<>(getQueryProbabilisticExplanations());
    }

    /**
     * @deprecated
     * @return the usedProbabilisticAxiomsBiMap
     */
    @Deprecated
    public HashBiMap<OWLAxiom, Integer> getUsedProbabilisticAxiomsBiMap() {
        return usedProbabilisticAxiomsBiMap;
    }

    /**
     * @deprecated
     * @param usedProbabilisticAxiomsBiMap the usedProbabilisticAxiomsBiMap to set
     */
    @Deprecated
    public void setUsedProbabilisticAxiomsBiMap(HashBiMap<OWLAxiom, Integer> usedProbabilisticAxiomsBiMap) {
        this.usedProbabilisticAxiomsBiMap = usedProbabilisticAxiomsBiMap;
    }

    public HashMap<OWLAxiom, ApproxDouble> getUsedProbabilisticAxioms() {
        return usedProbabilisticAxioms;
    }

    public void setUsedProbabilisticAxioms(HashMap<OWLAxiom, ApproxDouble> usedProbabilisticAxioms) {
        this.usedProbabilisticAxioms = usedProbabilisticAxioms;
    }

    public void setUsedProbabilisticAxioms(@NotNull BiMap<OWLAxiom, Integer> usedProbabilisticAxiomsBiMap, @NotNull Map<OWLAxiom, ApproxDouble> pMap) {
        this.usedProbabilisticAxioms = new HashMap<>();
        for (OWLAxiom entry : usedProbabilisticAxiomsBiMap.keySet()) {
            ApproxDouble p = pMap.get(entry);
            if (p != null)
                this.usedProbabilisticAxioms.put(entry,p);
        }
    }

    public void collectUsedProbabilisticAxioms(@NotNull Map<OWLAxiom, ApproxDouble> pMap) {
        this.usedProbabilisticAxioms = new HashMap<>();
        for (ProbabilisticExplanation entry : queryExplanations) {
            for (OWLAxiom ax : entry.getExplanation()) {
                ApproxDouble p = pMap.get(ax);
                if (p != null)
                    this.usedProbabilisticAxioms.put(ax,p);
            }

        }
    }

}
