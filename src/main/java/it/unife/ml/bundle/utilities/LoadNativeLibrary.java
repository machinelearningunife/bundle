/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.utilities;

import it.unife.ml.bundle.BundleControllerImpl;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that provides static methods to load a dynamic/static native library
 * within a JAR.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LoadNativeLibrary {
    
        private static final Logger logger = LoggerFactory.getLogger(LoadNativeLibrary.class);


    /**
     * Loads library from current JAR archive
     *
     * The file from JAR is copied into system temporary directory and then
     * loaded. The temporary file is deleted after exiting. Method uses String
     * as filename because the pathname is "abstract", not system-dependent.
     *
     * @param path The path of file inside JAR as absolute path (beginning with
     * '/'), e.g. /package/File.ext
     * @throws IOException If temporary file creation or read/write operation
     * fails
     * @throws IllegalArgumentException If source file (param path) does not
     * exist
     * @throws IllegalArgumentException If the path is not absolute or if the
     * filename is shorter than three characters (restriction of {
     * @see File#createTempFile(java.lang.String, java.lang.String)
     * }).
     */
    public static void loadLibraryFromJar(String path) throws IOException {

        
        File tempFile = createTempFile(path);

        // Finally, load the library
        String ext = FilenameUtils.getExtension(tempFile.getName());
        int extPos = tempFile.getAbsolutePath().lastIndexOf(ext);
        String libraryName = tempFile.getAbsolutePath().substring(0, extPos-1);
        System.load(tempFile.getAbsolutePath());
//        System.load(libraryName);
        logger.debug("Loaded library {}", tempFile.getAbsolutePath());
    }

    public static File createTempFile(String path, String directory) throws IOException {
        if (!path.startsWith("/")) {
            throw new IllegalArgumentException("The path has to be absolute (start with '/').");
        }

        // Obtain basename from path
        String baseName = FilenameUtils.getBaseName(path);
        // Obtain extension from path
        String ext = "." + FilenameUtils.getExtension(path);

        // Check if the basename is okay
        if (baseName.isEmpty()) {
            throw new IllegalArgumentException("The filename is an empty string");
        }
        if (baseName.length() < 3) {
            throw new IllegalArgumentException("The filename has to be at least 3 characters long.");
        }

        // Prepare temporary file
        File dir = null;
        if (directory != null) {
            dir = new File(directory);
            if (!dir.isDirectory()) {
                throw new IOException(dir.getAbsolutePath() + " is not a directory");
            }
        }
//        File tempFile = File.createTempFile(baseName, ext, dir);
        File tempFile = new File(baseName + ext);
        if (!tempFile.exists()) {
            tempFile.createNewFile();
            tempFile.deleteOnExit();

            if (!tempFile.exists()) {
                throw new FileNotFoundException("File " + tempFile.getAbsolutePath() + " does not exist.");
            }

            // Prepare buffer for data copying
            byte[] buffer = new byte[4096];
            int readBytes;

            // Open and check input stream
//            URL t = LoadNativeLibrary.class.getResource(path);
            InputStream is = LoadNativeLibrary.class.getResourceAsStream(path);
            if (is == null) {
                throw new FileNotFoundException("File " + path + " was not found inside JAR.");
            }

            // Open output stream and copy data between source file in JAR and the temporary file
            OutputStream os = new FileOutputStream(tempFile);
            try {
                while ((readBytes = is.read(buffer)) != -1) {
                    os.write(buffer, 0, readBytes);
                }
            } finally {
                // If read/write fails, close streams safely before throwing an exception
                os.close();
                is.close();
            }
//            Files.copy(is, tempFile.toPath());
        }
        logger.debug("Created temp library file in: "+ tempFile.getAbsolutePath());
        return tempFile;
    }

    public static File createTempFile(String path) throws IOException {
        return createTempFile(path, null);
    }

}
