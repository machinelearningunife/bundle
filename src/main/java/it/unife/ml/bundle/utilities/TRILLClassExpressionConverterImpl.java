/**
 * This file is part of BUNDLE.
 *
 * BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 * BUNDLE can be used both as module and as standalone.
 *
 * BUNDLE and all its parts are distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.utilities;

import it.unife.ml.bundle.exception.IllegalValueException;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataExactCardinality;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataMaxCardinality;
import org.semanticweb.owlapi.model.OWLDataMinCardinality;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectExactCardinality;
import org.semanticweb.owlapi.model.OWLObjectHasSelf;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLObjectMinCardinality;
import org.semanticweb.owlapi.model.OWLObjectOneOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class TRILLClassExpressionConverterImpl implements TRILLClassExpressionConverter {

    private Stack<String> stack = new Stack<>();

    @Override
    public String getTRILLClassExpression(OWLClassExpression owlce) {
        owlce.accept(this);
        return stack.pop();
    }

    @Override
    public void visit(OWLClass ce) {
        String conv = ce.getIRI().toString();
        stack.push(conv);
    }

    @Override
    public void visit(OWLObjectIntersectionOf ce) {
        if (ce.getOperands().size() < 2) {
            if (ce.getOperands().size() <= 0) {
                throw new RuntimeException(ce + " wrong number of operands");
            }
            ce.getOperandsAsList().get(0).accept(this);
        } else {
            Set<String> trillOperands = new HashSet<>();
            for (OWLClassExpression c : ce.getOperands()) {
                c.accept(this);
                trillOperands.add(stack.pop());
            }

            String conv = "intersectionOf(";
            for (String trillOperand : trillOperands) {
                conv += trillOperand + ",";
            }
            conv = conv.substring(0, conv.length() - 1);

            conv += ")";
            stack.push(conv);
        }
    }

    @Override
    public void visit(OWLObjectUnionOf ce) {
        if (ce.getOperands().size() < 2) {
            if (ce.getOperands().size() <= 0) {
                throw new RuntimeException(ce + " wrong number of operands");
            }
            ce.getOperandsAsList().get(0).accept(this);
        } else {
            Set<String> trillOperands = new HashSet<>();
            for (OWLClassExpression c : ce.getOperands()) {
                c.accept(this);
                trillOperands.add(stack.pop());
            }

            String conv = "unionOf(";
            for (String trillOperand : trillOperands) {
                conv += trillOperand + ",";
            }

            conv = conv.substring(0, conv.length() - 1);

            conv += ")";
            stack.push(conv);
        }

    }

    @Override
    public void visit(OWLObjectComplementOf ce) {
        OWLClassExpression c = ce.getOperand();
        c.accept(this);
        String trillOperand = stack.pop();
        String conv = "complementOf(" + trillOperand + ")";
        stack.push(conv);
    }

    @Override
    public void visit(OWLObjectSomeValuesFrom ce) {
        OWLObject object = ce.getFiller();
        if (object instanceof OWLClassExpression) {

            OWLObjectPropertyExpression property = ce.getProperty();
            String p = property.asOWLObjectProperty().getIRI().toString();
            OWLClassExpression c = ce.getFiller();
            c.accept(this);
            String trillOperand = stack.pop();
            String conv = "someValuesFrom(" + p + "," + trillOperand + ")";
            stack.push(conv);

        } else {
            throw new IllegalValueException("Wrong class expression."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void visit(OWLObjectAllValuesFrom ce) {
        OWLObject object = ce.getFiller();
        if (object instanceof OWLClassExpression) {

            OWLObjectPropertyExpression property = ce.getProperty();
            String p = property.asOWLObjectProperty().getIRI().toString();
            OWLClassExpression c = ce.getFiller();
            c.accept(this);
            String trillOperand = stack.pop();
            String conv = "allValuesFrom(" + p + "," + trillOperand + ")";
            stack.push(conv);

        } else {
            throw new IllegalValueException("Wrong class expression."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void visit(OWLObjectHasValue ce) {
        OWLObject object = ce.getFiller();
        if (object instanceof OWLIndividual) {

            OWLObjectPropertyExpression property = ce.getProperty();
            String p = property.asOWLObjectProperty().getIRI().toString();
            OWLIndividual i = ce.getFiller();
            String conv = "someValuesFrom(" + p + ", oneOf(" + i + "))";
            stack.push(conv);

        } else {
            throw new IllegalValueException("Wrong class expression."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void visit(OWLObjectMinCardinality ce) {
        OWLObject object = ce.getFiller();
        if (object instanceof OWLClassExpression) {

            OWLObjectPropertyExpression property = ce.getProperty();
            String p = property.asOWLObjectProperty().getIRI().toString();
            int cardinality = ce.getCardinality();
            OWLClassExpression c = ce.getFiller();
            c.accept(this);
            String trillOperand = stack.pop();
            String conv = "minCardinality(" + cardinality + "," + p + "," + trillOperand + ")";
            stack.push(conv);

        } else {
            throw new IllegalValueException("Wrong class expression."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void visit(OWLObjectExactCardinality ce) {
        OWLObject object = ce.getFiller();
        if (object instanceof OWLClassExpression) {

            OWLObjectPropertyExpression property = ce.getProperty();
            String p = property.asOWLObjectProperty().getIRI().toString();
            int cardinality = ce.getCardinality();
            OWLClassExpression c = ce.getFiller();
            c.accept(this);
            String trillOperand = stack.pop();
            String conv = "exactCardinality(" + cardinality + "," + p + "," + trillOperand + ")";
            stack.push(conv);

        } else {
            throw new IllegalValueException("Wrong class expression."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void visit(OWLObjectMaxCardinality ce) {
        OWLObject object = ce.getFiller();
        if (object instanceof OWLClassExpression) {

            OWLObjectPropertyExpression property = ce.getProperty();
            String p = property.asOWLObjectProperty().getIRI().toString();
            int cardinality = ce.getCardinality();
            OWLClassExpression c = ce.getFiller();
            c.accept(this);
            String trillOperand = stack.pop();
            String conv = "maxCardinality(" + cardinality + "," + p + "," + trillOperand + ")";
            stack.push(conv);

        } else {
            throw new IllegalValueException("Wrong class expression."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public void visit(OWLObjectHasSelf ce) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLObjectOneOf ce) {     
        String conv = "oneOf(";
        for (OWLIndividual c : ce.getIndividuals()) {
            conv += c + ",";
        }    
        conv = conv.substring(0, conv.length() - 1);
        conv += ")";
        stack.push(conv);
    }

    @Override
    public void visit(OWLDataSomeValuesFrom ce) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataAllValuesFrom ce) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataHasValue ce) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataMinCardinality ce) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataExactCardinality ce) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataMaxCardinality ce) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
