/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.utilities;

import it.unife.ml.bundle.BundleCLIImpl;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.config.AppenderRef;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.slf4j.Logger;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LogUtilities {

    public static void addConsoleAppender(Logger logger, Level level) {
        LoggerContext log4jContext = (LoggerContext) LogManager.getContext(false);
        Configuration log4jConfig = log4jContext.getConfiguration();
        Appender consoleAppender = log4jConfig.getAppender(Constants.CONSOLE_APPENDER);
        consoleAppender.start();
        AppenderRef ref = AppenderRef.createAppenderRef(consoleAppender.getName(), null, null);
        AppenderRef[] refs = new AppenderRef[]{ref};

        LoggerConfig loggerConfig = LoggerConfig.createLogger(true, level,
                logger.getName(), "true", refs, null, log4jConfig, null);

        if (!loggerConfig.getAppenders().containsKey(Constants.CONSOLE_APPENDER)) {
            loggerConfig.addAppender(consoleAppender, level, null);
            log4jConfig.addLogger(loggerConfig.getName(), loggerConfig);
            log4jContext.updateLoggers();
        }
    }

    public static void addConsoleAppender(String loggerName, Level level) {
        LoggerContext log4jContext = (LoggerContext) LogManager.getContext(false);
        Configuration log4jConfig = log4jContext.getConfiguration();
        Appender consoleAppender = log4jConfig.getAppender(Constants.CONSOLE_APPENDER);
        consoleAppender.start();

        LoggerConfig loggerConfig = log4jConfig.getLoggerConfig(loggerName);
        loggerConfig.addAppender(consoleAppender, level, null);
        log4jContext.updateLoggers();
    }

}
