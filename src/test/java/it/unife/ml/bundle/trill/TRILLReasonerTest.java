/**
 * This file is part of BUNDLE.
 *
 * BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 * BUNDLE can be used both as module and as standalone.
 *
 * BUNDLE and all its parts are distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.trill;

import it.unife.ml.probowlapi.core.ProbabilisticExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import java.io.File;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

/**
 *
 * @author Alfredo
 */
public class TRILLReasonerTest {

    public TRILLReasonerTest() {
    }

    /**
     * Test of setRootOntology method, of class TRILLReasoner.
     *
     * @throws org.semanticweb.owlapi.model.OWLOntologyCreationException
     */
    @Test
    public void testSetRootOntology() throws OWLOntologyCreationException {
        System.out.println("setRootOntology");
        // carichi ontologia da file
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology rootOntology = manager.loadOntologyFromOntologyDocument(new File("examples/father_el.owl"));
        System.out.println(System.getenv("JAVA_HOME"));
        System.out.println(System.getenv("LD_PRELOAD"));

        // istanza di TRILL
        TRILLReasoner instance = new TRILLReasoner();
        // dentro setRootOntology
        // convertire l'ontologia in stringa RDF/XML
        // inviarla a trill
        instance.setRootOntology(rootOntology);
        instance.init();

    }

    /**
     * Test of getRootOntology method, of class TRILLReasoner.
     */
    //@Test
    public void testGetRootOntology() {
        System.out.println("getRootOntology");
        TRILLReasoner instance = new TRILLReasoner();
        OWLOntology expResult = null;
        OWLOntology result = instance.getRootOntology();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setReasoningTimeout method, of class TRILLReasoner.
     */
//    @Test
    public void testSetReasoningTimeout_long() {
        System.out.println("setReasoningTimeout");
        long reasoningTimeout = 0L;
        TRILLReasoner instance = new TRILLReasoner();
        instance.setReasoningTimeout(reasoningTimeout);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of dispose method, of class TRILLReasoner.
     */
//    @Test
    public void testDispose() {
        System.out.println("dispose");
        TRILLReasoner instance = new TRILLReasoner();
        instance.dispose();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class TRILLReasoner.
     */
//    @Test
    public void testInit() throws Exception {
        System.out.println("init");
        TRILLReasoner instance = new TRILLReasoner();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isInitialized method, of class TRILLReasoner.
     */
//    @Test
    public void testIsInitialized() {
        System.out.println("isInitialized");
        TRILLReasoner instance = new TRILLReasoner();
        boolean expResult = false;
        boolean result = instance.isInitialized();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testComputeQuery() throws OWLOntologyCreationException, OWLException, ObjectNotInitializedException {
        System.out.println("computeQuery");
        // carichi ontologia da file
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology rootOntology = manager.loadOntologyFromOntologyDocument(new File("examples/father_el.owl"));
        // istanza di TRILL
        TRILLReasoner instance = new TRILLReasoner();
        // dentro setRootOntology
        // convertire l'ontologia in stringa RDF/XML
        // inviarla a trill
        instance.setRootOntology(rootOntology);
        instance.init();
        // create Class assertion axiom
        OWLDataFactory owldf = OWLManager.getOWLDataFactory();
        OWLClassExpression owlce = owldf.getOWLClass(IRI.create("http://example.com/father#person"));
        OWLIndividual owli = owldf.getOWLNamedIndividual(IRI.create("http://example.com/father#markus"));
        OWLClassAssertionAxiom query = owldf.getOWLClassAssertionAxiom(owlce, owli);
        ProbabilisticReasonerResult result = instance.computeQuery(query);
        assertEquals(0.059703868, result.getQueryProbability().getValue(), 0.001);
    }

    @Test
    public void testComputeExplainQuery() throws OWLOntologyCreationException, OWLException, ObjectNotInitializedException {
        System.out.println("computeExplainQuery");
        try {
            // carichi ontologia da file
            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
            OWLOntology rootOntology = manager.loadOntologyFromOntologyDocument(new File("examples/father_oe.owl"));
            // istanza di TRILL
            TRILLReasoner instance = new TRILLReasoner();
            // dentro setRootOntology
            // convertire l'ontologia in stringa RDF/XML
            // inviarla a trill
            instance.setRootOntology(rootOntology);
            instance.init();
            // create Class assertion axiom
            OWLDataFactory owldf = OWLManager.getOWLDataFactory();
            OWLClassExpression owlce = owldf.getOWLClass(IRI.create("http://example.com/father#person"));
            OWLIndividual owli = owldf.getOWLNamedIndividual(IRI.create("http://example.com/father#markus"));
            OWLClassAssertionAxiom query = owldf.getOWLClassAssertionAxiom(owlce, owli);
            ProbabilisticExplanationReasonerResult result = instance.computeExplainQuery(query);
            assertEquals(0.92875, result.getQueryProbability().getValue(), 0.001);
            assertEquals(4, result.getQueryExplanations().size());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace(System.out);
        }
    }

    @Test
    public void testComputeExplainQueryPropertyAssertionAxiom() throws OWLOntologyCreationException, OWLException, ObjectNotInitializedException {
        System.out.println("computeExplainQuery PropertyAssertion axiom");
        // carichi ontologia da file
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology rootOntology = manager.loadOntologyFromOntologyDocument(new File("examples/father_oe.owl"));
        // istanza di TRILL
        TRILLReasoner instance = new TRILLReasoner();
        // dentro setRootOntology
        // convertire l'ontologia in stringa RDF/XML
        // inviarla a trill
        instance.setRootOntology(rootOntology);
        instance.init();
        // create object property assertion axiom
        OWLDataFactory owldf = OWLManager.getOWLDataFactory();
        OWLObjectPropertyExpression owlProperty = owldf.getOWLObjectProperty(IRI.create("http://example.com/father#hasChild"));
        OWLIndividual owlIndividual1 = owldf.getOWLNamedIndividual(IRI.create("http://example.com/father#anna"));
        OWLIndividual owlIndividual2 = owldf.getOWLNamedIndividual(IRI.create("http://example.com/father#heinz"));
        OWLObjectPropertyAssertionAxiom query = owldf.getOWLObjectPropertyAssertionAxiom(owlProperty, owlIndividual1, owlIndividual2);

        ProbabilisticExplanationReasonerResult result = instance.computeExplainQuery(query);
        assertEquals(1.0, result.getQueryProbability().getValue(), 0.001);
        assertEquals(1, result.getQueryExplanations().size());
    }

    @Test
    public void testComputeExplainQuerySubclassAxiom() throws OWLOntologyCreationException, OWLException, ObjectNotInitializedException {
        System.out.println("computeExplainQuery Subclass axiom");
        // carichi ontologia da file
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology rootOntology = manager.loadOntologyFromOntologyDocument(new File("examples/father_oe.owl"));
        // istanza di TRILL
        TRILLReasoner instance = new TRILLReasoner();
        // dentro setRootOntology
        // convertire l'ontologia in stringa RDF/XML
        // inviarla a trill
        instance.setRootOntology(rootOntology);
        instance.init();
        // create subclass axiom
        OWLDataFactory owldf = OWLManager.getOWLDataFactory();
        OWLClassExpression superclass = owldf.getOWLClass(IRI.create("http://example.com/father#person"));
        OWLClassExpression subclass = owldf.getOWLClass(IRI.create("http://example.com/father#father"));
        OWLSubClassOfAxiom query = owldf.getOWLSubClassOfAxiom(subclass, superclass);

        ProbabilisticExplanationReasonerResult result = instance.computeExplainQuery(query);
        assertEquals(0.0597, result.getQueryProbability().getValue(), 0.001);
        assertEquals(1, result.getQueryExplanations().size());
    }
}
