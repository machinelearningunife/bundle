/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.trill;

import org.jpl7.Atom;
import org.jpl7.Compound;
import org.jpl7.Query;
import org.jpl7.Term;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class JPLTest {
    
    @Test
    public void simpleTest() {
        Query q1
                = new Query(
                        "use_module",
                        new Term[]{new Compound("library", new Term[]{new Atom("lists")})}
                );
        q1.hasSolution();
    }
    
}
