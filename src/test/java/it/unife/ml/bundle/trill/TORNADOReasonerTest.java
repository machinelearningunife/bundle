/**
 * This file is part of BUNDLE.
 *
 * BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 * BUNDLE can be used both as module and as standalone.
 *
 * BUNDLE and all its parts are distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.trill;


import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import java.io.File;


import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

/**
 *
 * @author Alfredo
 */
public class TORNADOReasonerTest {

    public TORNADOReasonerTest() {
    }

    /**
     * Test of setRootOntology method, of class TRILLReasoner.
     * @throws org.semanticweb.owlapi.model.OWLOntologyCreationException
     */
    @Test
    public void testSetRootOntology() throws OWLOntologyCreationException {
        System.out.println("setRootOntology");
        // carichi ontologia da file
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology rootOntology = manager.loadOntologyFromOntologyDocument(new File("examples/father_el.owl"));
       
        // istanza di TRILL
        AbstractTRILLFrameworkReasoner instance = new TORNADOReasoner();
        // dentro setRootOntology
        // convertire l'ontologia in stringa RDF/XML
        // inviarla a trill
        instance.setRootOntology(rootOntology);
        instance.init();

    }

    @Test
    public void testComputeQuery() throws OWLOntologyCreationException, OWLException, ObjectNotInitializedException {
        System.out.println("computeQuery instanceOf TORNADO");
        // carichi ontologia da file
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology rootOntology = manager.loadOntologyFromOntologyDocument(new File("examples/father_el.owl"));
        // istanza di TRILL
        AbstractTRILLFrameworkReasoner instance = new TORNADOReasoner();
        // dentro setRootOntology
        // convertire l'ontologia in stringa RDF/XML
        // inviarla a trill
        instance.setRootOntology(rootOntology);
        instance.init();
        // create Class assertion axiom
        OWLDataFactory owldf = OWLManager.getOWLDataFactory();
        OWLClassExpression owlce = owldf.getOWLClass(IRI.create("http://example.com/father#person"));
        OWLIndividual owli = owldf.getOWLNamedIndividual(IRI.create("http://example.com/father#markus"));
        OWLClassAssertionAxiom query = owldf.getOWLClassAssertionAxiom(owlce, owli);
        ProbabilisticReasonerResult result = instance.computeQuery(query);
        assertEquals(0.059703868, result.getQueryProbability().getValue(), 0.001);
          
    }
}
