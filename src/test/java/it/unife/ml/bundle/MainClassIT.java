/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import it.unife.ml.math.ApproxDouble;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainClassIT {

    private final String ont3 = "examples/example-3_wp_owl.owl";
    private final String fatherOnt = "examples/father_oe.owl";
    private final String pyrimidineModifiedOnt = "examples/pyrimidine_modified.owl";
    private final String minimalInconsistentOntology = "examples/minimal-inconsistent-ontology.owl";
    private final String minimalConsistentOntology = "examples/minimal-consistent-ontology.owl";
    private final String unsatOnt = "examples/unsat2/CP999-3-modified.ttl";

    public MainClassIT() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        ApproxDouble.resetAccuracy();
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of main method, of class MainClass.
     */
    @Disabled
    @Test
    public void testMain() {
        String[] args = {
            //            "-v",
            "--instance",
            "http://cohse.semanticweb.org/ontologies/people#Kevin,http://cohse.semanticweb.org/ontologies/people#petOwner",
            "file:" + ont3
        };
        System.out.println("Test 1 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
        // the result shoud be 0.2 (with accuracy 5)
    }

    /**
     * Test 2 of main method, of class MainClass.
     */
    @Disabled
    @Test
    public void testMain2() {
        String[] args = {
            "-v",
            "--instance",
            "http://example.com/father#markus,http://example.com/father#person",
            "file:" + fatherOnt
        };
        System.out.println("Test 2 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
        // the result should be 0.92875 (with accuracy 5)
    }

    @Disabled
    @Test
    public void testMain3() {
        String[] args = {
            "-v",
            "--noProb",
            //            "-m", "black",
            //            "--inconsistent",
            "file:examples/unsat/unsat.ttl"
        };
        System.out.println("Test 3 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Disabled
    @Test
    public void testMain4() {
        String[] args = {
            "-v",
            "--noProb",
            "--ignoreImports",
            //            "-m", "black",
            //            "--inconsistent",
            "file:examples/unsat/unsat.ttl"
        };
        System.out.println("Test 4 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Disabled
    @Test
    public void testMain5() {
        String[] args = {
            "--verbose",
            "--noProb",
            "--method",
            "owlexp",
            "--inconsistent",
            "file:" + minimalInconsistentOntology
        };
        System.out.println("Test 5 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Disabled
    @Test
    public void testMain6() {
        String[] args = {
            "--verbose",
            "--method",
            "owlexp",
            "file:" + minimalConsistentOntology
        };
        System.out.println("Test 6 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    // @Disabled
    @Test
    public void testMain7() {
        String[] args = {
            "-v",
            "-r", "fact++",
            "--instance",
            "http://cohse.semanticweb.org/ontologies/people#Kevin,http://cohse.semanticweb.org/ontologies/people#petOwner",
            "file:" + ont3
        };
        System.out.println("Test 7 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
        // the result shoud be 0.2 (with accuracy 5)
    }

    // @Disabled
    @Test
    public void testMain8() {
        String[] args = {
            "--verbose",
            //            "-max", "1",
            "--time", "2s",
            "--noProb",
            "-r",
            "jfact",
            "--method",
            "owlexp",
            "--inconsistent",
            "file:" + unsatOnt
        };
        System.out.println("Test 8 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Disabled
    @Test
    public void testMain9() {
        String[] args = {
            "--verbose",
            //            "-max", "1",
            "--time", "15s",
            "--noProb",
            "-r",
            "jfact",
            "--method",
            "owlexp",
            "--inconsistent",
            "file:" + unsatOnt
        };
        System.out.println("Test 9 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Disabled
    @Test
    public void testMain10() {
        String[] args = {
            "--verbose",
            "-max", "1",
            "--noProb",
            "-r",
            "jfact",
            "--method",
            "owlexp",
            "--inconsistent",
            "file:" + unsatOnt
        };
        System.out.println("Test 10 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Test
    public void testMain11() {
        String[] args = {
            "-v",
            "-r", "trill",
            "--instance",
            "http://example.com/father#markus,http://example.com/father#person",
            "file:" + fatherOnt
        };
        System.out.println("Test 11 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
        // the result shoud be 0.2 (with accuracy 5)
    }
    
    @Disabled
    @Test
    public void testMain12() {
        String[] args = {
            "-v",
            "-r", "trill",
            "--propertyValue",
            "http://example.com/father#anna,http://example.com/father#hasChild,http://example.com/father#heinz",
            "file:" + fatherOnt
        };
        System.out.println("Test 12 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain13() {
        String[] args = {
            "-v",
            "-r", "trill",
            "--subclass",
            "http://example.com/father#father,http://example.com/father#male",
            "file:" + fatherOnt
        };
        System.out.println("Test 13 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain14() {
        String[] args = {
            "-v",
            "-r", "trill",
            "--unsat",
            "http://example.com/father#father",
            "file:" + fatherOnt
        };
        System.out.println("Test 14 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain15() {
        String[] args = {
            "-v",
            "-r", "trill",
            "--inconsistent",
            "file:" + fatherOnt
        };
        System.out.println("Test 15 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Test
    public void testMain16() {
        String[] args = {
            "-v",
            "-r", "trillp",
            "--instance",
            "http://example.com/father#markus,http://example.com/father#person",
            "file:" + fatherOnt
        };
        System.out.println("Test 16 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain17() {
        String[] args = {
            "-v",
            "-r", "trillp",
            "--propertyValue",
            "http://example.com/father#anna,http://example.com/father#hasChild,http://example.com/father#heinz",
            "file:" + fatherOnt
        };
        System.out.println("Test 17 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain18() {
        String[] args = {
            "-v",
            "-r", "trillp",
            "--subclass",
            "http://example.com/father#father,http://example.com/father#male",
            "file:" + fatherOnt
        };
        System.out.println("Test 18 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain19() {
        String[] args = {
            "-v",
            "-r", "trillp",
            "--unsat",
            "http://example.com/father#father",
            "file:" + fatherOnt
        };
        System.out.println("Test 19 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain20() {
        String[] args = {
            "-v",
            "-r", "trillp",
            "--inconsistent",
            "file:" + fatherOnt
        };
        System.out.println("Test 20 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Test
    public void testMain21() {
        String[] args = {
            "-v",
            "-r", "tornado",
            "--instance",
            "http://example.com/father#markus,http://example.com/father#person",
            "file:" + fatherOnt
        };
        System.out.println("Test 21 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain22() {
        String[] args = {
            "-v",
            "-r", "tornado",
            "--propertyValue",
            "http://example.com/father#anna,http://example.com/father#hasChild,http://example.com/father#heinz",
            "file:" + fatherOnt
        };
        System.out.println("Test 22 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain23() {
        String[] args = {
            "-v",
            "-r", "tornado",
            "--subclass",
            "http://example.com/father#father,http://example.com/father#male",
            "file:" + fatherOnt
        };
        System.out.println("Test 23 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain24() {
        String[] args = {
            "-v",
            "-r", "tornado",
            "--unsat",
            "http://example.com/father#father",
            "file:" + fatherOnt
        };
        System.out.println("Test 24 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Disabled
    @Test
    public void testMain25() {
        String[] args = {
            "-v",
            "-r", "tornado",
            "--inconsistent",
            "file:" + fatherOnt
        };
        System.out.println("Test 25 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
    
    @Test
    public void testMain26() {
        String[] args = {
            "-r", "fact++",
            "-m", "black",
            "--subclass", "http://clarkparsia.com/pronto/cancer_ra.owl#MotherAffected,http://clarkparsia.com/pronto/cancer_ra.owl#RiskFactor",
            "file:" + "examples/BRCA.owl"
        };
        System.out.println("Test 26 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }
}
