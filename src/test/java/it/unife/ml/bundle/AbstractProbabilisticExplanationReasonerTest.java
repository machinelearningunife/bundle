/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import it.unife.ml.bundle.factpp.FactppInitializer;
import static it.unife.ml.bundle.utilities.BundleUtilities.getManchesterSyntaxString;
import it.unife.ml.bundle.utilities.Constants;
import static it.unife.ml.bundle.utilities.Constants.HSTMethod.*;
import it.unife.ml.bundle.utilities.Constants.ReasonerName;
import it.unife.ml.probowlapi.core.ProbabilisticExplanationReasoner;
import java.io.File;

import org.junit.jupiter.api.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.elk.owlapi.ElkReasonerFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class AbstractProbabilisticExplanationReasonerTest {

    protected static String pyrimidineModifiedOnt = "examples/pyrimidine_modified.owl";
    protected static String carcOntLearnedPath = "examples/carcinogenesis_learned.owl";
    protected static String peoplePetsOnt = "examples/example-3_wp_owl.owl";
    protected static String pizzaOntologyPath = "examples/pizza.owl";
    protected static String fatherOntologyPath = "examples/father_el.owl";
    protected static String crimePunishmentOntologyPath = "examples/crime_and_punishment.owl";
    protected static Double delta = 0.00001;
    protected static OWLOntologyManager manager;
    protected static OWLDataFactory df;
    protected static OWLOntology pyrimidineOntology;
    protected static OWLOntology carcOntology;
    protected static OWLOntology peoplePetsOntology;
    protected static OWLOntology pizzaOntology;
    protected static OWLOntology fatherOntology;
    protected static OWLOntology crimePunishmentOntology;

    private static void loadELOntologies() {
        try {
            fatherOntology = manager.loadOntologyFromOntologyDocument(
                    new File(fatherOntologyPath));
        } catch (OWLOntologyCreationException ex) {
            logger.error(ex.getMessage());
        }
    }

    static {
        manager = OWLManager.createOWLOntologyManager();
        df = manager.getOWLDataFactory();
    }

    protected ProbabilisticExplanationReasoner probReasoner;

    private static Logger logger = LoggerFactory.getLogger(AbstractProbabilisticExplanationReasonerTest.class);

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
        probReasoner.dispose();
    }

//    public AbstractProbabilisticExplanationReasonerTest(OWLAxiom query,
//            Double expectedProbability,
//            OWLOntology rootOntology,
//            int maxExplanations) {
//        this.query = query;
//        this.expectedProbability = expectedProbability;
//        this.rootOntology = rootOntology;
//        this.maxExplanations = maxExplanations;
//    }
//    @Parameters
    public static Collection queries() throws OWLOntologyCreationException {
        List<Object[]> parameters = new ArrayList<>();

        loadOntologies();

        String individualIRI;
        String classIRI;
        OWLIndividual owli;
        OWLClassExpression owlce;
        OWLClassAssertionAxiom owlca;
        Double expectedProbability;

        // Query 1
        individualIRI = "http://cohse.semanticweb.org/ontologies/people#Kevin";
        classIRI = "http://cohse.semanticweb.org/ontologies/people#petOwner";

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));
        owlce = df.getOWLClass(IRI.create(classIRI));

        owlca = df.getOWLClassAssertionAxiom(owlce, owli);

        expectedProbability = 0.2;

        parameters.add(new Object[]{owlca, expectedProbability,
            peoplePetsOntology, Integer.MAX_VALUE});
        // Query 2
        individualIRI = "http://dl-learner.org/res/pyrimidine042";
        classIRI = "https://sites.google.com/a/unife.it/ml/disponte/learnedClass";

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));
        OWLClassExpression learnedClass = df.getOWLClass(IRI.create(classIRI));

        owlca = df.getOWLClassAssertionAxiom(learnedClass, owli);

        expectedProbability = 0.56589;

        parameters.add(new Object[]{owlca, expectedProbability,
            pyrimidineOntology, Integer.MAX_VALUE});

        // Query 3
        individualIRI = "http://dl-learner.org/res/pyrimidine048";

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));
        owlca = df.getOWLClassAssertionAxiom(learnedClass, owli);

        expectedProbability = 0.56589;

        parameters.add(new Object[]{owlca, expectedProbability,
            pyrimidineOntology, Integer.MAX_VALUE});

        // Query 4
        individualIRI = "http://dl-learner.org/res/pyrimidine069";

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));
        owlca = df.getOWLClassAssertionAxiom(learnedClass, owli);

        expectedProbability = 0.56589;

        parameters.add(new Object[]{owlca, expectedProbability,
            pyrimidineOntology, Integer.MAX_VALUE});

        // Query 5
        // Fact++ bug the resulting probability is 0.56589. But it is wrong. The
        // explanations are not correct. 
        // I cannot understand why I have an error in local test with Fact++, whereas
        // with bitbucket pipelines there is no error
        individualIRI = "http://dl-learner.org/res/pyrimidine038";

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));
        owlca = df.getOWLClassAssertionAxiom(learnedClass, owli);

        expectedProbability = 0.393;

        parameters.add(new Object[]{owlca, expectedProbability,
            pyrimidineOntology, Integer.MAX_VALUE});

        // Query 6
        individualIRI = "http://dl-learner.org/carcinogenesis#d146";

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));

        owlca = df.getOWLClassAssertionAxiom(learnedClass, owli);

        expectedProbability = 0.86918;

        int maxExplanations = 50;
//        parameters.add(new Object[]{owlca, expectedProbability, carcOntology,
//            maxExplanations});
        // Query 7
        individualIRI = "http://www.semanticweb.org/giuseppe/ontologies/2017/10/untitled-ontology-29#a";
        classIRI = "http://www.semanticweb.org/giuseppe/ontologies/2017/10/untitled-ontology-29#Blasphemy";
//        owlce = df.getOWLObjectComplementOf(df.getOWLClass(IRI.create(classIRI)));
        owlce = df.getOWLObjectComplementOf(df.getOWLClass(IRI.create(classIRI)));

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));

        owlca = df.getOWLClassAssertionAxiom(owlce, owli);

        expectedProbability = 0.0;

        parameters.add(new Object[]{owlca, expectedProbability,
            pizzaOntology, Integer.MAX_VALUE});

        // Query 8
        individualIRI = "http://www.semanticweb.org/giuseppe/ontologies/2017/10/untitled-ontology-29#a";
        classIRI = "http://www.semanticweb.org/giuseppe/ontologies/2017/10/untitled-ontology-29#Blasphemy";
//        owlce = df.getOWLObjectComplementOf(df.getOWLClass(IRI.create(classIRI)));
        owlce = df.getOWLClass(IRI.create(classIRI));

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));

        owlca = df.getOWLClassAssertionAxiom(owlce, owli);

        expectedProbability = 1.0;

        parameters.add(new Object[]{owlca, expectedProbability,
            pizzaOntology, Integer.MAX_VALUE});

        // Query 9
        individualIRI = "http://www.semanticweb.org/crime_and_punishment#raskolnikov";
        classIRI = "http://www.semanticweb.org/crime_and_punishment#GreatMan";

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));
        owlce = df.getOWLClass(IRI.create(classIRI));

        owlca = df.getOWLClassAssertionAxiom(owlce, owli);

        expectedProbability = 0.176;

        parameters.add(new Object[]{owlca, expectedProbability,
            crimePunishmentOntology, Integer.MAX_VALUE});

        return parameters;
    }

    /**
     * Queries with EL OWL 2 ontologies.
     *
     * @return
     * @throws OWLOntologyCreationException
     */
    public static Collection elQueries() throws OWLOntologyCreationException {
        List<Object[]> parameters = new ArrayList<>();

        loadELOntologies();

        String individualIRI;
        String classIRI;
        OWLIndividual owli;
        OWLClassExpression owlce;
        OWLClassAssertionAxiom owlca;
        Double expectedProbability;
        OWLClassAssertionAxiom query;
        // Query 0
        individualIRI = "http://example.com/father#markus";
        classIRI = "http://example.com/father#male";

        owli = df.getOWLNamedIndividual(IRI.create(individualIRI));
        owlce = df.getOWLClass(IRI.create(classIRI));

        query = df.getOWLClassAssertionAxiom(owlce, owli);

        expectedProbability = 0.22557;

        parameters.add(new Object[]{query, expectedProbability,
            fatherOntology, Integer.MAX_VALUE});

        return parameters;
    }

    /*
    @Tag("konclude")
    @Tag("owlexplanation")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfKoncludeOWLExplanationComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations
    ) throws OWLOntologyCreationException, MalformedURLException {
        logger.info("Test query: (reasoner: Konclude\tmethod: OWLExplanation\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        System.out.printf("Test query: (reasoner: Konclude\tmethod: OWLExplanation\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        URL url = new URL("http://localhost:8888");
	OWLlinkReasonerConfiguration reasonerConfiguration =
		new OWLlinkReasonerConfiguration(url);
	OWLlinkHTTPXMLReasonerFactory factory = new OWLlinkHTTPXMLReasonerFactory();
        
        testComputeQuery(rootOntology, query, expectedProbability,
                PelletReasonerFactory.getInstance(), maxExplanations, OWLEXPLANATION);
    }
     */
//    @Ignore
    @Disabled
    @Tag("pellet")
    @Tag("glassbox")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfPelletGlassBoxComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations
    )
            throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: Pellet\tmethod: GlassBox\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        System.out.printf("Test query: (reasoner: Pellet\tmethod: GlassBox\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

//        BundleGlassBoxExplanation.setup();
//        PelletOptions.USE_ANNOTATION_SUPPORT = true;
//        PelletOptions.IGNORE_ANNOTATION_CLASSES = false;
//        probReasoner.setMaxExplanations(maxExplanations);
        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.PELLET, maxExplanations, GLASSBOX);
    }

//    @Ignore
    @Tag("pellet")
    @Tag("blackbox")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfPelletBlackBoxComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations
    ) throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: Pellet\tmethod: BlackBox\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        System.out.printf("Test query: (reasoner: Pellet\tmethod: BlackBox\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.PELLET, maxExplanations, BLACKBOX);
    }

//    @Ignore
    @Disabled
    @Tag("pellet")
    @Tag("owlexplanation")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfPelletOWLExplanationComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations
    ) throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: Pellet\tmethod: OWLExplanation\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        System.out.printf("Test query: (reasoner: Pellet\tmethod: OWLExplanation\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.PELLET, maxExplanations, OWLEXPLANATION);
    }

//    @Ignore
    @Tag("jfact")
    @Tag("blackbox")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfJFactBlackBoxComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations
    ) throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: JFact\tmethod: BlackBox\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        System.out.printf("Test query: (reasoner: JFact\tmethod: BlackBox\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.JFACT, maxExplanations, BLACKBOX);
    }

//    @Distabled
    @Tag("jfact")
    @Tag("owlexplanation")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfJFactOWLExplanationComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations
    ) throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: JFact\tmethod: OWLExplanation\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        System.out.printf("Test query: (reasoner: JFact\tmethod: OWLExplanation\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.JFACT, maxExplanations, OWLEXPLANATION);
    }

    // @Disabled
    @Tag("fact++")
    @Tag("blackbox")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfFactPPBlackBoxComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations
    ) throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: Fact++\tmethod: BlackBox\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        System.out.printf("Test query: (reasoner: Fact++\tmethod: BlackBox\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        FactppInitializer.init();
        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.FACTPP, maxExplanations, OWLEXPLANATION);
    }

    // @Disabled
    @Tag("fact++")
    @Tag("owlexplanation")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfFactPPOWLExplanationComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations
    ) throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: Fact++\tmethod: OWLExplanation\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        System.out.printf("Test query: (reasoner: Fact++\tmethod: OWLExplanation\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        FactppInitializer.init();
        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.FACTPP, maxExplanations, OWLEXPLANATION);
    }

//    @Ignore
    @Disabled
    @Tag("hermit")
    @Tag("blackbox")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfHermitBlackBoxComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations) throws OWLOntologyCreationException {
        System.out.printf("Test query: (reasoner: Hermit\tmethod: BlackBox\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.HERMIT, maxExplanations, BLACKBOX);
//        testComputeQuery(rootOntology, query, expectedProbability,
//                new org.semanticweb.HermiT.ReasonerFactory(), BLACKBOX);
    }

    @Disabled
    @Tag("hermit")
    @Tag("owlexplanation")
    @ParameterizedTest
    @MethodSource("queries")
    public void testInstanceOfHermitOWLExplanationComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations) throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: Hermit\tmethod: OWLExplanation\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        System.out.printf("Test query: (reasoner: Hermit\tmethod: OWLExplanation\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

//        testComputeQuery(rootOntology, query, expectedProbability,
//                new org.semanticweb.HermiT.ReasonerFactory(), maxExplanations, OWLEXPLANATION);
        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.HERMIT, OWLEXPLANATION);
    }

    @Tag("elk")
    @Tag("blackbox")
    @ParameterizedTest
    @MethodSource("elQueries")
    public void testInstanceOfELKBlackBoxComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations
    ) throws OWLOntologyCreationException {

        logger.info("Test query: (reasoner: ELK\tmethod: BlackBox\t"
                + "maxExplanations: {}): {}", Integer.MAX_VALUE, getManchesterSyntaxString(query));
        logger.info("Test query: (reasoner: ELK\tmethod: BlackBox\t"
                + "maxExplanations: %s): %s%n", Integer.MAX_VALUE, getManchesterSyntaxString(query));

        testComputeQuery(fatherOntology, query, expectedProbability,
                ReasonerName.ELK, Integer.MAX_VALUE, BLACKBOX);

    }

////    @Ignore
    @Tag("elk")
    @Tag("owlexplanation")
    @ParameterizedTest
    @MethodSource("elQueries")
    public void testInstanceOfELKOWLExplanationComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations) throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: ELK\tmethod: OWLExplanation\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        logger.info("Test query: (reasoner: ELK\tmethod: OWLExplanation\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        ElkReasonerFactory elkfact = new ElkReasonerFactory();
        OWLReasoner resoner = elkfact.createNonBufferingReasoner(rootOntology);
        NodeSet<OWLNamedIndividual> individuals = resoner.getInstances(df.getOWLClass(IRI.create("http://example.com/father#male")), false);
        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.ELK, Integer.MAX_VALUE, OWLEXPLANATION);
    }
//    @Test

    public void testInstanceOfStructuralComputeQuery(
            OWLAxiom query,
            Double expectedProbability,
            OWLOntology rootOntology,
            int maxExplanations) throws OWLOntologyCreationException {
        logger.info("Test query: (reasoner: Structural\tmethod: BlackBox\t"
                + "maxExplanations: {}): {}", maxExplanations, getManchesterSyntaxString(query));
        logger.info("Test query: (reasoner: Structural\tmethod: BlackBox\t"
                + "maxExplanations: %s): %s%n", maxExplanations, getManchesterSyntaxString(query));

        testComputeQuery(rootOntology, query, expectedProbability,
                ReasonerName.STRUCTURAL, maxExplanations, BLACKBOX);
    }

    private static void loadOntologies() {
//        manager = OWLManager.createOWLOntologyManager();
//        df = manager.getOWLDataFactory();
        try {
            pyrimidineOntology = manager.loadOntologyFromOntologyDocument(
                    new File(pyrimidineModifiedOnt));
        } catch (OWLOntologyCreationException ex) {
            logger.error(ex.getMessage());
        }
        try {
            carcOntology = manager.loadOntologyFromOntologyDocument(
                    new File(carcOntLearnedPath));
        } catch (OWLOntologyCreationException ex) {
            logger.error(ex.getMessage());
        }
        try {
            peoplePetsOntology = manager.loadOntologyFromOntologyDocument(
                    new File(peoplePetsOnt));
        } catch (OWLOntologyCreationException ex) {
            logger.error(ex.getMessage());
        }
        try {
            pizzaOntology = manager.loadOntologyFromOntologyDocument(
                    new File(pizzaOntologyPath));
        } catch (OWLOntologyCreationException ex) {
            logger.error(ex.getMessage());
        }
        try {
            crimePunishmentOntology = manager.loadOntologyFromOntologyDocument(
                    new File(crimePunishmentOntologyPath));
        } catch (OWLOntologyCreationException ex) {
            logger.error(ex.getMessage());
        }
    }

    /**
     * Execute a query and compare the result with the expected probability. It
     * performs the initialization before computing the query.
     *
     * @param ontology the knowledge base
     * @param query the axiom that represent the query
     * @param expectedProbability the expected probability
     * @param maxExplanations
     * @param hstMethod
     */
    public abstract void testComputeQuery(OWLOntology ontology, OWLAxiom query,
            Double expectedProbability, ReasonerName reasonerName,
            int maxExplanations, Constants.HSTMethod hstMethod);

    /**
     * Execute a query forcing the use of the BlackBox method and compare the
     * result with the expected probability. It performs the initialization
     * before computing the query.
     *
     * @param ontology the knowledge base
     * @param query the axiom that represent the query
     * @param expectedProbability the expected probability
     * @param reasonerFactory
     */
//    public abstract void testBlackBoxComputeQuery(OWLOntology ontology,
//            OWLAxiom query, Double expectedProbability, OWLReasonerFactory reasonerFactory);
    /**
     * Execute a query forcing the use of the OWLExplanation library method and
     * compare the result with the expected probability. It performs the
     * initialization before computing the query.
     *
     * @param ontology the knowledge base
     * @param query the axiom that represent the query
     * @param expectedProbability the expected probability
     * @param reasonerFactory
     */
//    public abstract void testOWLExplanationComputeQuery(OWLOntology ontology,
//            OWLAxiom query, Double expectedProbability, OWLReasonerFactory reasonerFactory);
    /**
     * Execute a query and compare the result with the expected probability. It
     * does not perform initialization of the reasoner before the computation.
     *
     * @param ontology
     * @param query
     * @param expectedProbability
     * @param hstMethod
     */
    public abstract void testComputeQuery(OWLOntology ontology, OWLAxiom query,
            Double expectedProbability, ReasonerName reasonerName, Constants.HSTMethod hstMethod);

//    public abstract void testGlassBoxComputeQuery(OWLOntology ontology, OWLAxiom query, Double expectedProbability, PelletReasonerFactory reasonerFactory);
}
