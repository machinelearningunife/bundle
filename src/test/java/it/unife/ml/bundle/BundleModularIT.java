/**
 *  This file is part of BUNDLE.
 *
 *  BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 *  BUNDLE can be used both as module and as standalone.
 *
 *  LEAP was implemented as a plugin of DL-Learner http://dl-learner.org,
 *  but some components can be used as stand-alone.
 *
 *  BUNDLE and all its parts are distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle;

import it.unife.ml.bundle.utilities.Constants;
import it.unife.ml.bundle.utilities.Constants.ReasonerName;
import it.unife.ml.probowlapi.core.ExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ProbabilisticExplanationReasonerResult;
import it.unife.ml.probowlapi.core.ProbabilisticReasonerResult;
import it.unife.ml.probowlapi.exception.ObjectNotInitializedException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Disabled;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
//@RunWith(Parameterized.class)
public class BundleModularIT extends AbstractProbabilisticExplanationReasonerTest {

    @BeforeAll
    public static void setUpClass() {

//        AbstractProbabilisticExplanationReasonerTest.setUpClass();
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
    }

    @AfterEach
    @Override
    public void tearDown() {
        super.tearDown();
    }

//    /**
//     * Test of computeQuery method, of class BundleModular.
//     */
////    @Test
//    @Disabled
//    public void testComputeQuery() throws Exception {
//        System.out.println("computeQuery");
//        OWLAxiom query = null;
//        Bundle instance = new Bundle();
//        ProbabilisticReasonerResult expResult = null;
//        ProbabilisticReasonerResult result = instance.computeQuery(query);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of dispose method, of class BundleModular.
//     */
////    @Test
//    public void testDispose() {
//        System.out.println("dispose");
//        Bundle instance = new Bundle();
//        instance.dispose();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of init method, of class BundleModular.
//     */
////    @Test
//    @Disabled
//    public void testInit() {
//        System.out.println("init");
//        Bundle instance = new Bundle();
//        instance.init();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of explainAxiom method, of class BundleModular.
//     */
////    @Test
//    @Disabled
//    public void testExplainAxiom() throws Exception {
//        System.out.println("explainAxiom");
//        OWLAxiom axiom = null;
//        Bundle instance = new Bundle();
//        ExplanationReasonerResult expResult = null;
//        ExplanationReasonerResult result = instance.explainAxiom(axiom);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of computeExplainQuery method, of class BundleModular.
//     */
////    @Test
//    @Disabled
//    public void testComputeExplainQuery() throws OWLException, ObjectNotInitializedException {
//        System.out.println("computeExplainQuery");
//        OWLAxiom query = null;
//        Bundle instance = new Bundle();
//        ProbabilisticExplanationReasonerResult expResult = null;
//        ProbabilisticExplanationReasonerResult result = instance.computeExplainQuery(query);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    @Override
    public void testComputeQuery(OWLOntology ontology, OWLAxiom query,
            Double expectedProbability, ReasonerName reasonerName,
            int maxExplanations, Constants.HSTMethod hstMethod) {
        ProbabilisticReasonerResult result = null;
        try {
            BundleConfigurationBuilder configBuilder = new BundleConfigurationBuilder(ontology);
            BundleConfiguration config = configBuilder.verbose(true)
                    .maxExplanations(maxExplanations)
                    .hstMethod(hstMethod)
                    .reasoner(reasonerName)
                    .buildConfiguration();
            probReasoner = new Bundle(config);

//            ((Bundle) probReasoner).verbose = true;
//            probReasoner.setRootOntology(ontology);
//            probReasoner.setMaxExplanations(maxExplanations);
//            probReasoner.setReasonerFactory(reasonerFactory);
//            ((Bundle) probReasoner).setHstMethod(hstMethod);
//            ((Bundle) probReasoner).setVerbose(true);
            probReasoner.init();
            result = probReasoner.computeQuery(query);
        } catch (ObjectNotInitializedException ex) {
            fail("Computing query without initialization");
        } catch (OWLException ex) {
            fail("Unable to compute the query");
        }

        assertNotNull(result);
        assertEquals(expectedProbability,
                result.getQueryProbability().getValue(), delta);
    }

//    @Override
//    public void testBlackBoxComputeQuery(OWLOntology ontology, OWLAxiom query,
//            Double expectedProbability, OWLReasonerFactory reasonerFactory) {
//        ProbabilisticReasonerResult result = null;
//        try {
//            probReasoner.setRootOntology(ontology);
//            probReasoner.setMaxExplanations(maxExplanations);
//            probReasoner.setReasonerFactory(reasonerFactory);
//            ((BundleModular) probReasoner).setHstMethod(BLACKBOX);
//            probReasoner.init();
//            result = probReasoner.computeQuery(query);
//        } catch (ObjectNotInitializedException ex) {
//            fail("Computing query without initialization");
//        } catch (OWLException ex) {
//            fail("Unable to compute the query");
//        }
//
//        assertNotNull(result);
//        assertEquals(expectedProbability,
//                result.getQueryProbability().getValue(), delta);
//    }
    @Override
    public void testComputeQuery(OWLOntology ontology, OWLAxiom query,
            Double expectedProbability, ReasonerName reasonerName,
            Constants.HSTMethod hstMethod) {
        ProbabilisticReasonerResult result = null;
        try {

            BundleConfigurationBuilder configBuilder = new BundleConfigurationBuilder(ontology);
            BundleConfiguration config = configBuilder.verbose(true)
                    .hstMethod(hstMethod)
                    .reasoner(reasonerName)
                    .buildConfiguration();
            probReasoner = new Bundle(config);

//            probReasoner.setRootOntology(ontology);
//            probReasoner.setReasonerFactory(reasonerFactory);
//            ((Bundle) probReasoner).setHstMethod(hstMethod);
//            ((Bundle) probReasoner).setVerbose(true);
            probReasoner.init();
            result = probReasoner.computeQuery(query);
        } catch (ObjectNotInitializedException ex) {
            fail("Computing query without initialization");
        } catch (OWLException ex) {
            fail("Unable to compute the query");
        }

        assertNotNull(result);
        assertEquals(expectedProbability,
                result.getQueryProbability().getValue(), delta);
    }

//    @Override
//    public void testOWLExplanationComputeQuery(OWLOntology ontology, OWLAxiom query, Double expectedProbability, OWLReasonerFactory reasonerFactory) {
//        ProbabilisticReasonerResult result = null;
//        try {
//            probReasoner.setRootOntology(rootOntology);
//            probReasoner.setMaxExplanations(maxExplanations);
//            probReasoner.setReasonerFactory(reasonerFactory);
//            ((BundleModular) probReasoner).setHstMethod(OWLEXPLANATION);
//            probReasoner.init();
//            result = probReasoner.computeQuery(query);
//        } catch (ObjectNotInitializedException ex) {
//            fail("Computing query without initialization");
//        } catch (OWLException ex) {
//            fail("Unable to compute the query");
//        }
//
//        assertNotNull(result);
//        assertEquals(expectedProbability,
//                result.getQueryProbability().getValue(), delta);
//    }
//
//    @Override
//    public void testGlassBoxComputeQuery(OWLOntology rootOntology, OWLAxiom query, Double expectedProbability, PelletReasonerFactory reasonerFactory) {
//        ProbabilisticReasonerResult result = null;
//        try {
//            probReasoner.setRootOntology(rootOntology);
//            probReasoner.setMaxExplanations(maxExplanations);
//            probReasoner.setReasonerFactory(reasonerFactory);
//            ((BundleModular) probReasoner).setHstMethod(GLASSBOX);
//            probReasoner.init();
//            result = probReasoner.computeQuery(query);
//        } catch (ObjectNotInitializedException ex) {
//            fail("Computing query without initialization");
//        } catch (OWLException ex) {
//            fail("Unable to compute the query");
//        }
//
//        assertNotNull(result);
//        assertEquals(expectedProbability,
//                result.getQueryProbability().getValue(), delta);
//    }
}
