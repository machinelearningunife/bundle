/**
 * This file is part of BUNDLE.
 *
 * BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 * BUNDLE can be used both as module and as standalone.
 *
 * BUNDLE and all its parts are distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.unife.ml.bundle.utilities;

import it.unife.ml.bundle.utilities.TRILLClassExpressionConverterImpl;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataExactCardinality;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataMaxCardinality;
import org.semanticweb.owlapi.model.OWLDataMinCardinality;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectExactCardinality;
import org.semanticweb.owlapi.model.OWLObjectHasSelf;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLObjectMinCardinality;
import org.semanticweb.owlapi.model.OWLObjectOneOf;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.OWLPropertyExpression;

/**
 *
 * @author Alfredo
 */
public class TRILLClassExpressionConverterImplTest {

    public TRILLClassExpressionConverterImplTest() {
    }

    /**
     * Test of getTRILLClassExpression method, of class
     * TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
//    public void testGetTRILLClassExpression() {
//        System.out.println("getTRILLClassExpression");
//        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
//        String expResult = "";
//        String result = instance.getTRILLClassExpression();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLClass() {
        System.out.println("visit");
        OWLClass ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    @Test
    public void testVisit_OWLObjectIntersectionUnionOf() {
        OWLDataFactory factory = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create("http://www.co-ode.org/ontologies/testont.owl");

        System.out.println("visit");
        OWLClass clsA = factory.getOWLClass(IRI.create(ontologyIRI + "#A"));
        OWLClass clsB = factory.getOWLClass(IRI.create(ontologyIRI + "#B"));
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        OWLObjectIntersectionOf intersection = factory.getOWLObjectIntersectionOf(clsA, clsB);
        OWLClass clsC = factory.getOWLClass(IRI.create(ontologyIRI + "#C"));
        OWLObjectUnionOf union = factory.getOWLObjectUnionOf(intersection, clsC);

        //instance.visit(union);

        String expected = "unionOf(http://www.co-ode.org/ontologies/testont.owl#C,intersectionOf(http://www.co-ode.org/ontologies/testont.owl#B,http://www.co-ode.org/ontologies/testont.owl#A))";
        assertEquals(instance.getTRILLClassExpression(union), expected);
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    @Test
    public void testVisit_OWLObjectIntersectionOf() {
        OWLDataFactory factory = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create("http://www.co-ode.org/ontologies/testont.owl");

        System.out.println("visit");
        OWLClass clsA = factory.getOWLClass(IRI.create(ontologyIRI + "#A"));
        OWLClass clsB = factory.getOWLClass(IRI.create(ontologyIRI + "#B"));
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        OWLObjectIntersectionOf intersection = factory.getOWLObjectIntersectionOf(clsA, clsB);

//        instance.visit(intersection);

        String expected = "intersectionOf(http://www.co-ode.org/ontologies/testont.owl#B,http://www.co-ode.org/ontologies/testont.owl#A)";
        assertEquals(instance.getTRILLClassExpression(intersection), expected);
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLObjectComplementOf() {
        System.out.println("visit");
        OWLObjectComplementOf ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    @Test
    public void testVisit_OWLObjectSomeValuesFrom() {
        OWLDataFactory factory = OWLManager.getOWLDataFactory();
        IRI ontologyIRI = IRI.create("http://www.co-ode.org/ontologies/testont.owl");

        System.out.println("visit");
        OWLObjectPropertyExpression p = factory.getOWLObjectProperty(IRI.create(ontologyIRI + "#P"));
        OWLClassExpression cls = factory.getOWLClass(IRI.create(ontologyIRI + "#C"));
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        OWLObjectSomeValuesFrom someValues = factory.getOWLObjectSomeValuesFrom(p, cls);

//        instance.visit(someValues);
        String expected = "someValuesFrom(http://www.co-ode.org/ontologies/testont.owl#P,http://www.co-ode.org/ontologies/testont.owl#C)";
        assertEquals(instance.getTRILLClassExpression(someValues), expected);
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLObjectAllValuesFrom() {
        System.out.println("visit");
        OWLObjectAllValuesFrom ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLObjectHasValue() {
        System.out.println("visit");
        OWLObjectHasValue ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLObjectMinCardinality() {
        System.out.println("visit");
        OWLObjectMinCardinality ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLObjectExactCardinality() {
        System.out.println("visit");
        OWLObjectExactCardinality ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLObjectMaxCardinality() {
        System.out.println("visit");
        OWLObjectMaxCardinality ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLObjectHasSelf() {
        System.out.println("visit");
        OWLObjectHasSelf ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLObjectOneOf() {
        System.out.println("visit");
        OWLObjectOneOf ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLDataSomeValuesFrom() {
        System.out.println("visit");
        OWLDataSomeValuesFrom ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLDataAllValuesFrom() {
        System.out.println("visit");
        OWLDataAllValuesFrom ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLDataHasValue() {
        System.out.println("visit");
        OWLDataHasValue ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLDataMinCardinality() {
        System.out.println("visit");
        OWLDataMinCardinality ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLDataExactCardinality() {
        System.out.println("visit");
        OWLDataExactCardinality ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of visit method, of class TRILLClassExpressionConverterImpl.
     */
    //@org.junit.Test
    public void testVisit_OWLDataMaxCardinality() {
        System.out.println("visit");
        OWLDataMaxCardinality ce = null;
        TRILLClassExpressionConverterImpl instance = new TRILLClassExpressionConverterImpl();
        instance.visit(ce);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
