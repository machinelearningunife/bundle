package it.unife.ml.bundle;

import it.unife.ml.bundle.MainClass;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.assertTrue;
//import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.jupiter.api.Test;

/**
 * This file is part of BUNDLE.
 *
 * BUNDLE is a probabilistic reasoner for OWL 2 ontologies.
 *
 * BUNDLE can be used both as module and as standalone.
 *
 * LEAP was implemented as a plugin of DL-Learner http://dl-learner.org, but
 * some components can be used as stand-alone.
 *
 * BUNDLE and all its parts are distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class HelpTest {
    
//    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    public void testHelp1() {
        String[] args = {
            "-h"
        };
        System.out.println("Test 1 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Test
    public void testHelp2() {
        String[] args = {
            "--help"
        };
        System.out.println("Test 2 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

    @Test
    public void testHelp3() {
        String[] args = {
            "help"
        };
        System.out.println("Test 3 with args: " + Arrays.toString(args));
        MainClass.main(args);
        assertTrue(true);
    }

}
