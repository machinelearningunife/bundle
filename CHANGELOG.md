# BUNDLE Changelog

## Version 3.0.0
* BUNDLE supports the current OWL reasoners:
  * Pellet
  * Hermit
  * JFact
  * Fact++

## Version 2.3.2
* Fixed bugs #1 and #2

## Version 2.3.1
* Fixed bugs relative to DisjointUnion axioms
* OWLExplanation library can be used to retrieve the explanations
* Automated Docker image creation
* Added tests

## Version 2.3
* Fixed bugs relative to wrong explanations returned
* Fixes some memory leaks
* Improved perfomances by removing slow printings
* Improved interfaces
* Improved tests
