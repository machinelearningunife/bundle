BUNDLE (Bdds for Uncertain resoNing on Description Logic thEories) is an algorithm for reasoning on probabilistic ontologies following a distribution semantics.

BUNDLE uses JavaBDD library which exploits shared libraries. Before using BUNDLE you need to download and install/compile CUDD [1] and put the shared libraries in a folder reachable by the jvm.

[1] Developed by Fabio Somenzi. Visit http://vlsi.colorado.edu/~fabio/ to find the latest version of CUDD and its documentation.