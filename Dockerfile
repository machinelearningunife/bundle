# Java + swi-prolog + cplint + trill image
FROM rzese/trill

#USER root

#RUN mkdir $HOME/bundle
#RUN cd $HOME/bundle
WORKDIR /bundle

ADD target/bundle-*-standalone.jar .
ADD target/classes/bundle .
ADD examples ./examples

RUN chmod +x bundle

#RUN export PATH=$PATH:/bundle

ENV PATH="/bundle:${PATH}"
ENV CLASSPATH="/bundle:${CLASSPATH}"

#RUN ln -s /bundle/bundle /bin/bundle

# some useful tools
RUN apt update -y
RUN apt-get install -y \
	vim
#CMD ./bundle
